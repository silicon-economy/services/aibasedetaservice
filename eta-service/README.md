# Java application for the AI-based ETA service.


## Technology stack

* Java
* SpringBoot
* PostgreSQL

## Dependent on the following services and infrastructures

The services have to be running in order for the ETA-service to work properly.
* ETA-engine
* database (postgreSQL)

## Build and run

__All following commands should be executed in the directory 'aibasedetaservice/eta-service'.__

Run tests: `mvn test`

After running the tests a protocol of the results will be written to 'aibasedetaservice.eta-service.test-results.log'.
  
Build: `mvn clean package`

Run: `java -jar target\aibasedetaservice.eta-service-1.0-SNAPSHOT.jar`


### Build and run with Docker

To build and run the application in docker, execute:

Build: `docker build -t eta-service .`

Run: `docker run -p 8080:8080 eta-service`

To specify database parameters when running with docker use an environment file like the one found under 
'aibasedetaservice/config/eta-service-params.env' and specify it in the docker run command as follows:

`docker run -p 8080:8080 eta-service -env-file /path/to/env-file`

If you want to use the application in a docker-compose file, look at 'aibasedetaservice/docker-compose.yml' for reference.

If you want to run your Spring Application in your IDE, then you can use the docker-compose.dev.yml.

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.txt` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.

### Generating third-party license reports

This project uses the [license-maven-plugin](https://github.com/mojohaus/license-maven-plugin) to generate a file containing the licenses used by the third-party dependencies.
The content of the `mvn license:add-third-party` Maven goal's output (`target/generated-sources/license/THIRD-PARTY.txt`) can be copied into `third-party-licenses/third-party-licenses.txt`.

Third-party dependencies for which the licenses cannot be determined automatically by the license-maven-plugin have to be documented manually in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "Unknown license" license.
