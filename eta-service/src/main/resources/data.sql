
INSERT INTO t_transports (transport_id, client, timeliness_priority, source, transport_mode, destination, departure, eta, from_time, to_time) VALUES 
('1231', 'Alpha', 'LOW', 'Joseph-von-Fraunhofer-Str. 2-4, 44227 Dortmund, Deutschland', 'ROAD', 'Baroperstr. 288, 44227 Dortmund, Deutschland', '2020-08-17T10:00:00', '2020-08-18T09:00:00', '2020-08-18T09:00:00', '2020-08-18T12:00:00');

INSERT INTO t_transports (transport_id, client, timeliness_priority, source, transport_mode, destination, departure, eta, from_time, to_time) VALUES 
('3234', 'Butter', 'VERY_HIGH', 'Prien', 'ROAD', 'Frankfurt', '2020-08-17T10:00:00', '2020-08-18T09:00:00', '2020-08-18T09:00:00', '2020-08-18T12:01:00');

INSERT INTO t_transports (transport_id, client, timeliness_priority, source, transport_mode, destination, departure, eta, from_time, to_time) VALUES 
('3238', 'Butter', 'VERY_HIGH', 'Prien', 'ROAD', 'Frankfurt', '2020-08-17T10:00:00', '2020-08-18T09:00:00', '2020-08-18T09:00:00', '2020-08-18T12:01:00');

INSERT INTO t_transports (transport_id, client, timeliness_priority, source, transport_mode, destination, departure, eta, from_time, to_time) VALUES 
('3294', 'Butter', 'LOW', 'Prien', 'ROAD', 'Frankfurt', '2020-08-17T10:00:00', '2020-08-18T09:00:00', '2020-08-18T09:00:00', '2020-08-18T12:00:00');

INSERT INTO t_transports (transport_id, client, timeliness_priority, source, transport_mode, destination, departure, eta, from_time, to_time) VALUES 
('12331', 'Alpha', 'LOW', 'München', 'ROAD', 'Dortmund', '2020-08-17T10:00:00', '2020-08-18T09:00:00', '2020-08-18T09:00:00', '2020-08-18T12:00:00');

INSERT INTO t_transports (transport_id, client, timeliness_priority, source, transport_mode, destination, departure, eta, from_time, to_time) VALUES 
('12351', 'Alpha', 'LOW', 'München', 'ROAD', 'Dortmund', '2020-08-17T10:00:00', '2020-08-18T09:00:00', '2020-08-18T09:00:00', '2020-08-18T12:00:00');
