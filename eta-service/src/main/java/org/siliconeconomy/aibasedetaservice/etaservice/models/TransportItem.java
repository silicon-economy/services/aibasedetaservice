/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.models;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The transport item which represents a row in the ETA table.
 * 
 * @author Pajtim Thaqi
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "T_TRANSPORTS")
@Entity
public class TransportItem {
	/* The transport id. */
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	private Long transportId;

	/* The client name. */
	@Column(name = "CLIENT", nullable = false)
	private String client;

	/* The timeliness priority of a transport. */
	@Column(name = "TIMELINESS_PRIORITY", nullable = false)
    @Enumerated(EnumType.STRING)
	private TimelinessPriority timelinessPriority;

	/* The source for this transport. */
	@Column(name = "SOURCE", nullable = false)
	private String source;

	/* The transport mode. */
	@Column(name = "TRANSPORT_MODE", nullable = false)
	@Enumerated(EnumType.STRING)
	private TransportMode transportMode;

	/* The destination for this transport. */
	@Column(name = "DESTINATION", nullable = false)
	private String destination;

	/* The time when this transport departs. */
	@Column(name = "DEPARTURE", nullable = true)
	private LocalDateTime departure;
	
	/* The calculated ETA from the ETA-Engine. */
	@Column(name = "ETA", nullable = true)
	private LocalDateTime eta;

	/* The time corridor for a transport to arrive. */
	@Embedded
	private TimeCorridor timeCorridor;
	
	/* The calculated route for this transport. */
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "TRANSPORT_ROUTE", referencedColumnName = "id")
	@JsonIgnore
	private TransportRoute transportRoute;
}
