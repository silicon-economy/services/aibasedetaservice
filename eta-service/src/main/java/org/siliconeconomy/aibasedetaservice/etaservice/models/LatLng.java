/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A LatLng is a point in geographical coordinates: latitude and longitude.
 * 
 * @author Pajtim Thaqi
 * @version 1.0
 */
@Data
@AllArgsConstructor @NoArgsConstructor
@Embeddable
public class LatLng {
	/**
	 * The latitude in degrees.
	 * 
	 * Latitude is specified in degrees within the range [-90, 90].
	 */
	@Column(name = "LAT", nullable = false)
	private Double lat;
	
	/**
	 * The langitude in degrees.
	 * 
	 * Longitude is specified in degrees within the range [-180, 180].
	 */
	@Column(name = "LNG", nullable = false)
	private Double lng;
}
