/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.mappers;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.siliconeconomy.aibasedetaservice.etaservice.models.RouteSegmentsByTransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportRoute;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.EtaEngineOutput;
import org.springframework.stereotype.Service;

/**
 * Helper class for mapping the {@link EtaEngineOutput} to a {@link TransportRoute}.
 * 
 * @author Pajtim Thaqi
 */
@Service
public class TransportRouteMapper {

	/**
	 * Maps the output of the ETA-Engine to a {@link TransportRoute}.
	 * 
	 * @param output the output as fetched by the ETA-Engine
	 *
	 * @return the {@link TransportRoute}
	 */
	public TransportRoute map(final EtaEngineOutput output) {
		Objects.requireNonNull(output, "The response from the ETA-Engine should not be null");
		final TransportRoute route = new TransportRoute();
		
		route.setSource(output.getSource());
		route.setDestination(output.getDestination());
		
		List<RouteSegmentsByTransportMode> routeSegmentsByType = 
				output.getRouteSegmentsByTransportMode()
				.stream()
				.map(routesByMode -> {
					final RouteSegmentsByTransportMode routesByTransportMode = new RouteSegmentsByTransportMode();
					routesByTransportMode.setTransportMode(routesByMode.getTransportMode());
					routesByTransportMode.setRouteSegments(routesByMode.getRouteSegments());
					routesByTransportMode.setTransportRoute(route);
					return routesByTransportMode;
				}).collect(Collectors.toList());
				
		route.setRouteSegmentsByTransportMode(routeSegmentsByType);
		
		return route;
	}
}
