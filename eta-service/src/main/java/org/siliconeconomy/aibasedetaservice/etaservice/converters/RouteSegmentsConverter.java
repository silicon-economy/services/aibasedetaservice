/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.converters;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter(autoApply = true)
public class RouteSegmentsConverter implements AttributeConverter<Double[][], String> {

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(Double[][] segments) {
		try {
			return objectMapper.writeValueAsString(segments);
		} catch (JsonProcessingException ex) {
			throw new IllegalArgumentException("Cannot convert the route segments to the Database column type.");
		}
	}

	@Override
	public Double[][] convertToEntityAttribute(String dbData) {
		try {
			return objectMapper.readValue(dbData, Double[][].class);
		} catch (IOException ex) {
			throw new IllegalArgumentException(String.format("Cannot convert to entity attribute: %s", dbData));
		}
	}

}
