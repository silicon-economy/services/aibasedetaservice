/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

/**
 * The time corridor for a transport to arrive.
 * 
 * @author Pajtim Thaqi
 */
@Data
@Embeddable
public class TimeCorridor {
	/* The from date for this time corridor. */
	@Column(name = "FROM_TIME", nullable = false)
	private LocalDateTime from;
	
	/* The to date for this time corridor. */
	@Column(name = "TO_TIME", nullable = false)
	private LocalDateTime to;
}
