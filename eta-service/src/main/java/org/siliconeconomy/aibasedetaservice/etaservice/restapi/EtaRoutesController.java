/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.restapi;

import java.util.Optional;

import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.EtaRoutesException;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportRoute;
import org.siliconeconomy.aibasedetaservice.etaservice.services.EtaRoutesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class provides REST end points for the ETA-Routes.
 * 
 * @author Pajtim Thaqi
 * @version 1.0
 */

@RestController
@RequestMapping("/eta-routes")
public class EtaRoutesController {

	private final EtaRoutesService service;

	@Autowired
	public EtaRoutesController(final EtaRoutesService service) {
		this.service = service;
	}

	@GetMapping("/{transportId}")
	public TransportRoute getTransportRoute(@PathVariable final Long transportId) {
		Optional<TransportRoute> transportRoute = service.getTransportRoute(transportId);
		if (transportRoute.isPresent()) {
			return transportRoute.get();
		} else {
			throw new EtaRoutesException(HttpStatus.BAD_REQUEST, transportId);
		}
	}
}
