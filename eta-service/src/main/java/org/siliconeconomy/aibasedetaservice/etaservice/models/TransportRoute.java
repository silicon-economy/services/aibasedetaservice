/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.models;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * A transport route with the geographical positions for the source and
 * destination and the route segments for each transport mode to drive.
 * 
 * @author Pajtim Thaqi
 * @version 1.0
 */
@Data
@Table(name = "T_TRANSPORT_ROUTE")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TransportRoute {
	/* A generated identifier. */
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	@Column(name = "ID")
	private Long id;
	
	/* The source of the transporter. */
	@Embedded
	@AttributeOverride(name = "lat", column = @Column(name = "START_LAT"))
	@AttributeOverride(name = "lng", column = @Column(name = "START_LNG"))
	private LatLng source;
	
	/* The destination of the transporter. */
	@Embedded
	@AttributeOverride(name = "lat", column = @Column(name = "TARGET_LAT"))
	@AttributeOverride(name = "lng", column = @Column(name = "TARGET_LNG"))
	private LatLng destination;
	
	/* The segments to drive separated by the transport modes. */
	@OneToMany(
			mappedBy = "transportRoute",
			cascade = {CascadeType.ALL},
			orphanRemoval = true)
	private Collection<RouteSegmentsByTransportMode> routeSegmentsByTransportMode = new ArrayList<>();
	
	/* The TransportItem where this route belongs to. */
	@OneToOne(mappedBy = "transportRoute")
	@JsonIgnore
	private TransportItem transportItem;
}
