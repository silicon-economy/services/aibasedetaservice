/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.outputs;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;

import lombok.Data;

/**
 * This POJO represents the route segments to drive for a given {@link TransportMode}.
 * 
 * It will be used in the {@link EtaEngineOutput}.
 * 
 * @author Pajtim Thaqi
 */
@Data
public class RouteSegmentsByTransportModeOutput {
	/* The transport mode to use to drive the route segments. */
	private TransportMode transportMode;
	
	/* The calculated route segments to drive by the given transport mode. */
	private Double[][] routeSegments;
}
