/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.inputs;

import java.time.LocalDateTime;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TimeCorridor;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimelinessPriority;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;

import lombok.Data;

/**
 * This DTO is used to transfer input data of a new {@link TransportItem}.
 * 
 * @author Pajtim Thaqi
 */
@Data
public class TransportItemInput {
	/* The client name. */
	private String client;

	/* The timeliness priority of a transport. */
	private TimelinessPriority timelinessPriority;

	/* The source for this transport. */
	private String source;

	/* The transport mode. */
	private TransportMode transportMode;

	/* The destination for this transport. */
	private String destination;

	/* The time when this transport departs. */
	private LocalDateTime departure;

	/* The time corridor for a transport to arrive. */
	private TimeCorridor timeCorridor;
}
