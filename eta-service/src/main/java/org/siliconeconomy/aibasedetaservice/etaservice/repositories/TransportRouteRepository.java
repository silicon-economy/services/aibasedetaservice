/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.repositories;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportRoute;
import org.siliconeconomy.aibasedetaservice.etaservice.services.EtaRoutesService;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface used for database access for the {@link TransportRoute}s. Will be generated and implemented by Spring at runtime and
 * will then contain methods for all CRUD operations, which will be used by the {@link EtaRoutesService}. Does not need
 * to be implemented manually.
 * 
 * @author Pajtim Thaqi
 * @version 1.0
 */
public interface TransportRouteRepository extends JpaRepository<TransportRoute, Long> {
}
