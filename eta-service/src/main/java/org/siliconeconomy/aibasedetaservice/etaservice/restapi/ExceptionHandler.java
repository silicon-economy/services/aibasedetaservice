/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.restapi;

import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.TransportItemNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Generates responses in case an exception occurs during processing the request of a user.
 * It is marked as {@link RestControllerAdvice} so that it will be used by all rest controller classes.
 */
@RestControllerAdvice
public class ExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    /**
     * Signals the user that the transport id supplied in their request is not found.
	 *
     * @param exception the exception causing this ExceptionHandler to be called
     * 
	 * @return message describing the problem
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(TransportItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleTransportItemNotFoundException(final TransportItemNotFoundException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return "Please check you request: " + exception.getMessage();
    }

}
