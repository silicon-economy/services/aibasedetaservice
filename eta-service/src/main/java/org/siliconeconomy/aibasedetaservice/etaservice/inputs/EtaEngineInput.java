/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.inputs;

import java.time.LocalDateTime;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TimeCorridor;

import lombok.Data;

/**
 * This POJO represents the body used in the request for fetching the route and
 * the ETA or ETD, depending on if the {@link EtaEngineInput#getDeparture()} is
 * set or not.
 * 
 * @author Pajtim Thaqi
 */
@Data
public class EtaEngineInput {

	/* The source of the transport. */
	private String source;

	/* The destination of the transport. */
	private String destination;

	/* The time corridor. */
	private TimeCorridor timeCorridor;

	/**
	 * The departure. If this is provided in the request then the ETA will be
	 * returned, otherwise we are calculating the ETD by the engine.
	 */
	private LocalDateTime departure;

}
