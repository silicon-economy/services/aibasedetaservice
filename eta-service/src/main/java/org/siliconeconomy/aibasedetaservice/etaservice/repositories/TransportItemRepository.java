/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.repositories;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.services.EtaTransportsService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Interface used for database access for the {@link TransportItem}s. Will be generated and implemented by Spring at runtime and
 * will then contain methods for all CRUD operations, which will be used by the {@link EtaTransportsService}. Does not need
 * to be implemented manually.
 * 
 * @author Pajtim Thaqi
 * @version 1.0
 */
@Repository
public interface TransportItemRepository extends JpaRepository<TransportItem, Long> {
}
