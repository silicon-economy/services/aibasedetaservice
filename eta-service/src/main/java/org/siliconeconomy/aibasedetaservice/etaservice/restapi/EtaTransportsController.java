/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.restapi;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.TransportItemInput;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.services.EtaTransportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class provides REST end points for the ETA-Transports.
 * 
 * @author Pajtim Thaqi
 * @version 1.0
 */
@RestController
@RequestMapping("/eta-transports")
public class EtaTransportsController {
	
	/* Used to query ETA transports. */
	private final EtaTransportsService service;
	
	/* Used to map input data to DB entities */
	private final ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	public EtaTransportsController(final EtaTransportsService service) {
		this.service = service;
	}
	
	/**
	 * Returns all known transport items.
	 * 
	 * @return a list of {@link TransportItem}s
	 */
	@GetMapping
	public List<TransportItem> getTransportItems() {
		return service.findAll();
	}
	
	/**
	 * Creates a new {@link TransportItem}.
	 * 
	 * @param transportItem a new transport item
	 */
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public TransportItem createTransportItem(@RequestBody final TransportItemInput input) {
		final TransportItem item = modelMapper.map(input, TransportItem.class);
		return this.service.createOrUpdate(item);
	}
	
	/**
	 * Deletes a {@link TransportItem} with the given id.
	 * 
	 * @param transportId the id to delete
	 */
	@DeleteMapping("/{transportId}")
	public void deleteTransportItem(@PathVariable final Long transportId) {
		this.service.delete(transportId);
	}
	
	/**
	 * Updates a {@link TransportItem} for the given transport id.
	 * 
	 * @param transportId the id of the {@link TransportItem} to update
	 * @param input the updated informations
	 */
	@PutMapping("/{transportId}")
	public void updateTransportItem(
			@PathVariable final Long transportId, 
			@RequestBody final TransportItemInput input) {
		final TransportItem updatedTransportItem = modelMapper.map(input, TransportItem.class);
		updatedTransportItem.setTransportId(transportId);
		
		service.createOrUpdate(updatedTransportItem);
	}
	
	/**
	 * Returns a single {@link TransportItem} for the given
	 * transport id or null otherwise if there doesn't exists
	 * any transport item with the id.
	 * 
	 * @param transportId
	 *
	 * @return a {@link TransportItem}
	 */
	@GetMapping("/{transportId}")
	public TransportItem getTranportItemById(@PathVariable final Long transportId) {
		return service.findOne(transportId);
	}
}
