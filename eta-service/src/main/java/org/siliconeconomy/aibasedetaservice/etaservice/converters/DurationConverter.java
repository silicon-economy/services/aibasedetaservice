/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.converters;

import java.time.Duration;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DurationConverter implements AttributeConverter<Duration, Long> {
	@Override
    public Long convertToDatabaseColumn(final Duration duration) {
        return duration == null ? null: duration.toMillis();
    }

    @Override
    public Duration convertToEntityAttribute(Long duration) {
        return duration == null ? null : Duration.ofMillis(duration);
    }
}
