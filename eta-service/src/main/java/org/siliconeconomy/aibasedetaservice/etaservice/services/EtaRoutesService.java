/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.siliconeconomy.aibasedetaservice.etaservice.creators.EtaEngineInputCreator;
import org.siliconeconomy.aibasedetaservice.etaservice.etaclient.EtaRestClient;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.EtaRestClientException;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.EtaRoutesException;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.EtaEngineInput;
import org.siliconeconomy.aibasedetaservice.etaservice.mappers.TransportRouteMapper;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportRoute;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.EtaEngineOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

/**
 * This service offers methods to handle with {@link TransportRoute}s
 * 
 * @author Pajtim Thaqi
 */
@Service
public class EtaRoutesService {

	private final EtaTransportsService transportService;

	private final TransportRouteMapper transportRouteMapper;

	private final EtaRestClient etaRestClient;

	private final EtaEngineInputCreator inputCreator;

	@Autowired
	public EtaRoutesService(final EtaTransportsService transportService,
			final TransportRouteMapper transportRouteMapper, final EtaRestClient etaRestClient,
			final EtaEngineInputCreator inputCreator) {
		this.transportService = transportService;
		this.transportRouteMapper = transportRouteMapper;
		this.etaRestClient = etaRestClient;
		this.inputCreator = inputCreator;
	}

	/**
	 * Returns a {@link TransportRoute} for the given transport id.
	 * 
	 * @param transportId the transport id to fetch the route
	 *
	 * @return a {@link TransportRoute}
	 */
	@Transactional
	public Optional<TransportRoute> getTransportRoute(Long transportId) {
		final TransportItem transport = transportService.findOne(transportId);

		if (transport.getTransportRoute() != null) {
			// if the transport has already a calculated route then return this
			return Optional.of(transport.getTransportRoute());
		}
		// otherwise fetch the route from the ETA-Engine
		final EtaEngineInput input = inputCreator.create(transport);

		// fetch the data from the ETA-Engine
		Optional<EtaEngineOutput> response;
		try {
			response = etaRestClient.fetchResponseFromEtaEngine(input);
		} catch (RestClientException restClientExc) {
			throw new EtaRoutesException(HttpStatus.INTERNAL_SERVER_ERROR, transportId, restClientExc);
		} catch (EtaRestClientException e) {
			transportService.delete(transportId);
			return Optional.empty();
		}
		
		EtaEngineOutput etaEngineResponse;
		if (response.isPresent()) {
			etaEngineResponse = response.get();
		} else {
			transportService.delete(transportId);
			return Optional.empty();
		}

		TransportRoute mappedRoute = transportRouteMapper.map(etaEngineResponse);
		mappedRoute.setTransportItem(transport);

		// set the ETA or ETD on the TransportItem
		transport.setTransportRoute(mappedRoute);
		if (etaEngineResponse.getType() == Type.ETA) {
			transport.setEta(etaEngineResponse.getTimestamp());
		} else if (etaEngineResponse.getType() == Type.ETD) {
			transport.setDeparture(etaEngineResponse.getTimestamp());
			// if the ETD was calculated by the ETA-engine then the ETA has the same value
			// as the TimeCorridor#from.
			transport.setEta(transport.getTimeCorridor().getFrom());
		} else {
			throw new IllegalArgumentException(String.format("Unknown type: %s", etaEngineResponse.getType()));
		}

		// update the existing TransportItem
		transportService.createOrUpdate(transport);

		return Optional.of(transport.getTransportRoute());
	}
}
