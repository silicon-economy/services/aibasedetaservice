/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.etaclient;

import java.util.Optional;

import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.EtaRestClientException;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.EtaEngineInput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.EtaEngineOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.properties.EtaEngineResourceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * This class will be used to fetch the data from the ETA-Engine as provided in
 * the python backend.
 * 
 * @author Pajtim Thaqi
 */
@Service
public class EtaRestClient {
	private RestTemplate restTemplate;
	private final RestTemplateBuilder restTemplateBuilder;

	private final EtaEngineResourceProperties etaEngineResourceProperties;

	@Autowired
	public EtaRestClient(final RestTemplateBuilder builder,
			final EtaEngineResourceProperties etaEngineResourceProperties) {
		this.restTemplateBuilder = builder;
		this.etaEngineResourceProperties = etaEngineResourceProperties;
	}

	/**
	 * Fetches a {@link EtaEngineOutput} from the ETA-Engine for the given
	 * parameters as provided in the {@link EtaEngineInput}.
	 * 
	 * @param input the input request body used to fetch the informations from the
	 *              engine
	 * @throws EtaRestClientException throws an exception with a short message which
	 *                                explains the error
	 * @return
	 */
	public Optional<EtaEngineOutput> fetchResponseFromEtaEngine(final EtaEngineInput input) {
		if (restTemplate == null) {
			this.restTemplate = restTemplateBuilder.build();
		}

		ResponseEntity<EtaEngineOutput> responseEntity = restTemplate.postForEntity(
				etaEngineResourceProperties.getResourceUrl(), new HttpEntity<EtaEngineInput>(input),
				EtaEngineOutput.class);

		if (responseEntity.getStatusCode() != HttpStatus.OK) {
			throw new EtaRestClientException(responseEntity.getStatusCode(), String.format("Request returned status code: %d - %s",
					responseEntity.getStatusCodeValue(), responseEntity.getStatusCode()));
		}

		return Optional.ofNullable(responseEntity.getBody());
	}
}
