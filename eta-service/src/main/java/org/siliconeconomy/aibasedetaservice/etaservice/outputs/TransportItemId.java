/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.outputs;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This DTO will be used as response when a new {@link TransportItem} will
 * be created.
 * 
 * @author Pajtim Thaqi
 */
@Data
@AllArgsConstructor
public class TransportItemId {
	/* The transport id of the newly created Transport item */
	private Long transportId;
}
