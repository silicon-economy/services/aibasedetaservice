/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.models;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.siliconeconomy.aibasedetaservice.etaservice.converters.RouteSegmentsConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Represents the route segments for a given {@link TransportMode}.
 * 
 * @author Pajtim Thaqi
 */
@Data
@Entity
@Table(name = "T_ROUTE_SEGMENTS_BY_TRANSPORT_MODE")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RouteSegmentsByTransportMode {
	/* A generated identifier. */
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TRANSPORT_ROUTE_ID", referencedColumnName = "ID", nullable = false)
	@JsonIgnore
	private TransportRoute transportRoute;
	
	/* The transport mode. */
	@Column(name = "TRANSPORT_MODE", nullable = false)
	@Enumerated(EnumType.STRING)
	private TransportMode transportMode;
	
	/* The segments of the route to drive. */
	@Column(name = "ROUTE_SEGMENTS", columnDefinition = "TEXT")
	@Convert(converter = RouteSegmentsConverter.class)
	private Double[][] routeSegments;
}
