/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.creators;

import java.util.Objects;

import org.siliconeconomy.aibasedetaservice.etaservice.inputs.EtaEngineInput;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.springframework.stereotype.Service;

/**
 * Helper class for creating a {@link EtaEngineInput} from a {@link TransportItem}.
 * 
 * @author Pajtim Thaqi
 */
@Service
public class EtaEngineInputCreator {

	/**
	 * Creates a {@link EtaEngineInput} from a {@link TransportItem}.
	 * 
	 * @param transport the transport item
	 *
	 * @return the newly created {@link EtaEngineInput}
	 */
	public EtaEngineInput create(TransportItem transport) {
		Objects.requireNonNull(transport, "TransportItem should not be null.");
		
		final EtaEngineInput input = new EtaEngineInput();
		input.setSource(transport.getSource());
		input.setDestination(transport.getDestination());
		input.setDeparture(transport.getDeparture());
		input.setTimeCorridor(transport.getTimeCorridor());
		
		return input;
	}

}
