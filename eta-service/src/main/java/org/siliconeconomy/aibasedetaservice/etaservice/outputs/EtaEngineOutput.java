/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.outputs;

import java.time.LocalDateTime;
import java.util.List;

import org.siliconeconomy.aibasedetaservice.etaservice.models.LatLng;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * This POJO represents the response of the ETA-Engine. It will be used
 * to encapsulate the JSON response in an entity.
 * 
 * @author Pajtim Thaqi
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EtaEngineOutput {
	
	/* The source of the transporter. */
	private LatLng source;
	
	/* The destination of the transporter. */
	private LatLng destination;
	
	/* The segments to drive separated by the transport modes. */
	private List<RouteSegmentsByTransportModeOutput> routeSegmentsByTransportMode;
	
	/* This timestamp represents the ETA or the ETD depending on the provided type. */
	private LocalDateTime timestamp;
	
	/* The type which specifies how to interpret the attribute: timestamp. */
	private Type type;
}
