/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.outputs;

/**
 * An enum used in the {@link EtaEngineOutput}.
 * 
 * This type represents the interpretations of the
 * {@link EtaEngineOutput#getTimestamp()}.
 * 
 * If the {@link EtaEngineOutput#getType()} is of type {@link Type#ETA} then the
 * {@link EtaEngineOutput#getTimestamp()} represents the calculated ETA by the
 * ETA-Engine, otherwise it represents the ETD.
 * 
 * @author Pajtim Thaqi
 */
public enum Type {
	ETA, ETD;
}
