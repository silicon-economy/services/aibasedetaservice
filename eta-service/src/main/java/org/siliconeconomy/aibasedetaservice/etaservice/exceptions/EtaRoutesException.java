/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.exceptions;

import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception that will be thrown if something went wrong while fetching data
 * from the ETA-Engine for a given {@link TransportItem#getTransportId()}.
 * 
 * @author Pajtim Thaqi
 */
public class EtaRoutesException extends ResponseStatusException {
	private static final long serialVersionUID = -4714374403050675216L;
	
	private final Long transportItemId;
	
	/**
	 * Exception that will be thrown if something went wrong while fetching data
	 * from the ETA-Engine for a given {@link TransportItem#getTransportId()}.
	 * 
	 * @param status the http status
	 * @param item the transport item for which the error occurs
	 * @param cause the cause for this exception
	 */
	public EtaRoutesException(final HttpStatus status, final Long transportItemId, final Throwable cause) {
		super(status, null, cause);
		this.transportItemId = transportItemId;
	}
	
	/**
	 * Exception that will be thrown if something went wrong while fetching data
	 * from the ETA-Engine for a given {@link TransportItem#getTransportId()}.
	 * 
	 * @param status the http status
	 * @param item the transport item for which the error occurs
	 */
	public EtaRoutesException(final HttpStatus status, final Long transportItemId) {
		super(status);
		this.transportItemId = transportItemId;
	}
	
	@Override
	public String getMessage() {
		return String.format("Cannot fetch any data from the ETA-Engine for the transport with id: %d",
				transportItemId);
	}
}
