/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.services;

import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.TransportItemNotFoundException;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.repositories.TransportItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service offers methods to handle with {@link TransportItem}s on a Repository.
 * 
 * @author Pajtim Thaqi
 */
@Service
public class EtaTransportsService {
    private final TransportItemRepository repository;
	
	/**
	 * The constructor for the {@link EtaTransportsService}.
	 */
	@Autowired
	public EtaTransportsService(final TransportItemRepository repository) {
		this.repository = repository;
	}

	/**
	 * Queries a specific transport by their id.
	 * 
	 * @param transportId the transport id
	 *
	 * @return the {@link TransportItem} if a valid id was supplied
	 *
	 * @throws TransportItemNotFoundException if the transport id is unknown
	 */
	public TransportItem findOne(Long transportId) {
		return repository.findById(transportId).orElseThrow(
				() -> new TransportItemNotFoundException(transportId));
	}

	/**
     * Queries all registered transports.
     * 
     * @return a list of {@link TransportItem}s
     */
	public List<TransportItem> findAll() {
		return repository.findAll();
	}
	
	/**
	 * Creates or updates a {@link TransportItem}.
	 * 
	 * @param item the {@link TransportItem} to create or update
	 *
	 * @return the newly created {@link TransportItem}
	 */
	@Transactional(value = TxType.REQUIRED)
	public TransportItem createOrUpdate(final TransportItem item) {
		return this.repository.save(item);
	}
	
	/**
	 * Deletes a {@link TransportItem} for a given transport id.
	 * 
	 * @param transportID the transport id to delete
	 */
	@Transactional(value = TxType.REQUIRED)
	public void delete(final Long transportID) {
		this.repository.deleteById(transportID);
	}
	
}
