/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.exceptions;

import org.siliconeconomy.aibasedetaservice.etaservice.etaclient.EtaRestClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception that is thrown while trying to fetch data from the ETA-Engine via
 * the {@link EtaRestClient}.
 * 
 * @author Pajtim Thaqi
 */
public class EtaRestClientException extends ResponseStatusException {
	private static final long serialVersionUID = 4515733359413799437L;

	/**
	 * Exception that is thrown while trying to fetch data from the ETA-Engine via
	 * the {@link EtaRestClient}.
	 */
	public EtaRestClientException(final HttpStatus status, final String message) {
		super(status, message);
	}
}
