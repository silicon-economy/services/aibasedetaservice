/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TransportItemNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -4119018712176254553L;
	
	private final Long transportId;
	
	public TransportItemNotFoundException(final Long transportId) {
		this.transportId = transportId;
	}
	
	/**
	 * The transport Id.
	 * 
	 * @return the transport id
	 */
	public Long getTransportId() {
		return transportId;
	}
	
	@Override
	public String getMessage() {
		return String.format("There doesn't exists a transport item for the id: %d", transportId);
	}
}
