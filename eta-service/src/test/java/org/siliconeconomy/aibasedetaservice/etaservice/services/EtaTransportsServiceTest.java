/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.TransportItemNotFoundException;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimeCorridor;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimelinessPriority;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.repositories.TransportItemRepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EtaTransportsServiceTest {
	
	@InjectMocks
	private EtaTransportsService service;
	
	@Mock
	private TransportItemRepository repository;
	
	@BeforeEach
	public void setUp() {
		
	}
	
	@Test
	public void findOne() {
		// arrange
		final TransportItem item = getEtaTransport();
		when(repository.findById(anyLong()))
		.thenReturn(Optional.of(item));

		// act
		final TransportItem expected = service.findOne(1231L);

		// assert
		assertThat(expected).isEqualToComparingFieldByField(item);
	}
	
	@Test
	public void findAll() {
		// arrange
		final List<TransportItem> items = Arrays.asList(getEtaTransport());
		
		when(repository.findAll()).thenReturn(items);
		
		// act
		List<TransportItem> all = service.findAll();
		
		// assert
		assertThat(all)
		.containsExactlyInAnyOrderElementsOf(items);
	}
	
	@Test
	public void insert() {
		// arrange
		final TransportItem item = getEtaTransport();
		when(repository.save(item)).thenReturn(item);
		
		// act
		service.createOrUpdate(item);
		
		// assert
		verify(repository, times(1)).save(item);
	}
	
	@Test
	public void delete() {
		// arrange
		final Long id = 1L;
		
		// act
		service.delete(id);
		
		// assert
		verify(repository, times(1)).deleteById(1L);
	}
	
	@Test
	public void findOne_throws_exception() {
		// act
		final Throwable exception = catchThrowable(() -> service.findOne(12345L));
		
		// assert
		assertThat(exception).isInstanceOf(TransportItemNotFoundException.class)
			.hasMessage("There doesn't exists a transport item for the id: 12345");
	}
	
	private static TransportItem getEtaTransport() {
		final TransportItem item = new TransportItem();
		item.setTransportId(1231L);
		item.setClient("Alpha");
		item.setTimelinessPriority(TimelinessPriority.LOW);
		item.setSource("München");
		item.setTransportMode(TransportMode.ROAD);
		item.setDestination("Dortmund");
		item.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		item.setEta(LocalDateTime.of(2020, 9, 4, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		item.setTimeCorridor(timeCorridor);
		return item;
	}
	
}
