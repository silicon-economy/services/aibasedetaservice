/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.converters;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DurationConverterTest {
    
    /* class under test */
    @InjectMocks
    private DurationConverter durationConverter;
    
    /* parameters */
    private Duration duration;
    private Long durationLong;
    
    @Test
    public void convertToDatabaseColumn() {
        // arrange
        duration = Duration.ofSeconds(10);
        Long expectedResult = duration.toMillis();
        
        // act
        Long result = durationConverter.convertToDatabaseColumn(duration);
        
        // assert
        assertThat(result).isEqualTo(expectedResult);
    }
    
    @Test
    public void convertToDatabaseColumn_null() {
        // act
        Long result = durationConverter.convertToDatabaseColumn(null);
        
        // assert
        assertThat(result).isNull();
    }
    
    @Test
    public void convertToEntityAttribute() {
        // arrange
        durationLong = 100l;
        Duration expectedResult = Duration.ofMillis(durationLong);
        
        // act
        Duration result = durationConverter.convertToEntityAttribute(durationLong);
        
        // assert
        assertThat(result).isEqualTo(expectedResult);
    }
    
    @Test
    public void convertToEntityAttribute_null() {
        // act
        Duration result = durationConverter.convertToEntityAttribute(null);
        
        // assert
        assertThat(result).isNull();
    }

}
