/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.TransportItemNotFoundException;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.TransportItemInput;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimeCorridor;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimelinessPriority;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.services.EtaTransportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = { EtaTransportsController.class, EtaTransportsService.class, TransportItem.class,
		ExceptionHandler.class })
@EnableWebMvc
public class EtaTransportsControllerTest {

	private MockMvc mockMvc;

	@MockBean
	private EtaTransportsService etaService;

	@MockBean
	private ModelMapper mapper;

	@Autowired
	@InjectMocks
	private EtaTransportsController etaServiceRestController;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	public void init() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(etaServiceRestController)
				.setControllerAdvice(new ExceptionHandler()).build();
		MockitoAnnotations.initMocks(this);

		objectMapper.registerModule(new JavaTimeModule());
	}

	@Test
	public void queryEtaTransports() throws Exception {
		// arrange
		when(etaService.findAll()).thenReturn(Collections.singletonList(getEtaTransport()));

		// act & assert
		mockMvc.perform(MockMvcRequestBuilders.get("/eta-transports")).andDo(mvcResult -> {
			String response = mvcResult.getResponse().getContentAsString();
			TransportItem[] transports = objectMapper.readValue(response, TransportItem[].class);
			Assertions.assertEquals(1, transports.length);
			Assertions.assertEquals(getEtaTransport(), transports[0]);
		});

	}

	@Test
	public void querySingleEtaTransport() throws Exception {
		// arrange
		when(etaService.findOne(anyLong())).thenReturn(getEtaTransport());

		// act & assert
		mockMvc.perform(MockMvcRequestBuilders.get("/eta-transports/1231")).andExpect(status().isOk())
				.andDo(mvcResult -> {
					String response = mvcResult.getResponse().getContentAsString();
					TransportItem item = objectMapper.readValue(response, TransportItem.class);
					Assertions.assertEquals(getEtaTransport(), item);
				});
	}

	@Test
	public void querySingleEtaTransport_invalidId() throws Exception {
		when(etaService.findOne(anyLong())).thenThrow(new TransportItemNotFoundException(anyLong()));

		mockMvc.perform(MockMvcRequestBuilders.get("/eta-transports/123")).andExpect(status().isNotFound());
	}

	@Test
	public void crateTransportItem() {
		// arrange
		final TransportItemInput input = new TransportItemInput();
		input.setClient("Alpha");
		input.setSource("München");
		input.setDestination("Dortmund");
		input.setTransportMode(TransportMode.ROAD);
		input.setTimelinessPriority(TimelinessPriority.LOW);
		input.setDeparture(LocalDateTime.of(2020, 9, 1, 11, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 3, 9, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 7, 12, 0));
		input.setTimeCorridor(timeCorridor);

		when(mapper.map(input, TransportItem.class)).thenReturn(getEtaTransport());

		// act
		etaServiceRestController.createTransportItem(input);

		// assert
		verify(etaService, times(1)).createOrUpdate(getEtaTransport());
	}

	@Test
	public void updateTransportItem() {
		// arrange
		final TransportItemInput input = new TransportItemInput();
		input.setClient("Alpha");
		input.setSource("Berlin");
		input.setDestination("Dortmund");
		input.setTransportMode(TransportMode.RAIL);
		input.setTimelinessPriority(TimelinessPriority.LOW);
		input.setDeparture(LocalDateTime.of(2020, 9, 1, 11, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 3, 9, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 7, 12, 0));
		input.setTimeCorridor(timeCorridor);
		
		TransportItem etaTransport = getEtaTransport();
		etaTransport.setTransportId(1L);

		when(mapper.map(input, TransportItem.class)).thenReturn(getEtaTransport());

		// act
		etaServiceRestController.updateTransportItem(1L, input);

		// assert
		verify(etaService, times(1)).createOrUpdate(etaTransport);
	}

	@Test
	public void deleteTransportItemById() {
		// act
		etaServiceRestController.deleteTransportItem(1L);

		// assert
		verify(etaService, times(1)).delete(1L);
	}
	
	private static TransportItem getEtaTransport() {
		final TransportItem item = new TransportItem();
		item.setClient("Alpha");
		item.setTimelinessPriority(TimelinessPriority.LOW);
		item.setSource("München");
		item.setTransportMode(TransportMode.ROAD);
		item.setDestination("Dortmund");
		item.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		item.setEta(LocalDateTime.of(2020, 9, 4, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		item.setTimeCorridor(timeCorridor);
		return item;
	}
}
