/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.converters;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class RouteSegmentsConverterTest {

	/* class under test */
	@InjectMocks
	private RouteSegmentsConverter converter;
	
	/* parameters */
	private Double[][] segments;
	private String dbData;
	
	@BeforeEach
	public void setUp() {
		segments = new Double[][] {
			{48.13255, 11.52886}, 
			{48.13249, 11.52894}, 
			{48.13239, 11.52908}, 
			{48.13235, 11.52914}, 
			{48.13231, 11.52919}, 
			{48.13223,11.52931}};
		dbData = "[[48.13255,11.52886],[48.13249,11.52894],[48.13239,11.52908],[48.13235,11.52914],[48.13231,11.52919],[48.13223,11.52931]]";
	}
	
	@Test
	public void convertToDatabaseColumn() {
		// act
		final String result = converter.convertToDatabaseColumn(segments);
		
		// assert
		assertThat(result).isEqualTo(dbData);
	}
	
	@Test
	public void convertToEntityAttribute() {
		// act
		final Double[][] result = converter.convertToEntityAttribute(dbData);
		
		// assert
		assertThat(result).isEqualTo(segments);
	}
}
