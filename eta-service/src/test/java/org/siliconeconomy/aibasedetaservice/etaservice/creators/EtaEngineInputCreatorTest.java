/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.creators;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.EtaEngineInput;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimeCorridor;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimelinessPriority;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportItem;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;

@ExtendWith(MockitoExtension.class)
public class EtaEngineInputCreatorTest {
	
	@InjectMocks
	private EtaEngineInputCreator creator;
	
	
	/* test parameters */
	private TransportItem item;
	
	@BeforeEach
	public void setUp() {
		item = new TransportItem();
		item.setClient("Alpha");
		item.setTimelinessPriority(TimelinessPriority.LOW);
		item.setSource("München");
		item.setTransportMode(TransportMode.ROAD);
		item.setDestination("Dortmund");
		item.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		item.setEta(LocalDateTime.of(2020, 9, 4, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		item.setTimeCorridor(timeCorridor);
	}
	
	@Test
	public void create() {
		// arrange
		final EtaEngineInput expected = new EtaEngineInput();
		expected.setSource("München");
		expected.setDestination("Dortmund");
		expected.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		expected.setTimeCorridor(timeCorridor);
		
		// act
		final EtaEngineInput input = creator.create(item);
		
		// assert
		assertThat(input).isEqualTo(expected);
	}
}
