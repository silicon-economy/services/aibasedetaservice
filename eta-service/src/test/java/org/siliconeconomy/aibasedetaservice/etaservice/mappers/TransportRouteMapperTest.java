/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.mappers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.aibasedetaservice.etaservice.models.LatLng;
import org.siliconeconomy.aibasedetaservice.etaservice.models.RouteSegmentsByTransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportRoute;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.EtaEngineOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.RouteSegmentsByTransportModeOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.Type;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class TransportRouteMapperTest {
	/* class under test */
	@InjectMocks
	private TransportRouteMapper routeMapper;
	
	
	/* test parameters */
	private EtaEngineOutput output;
	
	@BeforeEach
	public void setUp() {
		output = new EtaEngineOutput();
		output.setSource(new LatLng(1d, 1d));
		output.setDestination(new LatLng(2d, 2d));
		output.setTimestamp(LocalDateTime.of(2020, 9, 2, 14, 0));
		output.setType(Type.ETA);
		RouteSegmentsByTransportModeOutput routeSegments = new RouteSegmentsByTransportModeOutput();
		routeSegments.setTransportMode(TransportMode.ROAD);
		routeSegments.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		output.setRouteSegmentsByTransportMode(Arrays.asList(routeSegments));
	}
	
	@Test
	public void map() {
		// arrange
		final TransportRoute expected = new TransportRoute();
		expected.setSource(new LatLng(1d, 1d));
		expected.setDestination(new LatLng(2d, 2d));
		RouteSegmentsByTransportMode routeSegments = new RouteSegmentsByTransportMode();
		routeSegments.setTransportMode(TransportMode.ROAD);
		routeSegments.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		expected.setRouteSegmentsByTransportMode(Arrays.asList(routeSegments));
		
		// act
		TransportRoute mappedRoute = routeMapper.map(output);
		
		// assert
		assertThat(mappedRoute.getSource()).isEqualTo(expected.getSource());
		assertThat(mappedRoute.getDestination()).isEqualTo(expected.getDestination());
	}
}
