/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.restapi;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.TransportItemNotFoundException;
import org.siliconeconomy.aibasedetaservice.etaservice.models.LatLng;
import org.siliconeconomy.aibasedetaservice.etaservice.models.RouteSegmentsByTransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportRoute;
import org.siliconeconomy.aibasedetaservice.etaservice.services.EtaRoutesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class EtaRoutesControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private EtaRoutesService service;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	public void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
	}

	@Test
	public void queryTransportRouteById() throws Exception {
		// arrange
		when(service.getTransportRoute(123L)).thenReturn(Optional.of(getTransportRoute()));
		
		// act & assert
		mockMvc.perform(MockMvcRequestBuilders.get("/eta-routes/123"))
                .andExpect(status().isOk())
                .andDo(mvcResult -> {
                    String response = mvcResult.getResponse().getContentAsString();
                    TransportRoute route = objectMapper.readValue(response, TransportRoute.class);
                    Assertions.assertEquals(getTransportRoute(), route);
                });
	}
	
	@Test
    public void querySingleEtaTransport_invalidId() throws Exception {
        when(service.getTransportRoute(1231L)).thenThrow(new TransportItemNotFoundException(1231L));

        mockMvc.perform(MockMvcRequestBuilders.get("/eta-routes/1231"))
                .andExpect(status().isNotFound());
    }
	
	
	private TransportRoute getTransportRoute() {
		final TransportRoute route = new TransportRoute();
		route.setSource(new LatLng(51d, 7d));
		route.setDestination(new LatLng(48d, 11d));
		final RouteSegmentsByTransportMode segmentsByTransportMode = new RouteSegmentsByTransportMode();
		segmentsByTransportMode.setTransportMode(TransportMode.ROAD);
		segmentsByTransportMode.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		segmentsByTransportMode.setTransportRoute(route);
		route.setRouteSegmentsByTransportMode(Arrays.asList(segmentsByTransportMode));
		return route;
	}
}
