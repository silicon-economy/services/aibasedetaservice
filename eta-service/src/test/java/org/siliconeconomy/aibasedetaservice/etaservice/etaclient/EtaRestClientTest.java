/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.etaclient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.EtaEngineInput;
import org.siliconeconomy.aibasedetaservice.etaservice.models.LatLng;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TimeCorridor;
import org.siliconeconomy.aibasedetaservice.etaservice.models.TransportMode;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.EtaEngineOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.RouteSegmentsByTransportModeOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.Type;
import org.siliconeconomy.aibasedetaservice.etaservice.properties.EtaEngineResourceProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class EtaRestClientTest {
	
	/* class under test */
	@InjectMocks
	private EtaRestClient client;
	
	/* dependencies */
	@Mock
	private RestTemplateBuilder builder;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Mock
	private EtaEngineResourceProperties properties;
	
	@Mock
	private ResponseEntity<EtaEngineOutput> output;
	
	/* test parameters */
	private static final String RESOURCE_URL = "http://localhost:5000/api/v1/eta-engine";
	
	private EtaEngineInput input;
	private EtaEngineOutput response;
	
	@BeforeEach
	public void setUp() {
		// arrange
		input = new EtaEngineInput();
		input.setSource("München");
		input.setDestination("Dortmund");
		input.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		input.setTimeCorridor(timeCorridor);
		
		response = new EtaEngineOutput();
		response.setDestination(new LatLng(1d, 1d));
		response.setSource(new LatLng(2d, 2d));
		response.setTimestamp(LocalDateTime.of(2020, 9, 2, 14, 0));
		response.setType(Type.ETA);
		RouteSegmentsByTransportModeOutput routeSegments = new RouteSegmentsByTransportModeOutput();
		routeSegments.setTransportMode(TransportMode.ROAD);
		routeSegments.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		response.setRouteSegmentsByTransportMode(Arrays.asList(routeSegments));
		
		
		when(output.getStatusCode()).thenReturn(HttpStatus.OK);
		when(output.getBody()).thenReturn(response);
		
		when(builder.build()).thenReturn(restTemplate);
		
		when(properties.getResourceUrl()).thenReturn(RESOURCE_URL);
		
		when(restTemplate.postForEntity(
				RESOURCE_URL, 
				new HttpEntity<EtaEngineInput>(input),
				EtaEngineOutput.class)).thenReturn(output);
	}
	
	@Test
	public void fetchResponseFromEtaEngine() {
		// arrange
		final EtaEngineOutput expected = new EtaEngineOutput();
		expected.setDestination(new LatLng(1d, 1d));
		expected.setSource(new LatLng(2d, 2d));
		expected.setTimestamp(LocalDateTime.of(2020, 9, 2, 14, 0));
		expected.setType(Type.ETA);
		RouteSegmentsByTransportModeOutput routeSegments = new RouteSegmentsByTransportModeOutput();
		routeSegments.setTransportMode(TransportMode.ROAD);
		routeSegments.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		expected.setRouteSegmentsByTransportMode(Arrays.asList(routeSegments));
		
		
		// act
		Optional<EtaEngineOutput> response = client.fetchResponseFromEtaEngine(input);
		EtaEngineOutput etaEngineOutput = response.get();
		
		// assert
		assertThat(response).isNotEmpty();
		assertThat(etaEngineOutput).isEqualTo(expected);
	}
}
