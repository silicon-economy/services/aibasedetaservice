/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

package org.siliconeconomy.aibasedetaservice.etaservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.aibasedetaservice.etaservice.creators.EtaEngineInputCreator;
import org.siliconeconomy.aibasedetaservice.etaservice.etaclient.EtaRestClient;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.EtaRestClientException;
import org.siliconeconomy.aibasedetaservice.etaservice.exceptions.EtaRoutesException;
import org.siliconeconomy.aibasedetaservice.etaservice.inputs.EtaEngineInput;
import org.siliconeconomy.aibasedetaservice.etaservice.mappers.TransportRouteMapper;
import org.siliconeconomy.aibasedetaservice.etaservice.models.*;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.EtaEngineOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.RouteSegmentsByTransportModeOutput;
import org.siliconeconomy.aibasedetaservice.etaservice.outputs.Type;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EtaRoutesServiceTest {

	@InjectMocks
	private EtaRoutesService routesService;
	
	@Mock
	private EtaTransportsService transportService;
	
	@Mock
	private TransportRouteMapper transportRouteMapper;
	
	@Mock
	private EtaEngineInputCreator inputCreator;
	
	@Mock
	private EtaRestClient etaRestClient;
	
	/* test parameters */
	private EtaEngineInput input;
	private EtaEngineOutput output;
	private TransportRoute expected;
	private TransportItem item;
	
	@BeforeEach
	public void setUp() {
		// EtaEngineInput
		input = new EtaEngineInput();
		input.setSource("München");
		input.setDestination("Dortmund");
		input.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		input.setTimeCorridor(timeCorridor);
		
		// TransportItem
		item = getEtaTransport();
		
		// EtaEngineOutput
		output = new EtaEngineOutput();
		output.setSource(new LatLng(1d, 1d));
		output.setDestination(new LatLng(2d, 2d));
		output.setType(Type.ETA);
		output.setTimestamp(LocalDateTime.of(2020, 9, 2, 10, 0));
		List<RouteSegmentsByTransportModeOutput> routeSegmentsByTransportMode = new ArrayList<>();
		RouteSegmentsByTransportModeOutput routeSegmentOutput = new RouteSegmentsByTransportModeOutput();
		routeSegmentOutput.setTransportMode(TransportMode.ROAD);
		routeSegmentOutput.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		output.setRouteSegmentsByTransportMode(routeSegmentsByTransportMode);
		
		when(transportService.findOne(1231L)).thenReturn(item);
		Mockito.lenient().when(inputCreator.create(item)).thenReturn(input);
		Mockito.lenient().when(etaRestClient.fetchResponseFromEtaEngine(input)).thenReturn(Optional.of(output));
		
		expected = new TransportRoute();
		expected.setSource(new LatLng(1d, 1d));
		expected.setDestination(new LatLng(2d, 2d));
		expected.setTransportItem(item);
		
		List<RouteSegmentsByTransportMode> routeModes = new ArrayList<>();
		RouteSegmentsByTransportMode routeMode = new RouteSegmentsByTransportMode();
		routeMode.setTransportMode(TransportMode.ROAD);
		routeMode.setTransportRoute(expected);
		routeMode.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		expected.setRouteSegmentsByTransportMode(routeModes);
	}
	
	@Test
	public void getTransportRoute() {
		// arrange
		when(transportRouteMapper.map(output)).thenReturn(expected);

		// act
		routesService.getTransportRoute(1231L);
		
		// assert
		verify(inputCreator).create(item);
		verify(etaRestClient).fetchResponseFromEtaEngine(input);
		verify(transportRouteMapper).map(output);
		
		assertThat(item.getEta()).isNotNull();
	}
	
	@Test
	public void getTransportRoute_return_already_calculated_route() {
		// arrange
		TransportRoute route = new TransportRoute();
		route.setId(1L);
		route.setSource(new LatLng(1d, 1d));
		route.setDestination(new LatLng(2d, 2d));
		route.setTransportItem(item);
		
		List<RouteSegmentsByTransportMode> routeModes = new ArrayList<>();
		RouteSegmentsByTransportMode routeMode = new RouteSegmentsByTransportMode();
		routeMode.setTransportMode(TransportMode.ROAD);
		routeMode.setTransportRoute(expected);
		routeMode.setRouteSegments(new Double[][] {{1d,2d,3d}, {3d,2d,1d}});
		route.setRouteSegmentsByTransportMode(routeModes);
		
		item.setTransportRoute(route);
		
		// act
		Optional<TransportRoute> transportRoute = routesService.getTransportRoute(1231L);
		
		// assert
		verify(inputCreator, times(0)).create(item);
		verify(etaRestClient, times(0)).fetchResponseFromEtaEngine(input);
		assertThat(transportRoute)
			.isNotEmpty()
			.contains(route);
	}
	
	@Test
	public void getTransportRoute_return_empty_optional_delete_transport() {
		// arrange
		item.setSource("Blablub");
		when(etaRestClient.fetchResponseFromEtaEngine(input)).thenReturn(Optional.empty());
		
		// act
		routesService.getTransportRoute(1231L);
		
		// assert
		verify(transportService, times(0)).createOrUpdate(item);
		verify(transportService, times(1)).delete(1231L);
	}
	
	@Test
	public void getTransportRoute_throw_RestClientException() {
		// arrange
		when(etaRestClient.fetchResponseFromEtaEngine(input)).thenThrow(new RestClientException("Any reason."));
		
		// act & assert
		assertThatThrownBy(() -> {
			routesService.getTransportRoute(1231L);
		}).isInstanceOf(EtaRoutesException.class)
				.hasMessage("Cannot fetch any data from the ETA-Engine for the transport with id: 1231");
	}
	
	@Test
	public void getTransportRoute_throw_EtaRestClientException_return_empty_optional() {
		// arrange
		when(etaRestClient.fetchResponseFromEtaEngine(input)).thenThrow(new EtaRestClientException(HttpStatus.BAD_REQUEST, "Any reason."));
		
		// act
		Optional<TransportRoute> transportRoute = routesService.getTransportRoute(1231L);
		
		// assert
		verify(transportService, times(0)).createOrUpdate(item);
		verify(transportService, times(1)).delete(1231L);
		assertThat(transportRoute).isEmpty();
	}
	
	@Test
	public void getTransportRoute_unknown_type() {
		// arrange
		when(transportRouteMapper.map(output)).thenReturn(expected);
		output.setType(null);
		
		// act & assert
		assertThatThrownBy(() -> {
			routesService.getTransportRoute(1231L);
		}).isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Unknown type: null");
	}
	
	private static TransportItem getEtaTransport() {
		final TransportItem item = new TransportItem();
		item.setTransportId(1231L);
		item.setClient("Alpha");
		item.setTimelinessPriority(TimelinessPriority.LOW);
		item.setSource("München");
		item.setTransportMode(TransportMode.ROAD);
		item.setDestination("Dortmund");
		item.setDeparture(LocalDateTime.of(2020, 9, 2, 10, 0));
		TimeCorridor timeCorridor = new TimeCorridor();
		timeCorridor.setFrom(LocalDateTime.of(2020, 9, 4, 10, 0));
		timeCorridor.setTo(LocalDateTime.of(2020, 9, 5, 11, 0));
		item.setTimeCorridor(timeCorridor);
		return item;
	}
}
