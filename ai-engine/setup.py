#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
""" Setup of the AI engine. """

from setuptools import setup

setup(
    name='aibasedetaservice.ai-engine',
    version='1.0.0',
    packages=['tests', 'ai_engine', 'ai_engine.api'],
    url='https://www.siliconeconomy.org',
    license='Open Logistics License 1.0',
    author='Kai Hannemann',
    author_email='kai.hannemann@iml.fraunhofer.de',
    description='The backend for the AI-Engine.',
    install_requires=['Flask',
                      'Flask-Cors',
                      'flask-restx',
                      'openpyxl'],
    entry_points={
        "console_scripts": [
            'server=ai_engine:server'
        ]
    }
)
