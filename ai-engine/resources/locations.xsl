<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:pub="http://datex2.eu/schema/2/2_0">
  <xsl:output method="text"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="pub:d2LogicalModel">
    <xsl:apply-templates select="pub:payloadPublication"/>
  </xsl:template>

  <xsl:template match="pub:payloadPublication">
    <!-- HEADERS -->
    <xsl:text>predefinedLocationReference,carriageway,lane,latitude,longitude</xsl:text>
    <xsl:text>&#xa;</xsl:text>     <!-- LINE BREAK -->
    <xsl:apply-templates select="pub:predefinedLocationContainer"/>
  </xsl:template>

  <xsl:template match="pub:predefinedLocation">
    <!-- ROWS -->
    <xsl:value-of select="concat(@id,',',
                                 descendant::pub:carriageway,',',
                                 descendant::pub:lane,',',
                                 descendant::pub:latitude,',',
                                 descendant::pub:longitude)"/>
    <xsl:text>&#xa;</xsl:text>     <!-- LINE BREAK -->
  </xsl:template>

</xsl:stylesheet>
