#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

""" Add missing module documentation. """
from flask import request
from flask_restx import Namespace, Resource
from ai_engine.estimation_unit.estimation_unit import get_estimation

api = Namespace('ai-engine', description="The AI engine")


@api.route('')
class AIEngine(Resource):
    def post(self):
        return get_estimation(request.get_json(force=True))
