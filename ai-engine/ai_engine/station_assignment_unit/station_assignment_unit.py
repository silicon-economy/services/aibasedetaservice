#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function that adds a new column
to a data frame containing qv stations with locations.
"""

import ast
import functools
import math
import geopandas
import numpy as np
import pandas as pd
from shapely.geometry import Point, LineString
from shapely.ops import polygonize

import ai_engine.database_connector as db_con
import ai_engine.utils.dataframe_helper as df_helper
import ai_engine.data_preprocessing_unit.road_partitioner as road_partition

__QV_STATIONS = pd.DataFrame()


# MAIN -----------------------------------------------------------------------------
def assign_station_references(route_df):
    """
    Generating a list of road sections in form of a dataframe.
    Each section is a continuous part of one street on the route.
    Returns a DataFrame that represents the given route enriched with geolocations and assigned qvw-stations, where
    possible.

    :param route_df: Dataframe containing names of the different streets in a route.
    :type route_df: DataFrame

    :return: The complete route as a dataframe with qvw-stations mapped to each segment, where available.
    """
    final_route_df = pd.DataFrame()
    road_sections = get_continuous_road_sections(route_df)

    for section in road_sections:
        # Adding a column with geo coordinates and assigning qv-stations to section_df where possible.
        # Section_df will be added to the final_route_df then.
        # This might be a full section without any station assigned, if not available.
        section_df = section
        section_df = add_geo_coordinates(section_df)
        section_df = replace_section_parts_with_qv_matching(section_df)

        final_route_df = final_route_df.append(section_df)

    return final_route_df


# MAIN -----------------------------------------------------------------------------


def add_geo_coordinates(dataframe):
    """
    Adds two columns of geo coordinates to a dataframe that contains two columns
    of segment coordinates (from and to) as strings.

    :param dataframe: Dataframe containing string formatted segment coordinates.
    :type dataframe: Dataframe

    :return: The initial dataframe with the corresponding geo coordinates added to it.
    """
    coordinates = []
    for i in dataframe[['SegFrom', 'SegTo']].iterrows():
        coordinates.append([i[0], ast.literal_eval(str(i[1]['SegFrom'])), ast.literal_eval(str(i[1]['SegTo']))])

    coordinates_df = pd.DataFrame(coordinates)
    dataframe = pd.concat([dataframe.reset_index(), coordinates_df[1].apply(Point), coordinates_df[2].apply(Point)],
                          axis=1).set_index('index').rename(columns={1: '_SegFrom', 2: '_SegTo'})
    dataframe.index.name = None

    return dataframe


def convert_section_to_path(dataframe):
    """
    Converts the section legs of a dataframe to a linestring.

    :param dataframe: Dataframe containing all the sections of a route.
    :type dataframe: Dataframe

    :return: Linestring built out of the entries of the input dataframe.
    """
    _leg = [ast.literal_eval(str(dataframe['SegFrom'].head(1).values[0]))]

    for i in dataframe.iterrows():
        _leg.append(ast.literal_eval(str(i[1]['SegTo'])))

    return LineString(_leg)


def fill_uncovered_segments(full_section_df, assigned_section_parts_df):
    """
    The road under consideration may start or end in an area that is not covered by the
    dirichlet partitioning and therefore has no qv-stations assigned.
    This function adds these missing parts to the final section to guarantee a 100% coverage of the route.

    :param full_section_df: Route gained from the routing engine.
    :type full_section_df: Dataframe
    :param assigned_section_parts_df: Route sections with already assigned qv stations.
    :type assigned_section_parts_df: Dataframe

    :return: Full route covered with qv stations as a dataframe.
    """
    # Find closest SegTo (index) in full_section_df to first SegFrom in assigned section parts (start of section)
    first_seg_from = assigned_section_parts_df.iloc[0]['_SegFrom']
    seg_to_index = df_helper.find_closest_segment_index(first_seg_from, full_section_df, '_SegTo')

    # Determine the actual start of the section
    section_start_df = pd.DataFrame()
    if first_seg_from == full_section_df.loc[seg_to_index]['_SegTo']:
        section_start_df = full_section_df.loc[:seg_to_index]

    # Find closes SegFrom (index) in full_section_df to the last SegTo in assigned section parts (rest of section)
    last_seg_to = assigned_section_parts_df.iloc[-1]['_SegTo']
    seg_from_index = df_helper.find_closest_segment_index(last_seg_to, full_section_df, '_SegFrom')

    # Determine the actual rest of the section
    section_end_df = pd.DataFrame()
    if last_seg_to == full_section_df.loc[seg_from_index]['_SegFrom']:
        section_end_df = full_section_df.loc[seg_from_index:]
    # Concatenate start, assigned_section and end being the complete section
    complete_section = section_start_df.append(assigned_section_parts_df.drop('index', axis=1))
    complete_section = complete_section.append(section_end_df)

    complete_section = complete_section.drop(['SegFrom', 'SegTo'], axis=1, errors='ignore')
    complete_section = complete_section.rename(columns={'_SegFrom': 'SegFrom', '_SegTo': 'SegTo'})
    complete_section = complete_section[
        ["SegFrom", "SegTo", "StreetName", "SegLength", "avgSpeed", "SegDurationMins", "predefinedLocationReference"]]

    return complete_section


def filter_and_geometrize_locations_df(street_name, direction):
    """
    Returns a filtered data frame, that contains the qv stations to the given street name and direction

    :param street_name: Name of a street
    :type street_name: String
    :param direction: Driving Direction
    :type direction: String

    :return: Dataframe containing qv stations belonging to :param street_name: and :param direction:
    """
    global __QV_STATIONS
    if __QV_STATIONS.empty:
        __QV_STATIONS = db_con.read_dataframe('locations_preprocessed')

    # Filter the stations to the desired street and direction and make a local copy of the df
    locations_df = __QV_STATIONS.loc[
        (__QV_STATIONS['highway'] == street_name) & (__QV_STATIONS['direction'] == direction)]
    locations_df = geopandas.GeoDataFrame(locations_df,
                                          geometry=geopandas.points_from_xy(locations_df.latitude,
                                                                            locations_df.longitude))
    return locations_df


def get_continuous_road_sections(route_df):
    """
    This loop iterates over the unique street names of the route_df and takes the lines of the df containing this
    street. This array is split by index-gaps. E.g.:

    ID    StreetName      ...
    0     "A"
    1     "B"
    2     "B"
    3     "A"
    4     "C"
    5     "C"

    is listed to (only indexes of lines here, actually there are sub-df) -> [[0],[3],[1,2],[4,5]]

    :param route_df: Dataframe containing names of the different streets in a route.
    :type route_df: Dataframe

    :return: Returns a list with road sections sorted by street name and indexes such that continuous sections are
    continuous in the list (see full description of this function).
    """
    road_sections = []

    for hw_name in list(route_df.StreetName.unique()):
        highway = route_df.loc[route_df['StreetName'] == hw_name]
        road_sections.extend(np.split(highway, np.flatnonzero(np.diff(highway.index) != 1) + 1))
    # Sorts the list with indexes
    road_sections.sort(key=functools.cmp_to_key(lambda a, b: -1 if a.index[0] < b.index[0] else 0))

    return road_sections


def get_direction(origin, destination):
    """
    Get the celestial direction of a line from "origin" to "destination".
    There are only the celestials "NO" and "SW" for north-east and south-west.
    This is done by deciding, whether the calculated direction points
    toward below the negative main angle bisector (SW) or above (NO).

    :param origin: Coordinates of the origin location
    :type origin: numeric tuple
    :param destination: Coordinates of the destination location
    :type destination: numeric tuple

    :return: South-West or North-East depending on the calculated value for degrees.
    """
    delta_x = destination[0] - origin[0]
    delta_y = destination[1] - origin[1]

    degrees = math.atan2(delta_x, delta_y) / math.pi * 180
    if degrees < 0:
        degrees += 360

    return 'SW' if 135 <= degrees <= 315 else 'NO'


def get_distances(sorted_df, section_df):
    """
    Adds a column "distances" to the given sorted_df, based on the route represented by section_df.
    The column distances contains the segment length measured starting at '_SegFrom' and ending at '_SegTo'.

    :param sorted_df: Incomplete dataframe of sorted stations along a route
    :type sorted_df: dataframe
    :param section_df: Dataframe which represents all sections along a route
    :type section_df: dataframe

    :return: The sorted dataframe with the distance between stations added to it
    """
    distances = []

    for qv_leg in sorted_df.iterrows():
        start_index = df_helper.find_closest_segment_index(qv_leg[1]['_SegFrom'], section_df, '_SegFrom')
        end_index = df_helper.find_closest_segment_index(qv_leg[1]['_SegTo'], section_df)
        distances.append(section_df.loc[start_index:end_index]['SegLength'].sum())

    sorted_df['SegLength'] = distances

    return sorted_df


def get_polygons_from_voronoi(voronoi_object):
    """
    Polygonizes the given voronoi object and returns the polygons as a list.

    :param voronoi_object: The voronoi object to polygonize.
    :type voronoi_object: Voronoi

    :return: List of polygons
    """
    lines = []  # Lines of Voronoi object representing the polygons
    for line in voronoi_object.ridge_vertices:
        if -1 not in line:
            lines.append(LineString(voronoi_object.vertices[line]))

    polygons = []  # Determining polygons of the voronoi Object
    for poly in polygonize(lines):
        polygons.append(poly)

    return polygons


def replace_section_parts_with_qv_matching(section_df):
    """
    Takes a section_df, that contains a continuous section on a single road without interruptions.
    Iterates this section and assigns qv stations to parts of the section where possible.
    If there are parts of the section that are not covered by a station, these uncovered parts are attached with
    a default value.

    :param section_df: Dataframe containing a continuous section on a single road.
    :type section_df: DataFrame

    :return: The given section_df with assigned stations, where available.
    """
    highway_name = section_df['StreetName'].head(1).values[0]

    if highway_name.startswith('A '):
        path = convert_section_to_path(section_df)

        # Get direction from origin (SegFrom) to destination (SegTo) point
        direction = get_direction(ast.literal_eval(str(section_df['SegFrom'].head(1).values[0])),
                                  ast.literal_eval(str(section_df['SegTo'].tail(1).values[0])))

        voronoi_object = road_partition.get_voronoi(highway_name, direction)
        if voronoi_object is not None:
            # Do the actual division of the section into smaller ones corresponding to the qv-stations
            section_per_qv_df = station_assignment(
                path, voronoi_object, highway_name, direction, section_df['avgSpeed'].max())

            if section_per_qv_df is not None:  # Add the actual computed section
                section_per_qv_df = get_distances(section_per_qv_df, section_df)
                section_per_qv_df['SegDurationMins'] = \
                    section_per_qv_df['SegLength'] / section_per_qv_df['avgSpeed'] * 60
                section_df = fill_uncovered_segments(section_df, section_per_qv_df)

    return section_df


def append_location_references(station, station_set):
    """
    Checks if the given station is empty and either appends the predefinedLocationReference or nothing
    to the station_set.

    :param station: Dataframe of the current road intersection
    :type station: Dataframe
    :param station_set: List of all previous locationReferences
    :type station_set: list

    :returns: The station_set with an additional locationReference
    """
    if not station.empty:
        station_set.append(station.iloc[0]['predefinedLocationReference'])
    else:
        station_set.append(None)

    return station_set


def station_assignment(route, voronoi_object, street_name, direction, max_speed):
    """
    Core function: Splits a linestring (section of a full route) according to a voronoi diagram (that derives
    from the given direction and name of the highway).
    Assigns the corresponding qv-station to the part of the route crossing this part of the voronoi.

    :param route: a linestring of a single street
    :type route: linestring
    :param voronoi_object: relative Voronoi Diagram of the given route
    :type voronoi_object: voronoi diagram
    :param street_name: Name of considered route highway
    :type street_name: String
    :param direction: driving direction (NO / NW)
    :type direction: String
    :param max_speed: limits the speed to this maximum value in case the qv-station has nonsense values
    :type max_speed: numeric Value

    :return: a data frame that contains the given route partitioned to segments corresponding to the voronoi
            of the qv stations.
    """
    # INIT & PRE
    seg_from_set = []  # Set of starting points of segments
    seg_to_set = []  # Set of ending points of segments
    station_set = []

    locations_df = filter_and_geometrize_locations_df(street_name, direction)
    polygons = get_polygons_from_voronoi(voronoi_object)

    # Actual sub route determination -----------------------------
    for i in polygons:  # For every cell of voronoi diagram of the given highway
        if route.intersects(i):
            for sub_route in pd.Series(route.intersection(i)).explode():
                if sub_route.is_ring is False:
                    seg_from_set.append(sub_route.boundary[0])
                    seg_to_set.append(sub_route.boundary[1])
                    station = locations_df.loc[locations_df['geometry'].intersects(i)]
                    station_set = append_location_references(station, station_set)

    non_sorted_df = pd.DataFrame({'_SegFrom': seg_from_set, '_SegTo': seg_to_set, 'SegLength': 0, 'avgSpeed': max_speed,
                                  'StreetName': street_name, 'SegDurationMins': 0,
                                  'predefinedLocationReference': station_set})

    # Sorting the sub routes to represent the full route on the considered street
    sorted_df = df_helper.sort_segments_df(non_sorted_df, route.boundary[0])

    return sorted_df
