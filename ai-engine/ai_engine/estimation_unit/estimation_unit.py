#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function to determine a route between two geocoded locations.
"""
import json
import os.path
from datetime import timedelta
import pickle

import pandas as pd

import ai_engine.station_assignment_unit.station_assignment_unit as sau
from ai_engine.utils.dataframe_helper import add_calendar_features_to_dataframe


def get_estimation(json_object):
    """
    TODO: Update comment
    As a placeholder, currently returns the ETA from the routing service.
    If the parameter 'departure' is None, an ETD is to be calculated.

    :param json_object: A json object containing a placeholder route.
    :type json_object: json

    :returns: The estimated duration for the placeholder route.
    """
    route_json = json.loads(json_object)
    data = route_json['data']
    header = route_json['columns']
    arrival = route_json['timeCorridor']['from']
    departure = route_json['departure']
    route_df = pd.DataFrame(data, columns=header)

    assigned_route_df = sau.assign_station_references(route_df)

    if departure is None:
        # Reverse route dataframe to calculate ETD from given time of arrival and set arrival as initial timestamp
        assigned_route_df = assigned_route_df.iloc[::-1]
        time = pd.to_datetime(arrival)
    else:
        # Set departure as initial timestamp
        time = pd.to_datetime(departure)

    resulting_time = calc_route_duration(assigned_route_df, time, departure)

    # Calculate duration = time of arrival - time of departure
    if departure is None:
        duration = (time - resulting_time).total_seconds() / 60
    else:
        duration = (resulting_time - time).total_seconds() / 60

    return duration


def calc_route_duration(route_df, time, departure):
    """
    Returns the duration of the given route using trained AI models

    :param route_df: A pandas DataFrame object containing a route.
    :type route_df: DataFrame

    :param time: A timestamp containing either departure or arrival time.
    :type time: Datetime

    :param departure: A timestamp containing departure time.
    :type departure: timestamp or None

    :returns: Returns a fixed value as estimated duration.
    """
    street_name_new = route_df.iloc[0]['StreetName']
    value = 0

    for i, row in route_df.iterrows():
        street_name = row['StreetName']

        # Check if StreetName changes or end of route is reached - i.e. a continuous road block ends
        if street_name != street_name_new or i == route_df.index[-1]:
            if i == route_df.index[-1]:
                i += 1
            dataframe = route_df[value:i]
            qv_ordered = dataframe.predefinedLocationReference.unique()
            for j in qv_ordered:
                # The time is given to estimate_segment_time and "added up" there.
                time = estimate_segment_time(j, dataframe, time, departure)
            value = i
            street_name_new = street_name

    return time


def estimate_segment_time(location_reference, dataframe, time, departure):
    """
    Calculates the time needed to drive through one section / qV-Station of a route.

    :param location_reference: The current name of the qV-Station.

    :param dataframe: A dataframe containing a part of a route with a continuous road.
    :type dataframe: DataFrame

    :param time: A Timestamp containing either a departure or arrival time.
    :type time: Datetime

    :param departure: A containing a departure time.
    :type departure: Datetime if :param time: is an arrival else None

    :return: The given timestamp altered by the current segments drive time.
    """
    model = f'./resources/models/{location_reference}.sav'

    # Extract part of route that belongs to given location reference
    loc_ref_df = dataframe.loc[dataframe['predefinedLocationReference'] == location_reference]

    if loc_ref_df.empty:
       loc_ref_df =  dataframe.loc[dataframe['predefinedLocationReference'].isnull()]

    if os.path.isfile(model):
        # Use machine learning for the given qv-station
        ml_df = pd.DataFrame(data={time}, columns=['time'])
        prediction_df = create_ml_features(ml_df)

        # Load required model and predict speeds
        with open(model, "rb") as file:
            loaded_model = pickle.load(file)
            estimation = loaded_model.predict(prediction_df.drop('time', axis=1, errors='ignore'))
            loc_ref_df['avgSpeed'] = estimation[0]

        loc_ref_df['SegDurationMins'] = (loc_ref_df['SegLength'] / loc_ref_df['avgSpeed']) * 60

    duration = loc_ref_df['SegDurationMins'].sum()

    if departure is None:
        time -= timedelta(minutes=duration)
    else:
        time += timedelta(minutes=duration)

    return time


def create_ml_features(dataframe):
    """
    Function to create machine learning features for dataframe.

    :param dataframe: A pandas DataFrame object containing a route.
    :type dataframe: DataFrame
    """
    # Round time to full 15 min to match prediction interval
    dataframe['time'] = pd.to_datetime(dataframe['time'])
    dataframe['time'] = dataframe['time'].round('15min')

    # Create calendar features
    dataframe = add_calendar_features_to_dataframe(dataframe)

    return dataframe
