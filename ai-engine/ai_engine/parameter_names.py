#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Some constants for parameter names.
"""

NOMINATIM_HOSTNAME = "NOMINATIM_HOSTNAME"
NOMINATIM_PORT = "NOMINATIM_PORT"
POSTGRES_USER = "POSTGRES_USER"
POSTGRES_PASSWORD = "POSTGRES_PASSWORD"
POSTGRES_DB = "POSTGRES_DB"
POSTGRES_HOST = "POSTGRES_HOST"
