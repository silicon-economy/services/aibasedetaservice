#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function that adds a new column
to a data frame containing qv stations with locations.
"""

import pandas as pd
from scipy.spatial import Voronoi

import ai_engine.database_connector as db_con

# A convex shape surrounding north-rhine-westphalia completely.
outer_shape = [[52.360113, 5.812947], [50.216926, 5.692097], [52.720933, 9.548298], [50.448351, 10.185505]]

try:
    __STREET_VORONOI_DIAGRAMS_DF = pd.read_pickle('./resources/voronois.pkl')
except FileNotFoundError:
    __STREET_VORONOI_DIAGRAMS_DF = pd.DataFrame()


def __partition_roads():
    """
    This function determines a dirichlet distribution (voronoi diagram) for each highway that has at least
    3 cameras (minimal requirement for determining a dirichlet distribution).
    """
    qv_stations = db_con.read_dataframe('locations_preprocessed')

    voronoi_diagrams = []
    for key, value in qv_stations.groupby(['highway', 'direction']):
        if len(value) >= 3:
            _points = outer_shape.copy()

            for i in value.iterrows():
                _points.append([i[1]['latitude'], i[1]['longitude']])

            voronoi_diagrams.append([key[0], key[1], _points])

    global __STREET_VORONOI_DIAGRAMS_DF
    __STREET_VORONOI_DIAGRAMS_DF = qv_stations[['highway', 'direction']]\
        .drop_duplicates()\
        .merge(pd.DataFrame(voronoi_diagrams), left_on=['highway', 'direction'], right_on=[0, 1], how='inner')
    __STREET_VORONOI_DIAGRAMS_DF = \
        __STREET_VORONOI_DIAGRAMS_DF[['highway', 'direction', 2]].rename(columns={2: 'shape'})
    __STREET_VORONOI_DIAGRAMS_DF.to_pickle('./resources/voronois.pkl')


def get_voronoi(street_name, direction):
    """
    Takes the name of a street and its direction as input to calculate coordinate points from it.
    The points are used as input to generate a voronoi diagram.

    :param street_name: Name of a street
    :type street_name: String
    :param direction: Driving Direction
    :type direction: String

    :returns: A voronoi diagram based on the points gained from the street name and directions.
    """
    if __STREET_VORONOI_DIAGRAMS_DF.empty:
        __partition_roads()

    try:
        points = __STREET_VORONOI_DIAGRAMS_DF[
            (__STREET_VORONOI_DIAGRAMS_DF['highway'] == street_name) &
            (__STREET_VORONOI_DIAGRAMS_DF['direction'] == direction)]['shape'].item()
    except (IndexError, ValueError):
        return None

    return Voronoi(points, qhull_options='Qbb Qc Qz Qx')  # qhull_options given are the default options


def write_voronoi_diagrams():
    """
    This function just calls the actual partitioning that determines the voronoi diagrams for each street that meet
    the requirements.

    TODO: Is this function then needed at all?
    """
    __partition_roads()


# Not needed at the moment...
def is_in_diagram_list(street_name):
    """
    Checks if a given street is already listed in the voronoi diagram.

    :param street_name: Name of a street
    :type street_name: String

    :return: True is the given name is in the list else False.
    """
    if __STREET_VORONOI_DIAGRAMS_DF.empty:
        __partition_roads()

    return street_name in __STREET_VORONOI_DIAGRAMS_DF.highway.unique()
