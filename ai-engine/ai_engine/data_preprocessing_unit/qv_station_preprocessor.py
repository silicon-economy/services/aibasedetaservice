#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3


"""
This module offers functions to pull qv stations from the qv-service, preprocess them to a desired format and
add street names and directions.
"""

from io import StringIO
import os
import json
from urllib import parse
import logging

import requests
import lxml.etree as et
import pandas as pd

import ai_engine.database_connector as db_con
import ai_engine.parameter_names

nominatim_host = os.environ.get(ai_engine.parameter_names.NOMINATIM_HOSTNAME,
                                "https://osm-nominatim.apps.sele.iml.fraunhofer.de")
nominatim_port = os.environ.get(ai_engine.parameter_names.NOMINATIM_PORT, "443")
GEOCODE_URL = f"{nominatim_host}:{nominatim_port}/search?q="
known_street_prefixes = ['A ', 'B ', 'E ', 'L ', 'K ']


def preprocess_qv_stations():
    """
    Get the QV stations collected by the QV service from the database and preprocess them to the needs of the AI engine.

    :returns: A dataframe containing references, street names and the street direction of all QV stations.
    """

    # Prepare and get stations from database
    locations = db_con.read_dataframe('qv_locations')

    if locations is None:
        logging.info('Could not read QV-stations from database. Using Backups...')
        locations = __open_backup_locations()

    logging.info('Start preprocessing of QV-locations...')

    # Actual preprocessing

    # Step 1.1: Filter locations by lane 1 and drop every other station:
    locations = locations.loc[locations['lane'] == 'lane1']

    # Step 1.2: Filter locations of access and exits of highways
    locations = locations.loc[~(locations['predefinedLocationReference'].str.contains('_AB_')) &
                              ~(locations['predefinedLocationReference'].str.contains('_ZU_'))]

    logging.info("After Step one (filtering):")
    logging.info(str(locations))
    logging.info('Adding street names to QV-locations')

    # Step 2: Locate street (name) of the QV stations:
    locations = __add_street_names(locations)

    logging.info('Adding directions to QV-locations')

    # Step 3: Extract and add the direction of a QV station from the predefinedLocationReference
    locations = __add_directions(locations)

    logging.info('...finished preprocessing QV-locations. Result: ')
    logging.info(str(locations))

    return locations


def __add_street_names(locations):
    """
    Add street names to the qv station by asking the geocoding engine.

    :param locations: Contains location info of qv stations.
    :type locations: Dataframe

    :returns: A dataframe containing the known qv stations matched together with their location's streets.
    """
    known_stations = read_known_stations()

    logging.info('Matching qv stations with highways')
    caught_exception = False
    _df = []

    for key, value in locations.iterrows():
        if known_stations is not None:
            # Read from known stations
            row = known_stations.loc[known_stations['predefinedLocationReference']
                                     == value['predefinedLocationReference']]
            if not row.empty:
                # Current station is known, continue
                _df.append([key, row.iloc[0]['highway']])
                continue

        # Station is not known, acquire street name
        if not caught_exception:
            resp, caught_exception = __geocode(value)
            street = check_street_prefixes(resp.json())
            if street is not None:
                _df.append([key, street])

    if len(_df) > 0:
        _df = pd.DataFrame(_df).set_index(0)
        _df = pd.DataFrame(_df).merge(locations, left_index=True, right_index=True)
        _df = _df.rename(columns={1: 'highway'})
    else:
        _df = locations
        _df['highway'] = None

    return _df


def read_known_stations():
    """
    Tries to read all qv stations (locations) from the database. If this fails,
    tries to read a fallback file. If that fails as well, returns None.
    """
    known_stations = db_con.read_dataframe('locations_preprocessed')

    if known_stations is None and os.path.exists('resources/preprocessed_qv_locations.json'):
        known_stations = pd.read_json('resources/preprocessed_qv_locations.json').T

    return known_stations


def __add_directions(locations):
    """
    Extract directions from the qv station's location reference and add it to the dataframe.

    :param locations: Contains the location references to different qv stations.
    :type locations: Dataframe

    :returns: A dataframe which adds the directions of the qv stations to general dataframe.
    """
    loc_df = []

    for key, value in locations.iterrows():
        if 'NO' in value['predefinedLocationReference']:
            loc_df.append([key, 'NO'])
        elif 'SW' in value['predefinedLocationReference']:
            loc_df.append([key, 'SW'])

    _df = locations.merge(pd.DataFrame(loc_df).set_index(0), left_index=True, right_index=True)
    _df = _df.rename(columns={1: 'direction'})

    return _df


def check_street_prefixes(json_object):
    """
    Converts street names into prefix form (e.g. A 40).
    Eliminates any leading blanks from the prefix_form.

    :param json_object: JSON reference to the street names without any prefixes.
    :type json_object: json

    :returns: A street name from the json object if it is found in the known prefixes.
              Else None is returned.
    """
    if json_object is not None:
        for i in json_object[0]['display_name'].split(','):
            if any((' ' + prefix) in i for prefix in known_street_prefixes):
                return i[1:]
            if any(prefix in i for prefix in known_street_prefixes):
                return i
            if 'Ruhrschnellweg' in i:
                return 'A 40'
            if 'Emscherschnellweg' in i:
                return 'A 42'

    return None


def __geocode(value):
    """
    Takes the latitude and longitude parameters and converts them into an exact geolocation via the geocoding engine.

    :param value: A dataframe containing latitude and longitude values of a qv station.
    :type value: Dataframe

    :returns: The geolocation coordinates and a bool value showing if an exception got caught.
    """
    lat_lon = str(value['latitude']) + ', ' + str(value['longitude'])
    caught_exception = False
    resp = None
    try:
        resp = requests.get(GEOCODE_URL + parse.quote_plus(lat_lon) + '&format=json&limit=1')
    except (ConnectionError, requests.RequestException):
        caught_exception = True
    return resp, caught_exception


def __open_backup_locations():
    """
    Opens a zipped xml file as a fallback if the QV service is unavailable.

    :returns: The information gained from the xml file as a dataframe.
    """
    with open('resources/locations.xml', 'r') as f_in:
        doc = et.parse(f_in)
        xsl = et.parse('resources/locations.xsl')
        transform = et.XSLT(xsl, access_control=et.XSLTAccessControl.DENY_WRITE)
        result = str(transform(doc))
        locations = pd.read_csv(StringIO(result))

    return locations
