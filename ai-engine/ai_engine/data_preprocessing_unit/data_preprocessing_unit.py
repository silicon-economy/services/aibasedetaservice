#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function collecting, aggregating and preprocessing all relevant data for
qv stations (e.g. get positions, match stations to qv-pairs, get street names of these
stations, ...) and returns an overall data frame containing these information.
"""

import logging
import pandas as pd
import time
import threading

from ai_engine.data_preprocessing_unit.qv_station_preprocessor import preprocess_qv_stations
from ai_engine.utils.dataframe_helper import add_calendar_features_to_dataframe
import ai_engine.database_connector as db_con

# Config constant values
LORRY_SPEED_THRESHOLD = 95
LORRY_SPEED_MAX = 88


def preprocess_locations():
    """
    Call the certain modules and collect all necessary data. Returns a full data frame with all
    QV stations with all relevant information for the AI engine.
    """
    # Get qv stations from the qv service and add street names etc. Returns a data frame
    qv_df = preprocess_qv_stations()

    # Write the result to the Database
    logging.info('Got preprocessed qv stations, writing preprocessed locations to database')
    engine = db_con.connect_to_database()
    with engine.begin() as connection:
        qv_df.to_sql('locations_preprocessed', con=connection, index=False, if_exists='replace')

    return qv_df


def get_preprocessed_qv_data(location_reference):
    """
    Obtains the data to a certain QV station (given by location reference).
    Preprocesses the data to the format needed by the model fitting unit (fit_qv_stations).

    :param location_reference: The unique location reference of a specific QV station.
    :type location_reference: string

    :returns: A data frame that contains information about the specific QV station such as a history of lorry speeds.
    """

    # Get station data from qv service for the locationReference
    logging.info('Preprocessing qv-data for ' + str(location_reference))
    _df = db_con.read_dataframe(location_reference)
    if _df is None or _df.empty:
        return pd.DataFrame()

    # Set "unrealistic" measuring speeds to typical average driver speed
    _df['lorry_Speed'] = pd.to_numeric(_df['lorry_Speed'])
    _df.loc[_df['lorry_Speed'] > LORRY_SPEED_THRESHOLD, 'lorry_Speed'] = LORRY_SPEED_MAX

    # Round to full 15 minute interval
    _df['timestamp'] = pd.to_datetime(_df['timestamp'])
    _df['Time'] = _df['timestamp'].round('1min')
    _df['time'] = _df['Time'].dt.ceil('15min')

    # Drop not used columns
    _df = _df.drop(
        ['anyVehicle_FlowRate', 'anyVehicle_Speed', 'lorryVehicle_FlowRate', 'timestamp', 'Time'], axis=1)

    # Fill unknown speeds with previous entry
    _df['lorry_Speed'] = _df['lorry_Speed'].ffill()
    _df = _df.reset_index(drop=True)
    _df = _df.fillna(0)
    _df = _df.rename(columns={'lorry_Speed': 'lorry_speed'})

    # Add weekday hour and minute values
    _df = add_calendar_features_to_dataframe(_df)

    return _df


class PreprocessingThread(threading.Thread):
    """ Preprocessor-Thread for qv-locations."""
    def __init__(self, thread_id, name, counter):
        """
        Init thread.

        :param thread_id: The ID which is given to the thread

        :param name: The name which is given to the thread

        :param counter: Thread internal counter
        """
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.counter = counter

    def run(self):
        """ Run qv-station-preprocessing. """
        print("Start preprocessing Thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
        preprocess_locations()
        print("Exit preprocessing Thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
