#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module offers some functions that perform certain actions with given data frames.
"""

import pandas as pd
from shapely.geometry import MultiPoint
from shapely.ops import nearest_points


def sort_segments_df(non_sorted_df, first_point):
    """
    Sort any dataframe having two points "SegFrom" and "SegTo" in each row. Needs a starting point.

    :param non_sorted_df: Dataframe containing unsorted fragments of a route.
    :type non_sorted_df: Dataframe
    :param first_point: Point which is used as first entry for the sorted dataframe.
    :type first_point: shapely point

    :return: The sorted version of the dataframe given in :param non_sorted_df:
    """
    sorted_df = pd.DataFrame()

    if len(non_sorted_df) > 1:
        # Sorting the resulted dataframe, based on the chain of the intersecting points
        for _ in range(len(non_sorted_df.index) - 1):
            next_segment = non_sorted_df.loc[find_closest_segment_index(first_point, non_sorted_df, '_SegFrom')]
            non_sorted_df = non_sorted_df[~non_sorted_df.isin(next_segment)].dropna(how='all')
            first_point = next_segment['_SegTo']
            sorted_df = sorted_df.append(next_segment)

    elif len(non_sorted_df) == 1:
        sorted_df = non_sorted_df

    else:
        return None

    sorted_df = sorted_df.reset_index()

    return sorted_df


def find_closest_segment_index(point, considered_segments_df, ref='_SegTo'):
    """
    Finds the closest segment to a given point among a dataframe of considered segments.

    :param point: The point is used as a reference to find the next closest segments.
    :type point: shapely point
    :param considered_segments_df: The dataframe contains all route segments which
                                   are considered to be closest to the given point.
    :type considered_segments_df: Dataframe
    :param ref: Determines if segments are calculated based from the point or towards the point.
                Defaults to '_SegTo'.
    :type ref: String

    :return: The index of the segment closest to the location specified in :param point:
    """
    seg_points = MultiPoint(list(considered_segments_df[ref]))
    closest_point = [o for o in nearest_points(point, seg_points)][1]
    found_index = considered_segments_df.loc[considered_segments_df[ref] == closest_point].index[0]

    return found_index


def add_calendar_features_to_dataframe(dataframe):
    """
    Takes a dataframe as input and splits it's 'time' column into two separate columns for the weekday and the time.
    DayOfWeek has values 0 - 6, 0 for Monday and 6 for Sunday.
    TimeOfDay is a number, representing the minute of the day, e.g. 70 for 1:10 am.
    Also determines if the weekday is a working day (Mon - Fri) or a weekend day (Sat, Sun), storing this as 1 or 0 in
    WorkingDay.

    :param dataframe: A Dataframe with a time-column for concrete day and time information.
    :type dataframe: Dataframe

    :returns: The dataframe with day and time information as number representations added.
    """
    # Add the day of week and time of day columns
    dataframe['DayOfWeek'] = pd.to_datetime(dataframe['time']).dt.weekday
    dataframe['TimeOfDay'] = dataframe['time'].dt.hour * 60 + dataframe['time'].dt.minute

    # Determine working or weekend days
    dataframe.loc[dataframe['DayOfWeek'].isin([0, 1, 2, 3, 4]), 'WorkingDay'] = 1
    dataframe.loc[~(dataframe['DayOfWeek'].isin([0, 1, 2, 3, 4])), 'WorkingDay'] = 0

    return dataframe
