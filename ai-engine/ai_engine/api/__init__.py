#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
API version 1 specification

All resources are added in their own namespaces
"""

from flask import Blueprint
from flask_restx import Api

from ai_engine.routes.ai_engine import api as ai_engine_api


# Create the api endpoint
api_v1_bp = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(api_v1_bp,
          version='1.0',
          title='AI engine API',
          description='The AI-Engine.')

# Add namespace here
api.add_namespace(ai_engine_api)


__all__ = ["api_v1_bp"]
