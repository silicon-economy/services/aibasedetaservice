#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
"""
Establish a connection to the database.
"""
import logging
import os

import psycopg2
import sqlalchemy as db
import pandas as pd
import ai_engine.parameter_names as param


def connect_to_database():
    """
    Reads the needed params and connects to the corresponding database.

    :returns: The created engine to communicate with the database.
    """
    db_user = os.environ.get(param.POSTGRES_USER, '12345')
    db_password = os.environ.get(param.POSTGRES_PASSWORD, '12345')
    db_database = os.environ.get(param.POSTGRES_DB, '12345')
    db_host = os.environ.get(param.POSTGRES_HOST, 'localhost:5432')
    engine = db.create_engine(f'postgresql://{db_user}:{db_password}@{db_host}/{db_database}')

    return engine


def read_dataframe(table_name: str):
    """
    Reads a table from the database and returns it as a data frame.
    Returns None, if database has any error.
    """
    logging.info('Read table %s from database...', table_name)

    table_as_df = None
    try:
        engine = connect_to_database()
        table_as_df = pd.read_sql_table(table_name, con=engine)
    except (ValueError, psycopg2.OperationalError) as exception:
        logging.info("An error occurred when reading from the database: %s", str(exception))

    return table_as_df
