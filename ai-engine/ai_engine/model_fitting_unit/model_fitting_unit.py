#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module fits AI models for the ETA estimation.
"""

import os
import threading
import pickle
import time
import lightgbm as lgbm
from sklearn.model_selection import train_test_split

import ai_engine.data_preprocessing_unit.data_preprocessing_unit as dpu
import ai_engine.database_connector as db_con


TEST_SIZE = 0.1
params = {'boosting_type': 'gbdt', 'n_estimators': 500,
          'learning_rate': 0.01, 'colsample_bytree': .85, 'num_leaves': 512, 'max_bin': 1024,
          'n_jobs': 16, 'verbose': -1}
callbacks = [lgbm.early_stopping(50, verbose=False)]


def __station_fitting(qv_location_data, station_name):
    """
    Fits a QV station for ML training.

    :param qv_location_data: Dataframe used as the ML input data.
    :type qv_location_data: Dataframe
    """
    try:
        regressor = lgbm.LGBMRegressor(**params)
        print('fitting model ' + str(station_name))

        # Training
        df_train, df_valid = train_test_split(qv_location_data, test_size=TEST_SIZE, shuffle=True, random_state=1)
        regressor.fit(
            df_train.drop(['predefinedLocationReference', 'time', 'lorry_speed'], axis=1, errors='ignore'),
            df_train['lorry_speed'],
            eval_set=[
                (df_valid.drop(['predefinedLocationReference', 'time', 'lorry_speed'], axis=1, errors='ignore'),
                 df_valid['lorry_speed'])], callbacks=callbacks)

        # Save the model to disk
        print('saving model ' + str(station_name))
        os.makedirs(os.path.dirname('resources/models/test.txt'), exist_ok=True)
        filename = 'resources/models/' + str(station_name) + '.sav'
        with open(filename, 'wb') as _f:
            pickle.dump(regressor, _f)

    except Exception as exception:
        print(exception)


def fit_qv_stations(max_station_count=0):
    """
    Fits all qv stations as single models.

    :param max_station_count: Maximum number of qv-stations. Defaults to 0.
    :type max_station_count: int
    """
    _df = db_con.read_dataframe('qv_locations')
    current_station = 0

    for _, value in _df.iterrows():
        qv_location_data = dpu.get_preprocessed_qv_data(value['predefinedLocationReference'])
        if not qv_location_data.empty:
            __station_fitting(qv_location_data, value['predefinedLocationReference'])
            if max_station_count > 0:
                current_station += 1
                if current_station >= max_station_count:
                    break


class FittingThread(threading.Thread):
    """ Model fitting thread for qv-data."""
    def __init__(self, thread_id, name, counter):
        """
        Init thread.

        :param thread_id: The ID which is given to the thread

        :param name: The name which is given to the thread

        :param counter: Thread internal counter
        """
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.counter = counter

    def run(self):
        """ Run qv-station-fitting. """
        print("Start model fitting thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
        fit_qv_stations()
        print("Exit model fitting thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
