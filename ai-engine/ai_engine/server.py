#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
"""
This module is designed to start a flask server. This is the entry point for external consumers to
interact with the AI engine.

Initializes flask server.
Initializes loggers.
"""

import logging
import os
import secrets
import string

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask
from flask_cors import CORS

from ai_engine.api import api_v1_bp
from ai_engine.model_fitting_unit.model_fitting_unit import FittingThread
from ai_engine.data_preprocessing_unit.data_preprocessing_unit import PreprocessingThread


def create_app(config='production') -> Flask:
    """
    Creates flask app.
    Registers blueprint for the AI api.
    Removes CORS filter.

    :returns: The flask app
    """
    flask_app = Flask(__name__)

    if flask_app.config['ENV'] == 'development' or config == 'development':
        flask_app.config.from_object('ai_engine.config.DevelopmentConfig')
    elif config == 'production':
        flask_app.config.from_object('ai_engine.config.ProductionConfig')
    elif config == 'test':
        if os.environ.get('WHERE_ARE_WE_TESTING') is not None:
            flask_app.config.from_object('ai_engine.config.TestAtServerConfig')
        else:
            flask_app.config.from_object('ai_engine.config.TestConfig')

    # Init loggers
    init_loggers()

    # Register blueprints
    register_blueprints(flask_app)

    # Add CORS header
    CORS(flask_app, resources={r"/*": {"origins": "*", "send_wildcard": "False"}})

    if config != 'test':
        # initial fit qv stations
        preprocess_locations()
        fit_stations()
        scheduler = BackgroundScheduler()
        # schedule model training for every saturday -> sunday night at 21:00
        scheduler.add_job(func=preprocess_locations, trigger="cron", day_of_week=5, hour=20)
        scheduler.add_job(func=fit_stations, trigger="cron", day_of_week=5, hour=21)

        scheduler.start()

    return flask_app


def fit_stations():
    """ Import all stations from mdm. """
    fitting = FittingThread(secrets.choice(string.digits),
                              "fit_qv_stations", secrets.choice(string.digits))
    fitting.start()


def preprocess_locations():
    """ Preprocesses QV-stations with direction and road name. """
    preprocessing = PreprocessingThread(secrets.choice(string.digits),
                              "preprocess_qv_stations", secrets.choice(string.digits))
    preprocessing.start()


def init_loggers():
    """
    Initializes loggers.
    """
    logging.basicConfig(level=logging.INFO)


def register_blueprints(flask_app: Flask):
    """
    Register all blueprints used in the AI engine.
    """
    flask_app.register_blueprint(api_v1_bp)


if __name__ == '__main__':
    app = create_app()
    logging.info("Starting AI engine!")

    app.run(host="0.0.0.0", port=5003, debug=True, use_reloader=False)
