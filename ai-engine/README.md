# Flask Backend Server for the AI-Engine.

## Technology stack
* Python
* Flask
* QV-service (future release)

## Deploy locally: Build and run
For a general deployment of the complete service, see the readme in the documentation folder.
For testing purposes or a single build of this engine, read here.
To build, you can use docker build.
In order to run the service, make sure, that Docker is running, and you started the databases (CouchDB) as well as the QV-service and weather-service.
For development purposes, you can run `docker-compose -f docker-compose.dev.yml up` in the project's root directory "aibasedetaservice" to start the database, but anyway you need to start the other services on your own.

## Build and run (if using no IDE)
If not declared explicitly otherwise, please run all commands in the python-backend directory.

### Preparations
* First of all, make sure, that Docker is running and, for development purposes, you run `docker-compose -f docker-compose.dev.yml up` in the project's root directory "aibasedetaservice".
* Create a virtual environment with `virtualenv venv`.
* Make sure, all dependencies are installed by executing the Pipfile wit `pipenv install`
* Open the virtual environment with `pipenv shell`.

### Start the flask server
You can start the flask server (i.e. the ai-engine) with `./application.sh dev-server` (if you do not execute via your IDE).

### run tests
Simply run `pytest` in console.

## Build and run with PyCharm
First of all, make sure, that Docker is running and, for development purposes, you run `docker-compose -f docker-compose.dev.yml up` in the project's root directory "aibasedetaservice".
Then open the project ai-engine with PyCharm. If you do so for the first time, PyCharm will  install the pipfile dependencies on its own and set up a proper virtual environment.

### Run the ai-engine flask server
Preparations:
* In the PyCharm explorer, find `./ai_engine/server.py` and `right click -> Modify Run Configurations...`
* Configure the following settings:
	* Optional: Define a configuration name (e.g. "Server").
	* Set the environment variables to `PYTHONUNBUFFERED=1;FLASK_APP=ai_engine.server.py;FLASK_ENV=development`
	* Set the working directory to `<your local directory>\aibasedetaservice\` (note that there is no \ai_engine\.)

From now on, you can run your "Server" configuration by selecting it and then pressing the green play button in the top right corner.

### Run tests
Preparations:
* In the PyCharm explorer, find `./tests` and `right click -> Modify Run Configurations...`
* Configure the following settings:
	* Optional: Set a name (e.g. "Tests").
	* Set the working directory to `<your local directory>\aibasedetaservice\` (note that there is no \tests\.)

From no on, you can run your "Tests" configuration by selecting it and then pressing the green play button in the top right corner.

## Rest

## Logging
Use a logging framework e.g. the package logging. printf won't work with docker properly.

## Licenses of third-party dependencies
The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports
This project uses [pip-licenses](https://pypi.org/project/pip-licenses/) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --with-urls --format=csv --output-file=third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --summary --output-file=third-party-licenses/third-party-licenses-summary.txt`

