#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the database connector.
"""
import unittest
from unittest.mock import patch

import pandas
from pandas.testing import assert_frame_equal

import ai_engine.database_connector as db_con


class TestDatabaseConnector(unittest.TestCase):
    """ Test class for the database connector. """

    def setUp(self):
        """ Set up before tests. """

    @patch("sqlalchemy.create_engine")
    def test_connect_to_database(self, db_create_mock):
        """ Test connect to database method. """
        # Arrange
        r_val = "Test"
        db_create_mock.return_value = r_val

        # Act
        result = db_con.connect_to_database()

        # Assert
        assert result == r_val

    @patch("pandas.read_sql_table")
    @patch("ai_engine.database_connector.connect_to_database")
    def test_read_dataframe(self, db_create_mock, db_read_mock):
        """ Test reading a dataframe from the db. """
        # Arrange
        _df = pandas.DataFrame(columns=["A", "B"], data={"A": [0, 1], "B": [2, 3]})
        db_create_mock.return_value = "Test"
        db_read_mock.return_value = _df

        # Act
        result = db_con.read_dataframe("test")

        # Assert
        assert_frame_equal(result, _df)

    @patch("pandas.read_sql_table", side_effect=ValueError())
    @patch("ai_engine.database_connector.connect_to_database")
    def test_read_dataframe_with_exception(self, db_create_mock, db_read_mock):
        """ Test reading a dataframe from the db and raising an exception. """
        # Arrange
        db_create_mock.return_value = "Test"

        # Act
        result = db_con.read_dataframe("test")

        # Assert
        assert result is None
