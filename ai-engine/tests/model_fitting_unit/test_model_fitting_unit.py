#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the model fitting unit.
"""
import unittest
from unittest.mock import patch
import os
import pandas as pd

import ai_engine.model_fitting_unit.model_fitting_unit as mfu


class TestModelFittingUnit(unittest.TestCase):
    """ Test class for model fitting unit. """
    qv_locations_data = pd.read_json('./tests/resources/test_preprocessed_locations.json')
    qv_station_mock_data = pd.read_json("./tests/resources/test_qvdata_2021-03-05_16.15.json").T
    location_reference = "fs.MQ_A40-10E_HFB_NO_1"

    def setUp(self):
        """ Set up before tests. """
        self.qv_station_mock_data = self.qv_station_mock_data.sort_values('timestamp')
        self.qv_station_mock_data = self.qv_station_mock_data.loc[
                self.qv_station_mock_data['predefinedLocationReference'] == self.location_reference]

    @patch("ai_engine.database_connector.read_dataframe", side_effect=[qv_locations_data, qv_station_mock_data])
    def test_fit_qv_stations(self, db_read_mock):
        """ Should create a sav file with the learned AI model for the particular station. """
        # Arrange
        if os.path.exists(f'resources/models/{self.location_reference}.sav'):
            os.remove(f'resources/models/{self.location_reference}.sav')
        # Act
        mfu.fit_qv_stations(1)

        # Assert
        assert os.path.exists(f'resources/models/{self.location_reference}.sav')

    @patch("ai_engine.model_fitting_unit.model_fitting_unit.fit_qv_stations")
    def test_thread(self, fitting_mock):
        # Act
        thread = mfu.FittingThread("test thread", "tt", 1)
        thread.run()

        # Assert
        fitting_mock.assert_called_once()
