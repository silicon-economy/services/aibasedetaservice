#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the estimation unit.
"""
import unittest
from datetime import timedelta
from unittest.mock import patch

from pandas.testing import assert_frame_equal
import pandas as pd

import ai_engine.estimation_unit.estimation_unit as e_unit


def mock_estimate(location_ref, dataframe, time, departure):
    if departure is None:
        time -= timedelta(minutes=1)
    else:
        time += timedelta(minutes=1)
    return time


class TestEstimationUnit(unittest.TestCase):
    """ Test class for data preprocessor. """
    qv_locations_data = pd.read_json('./tests/resources/test_preprocessed_locations.json')
    route_with_assigned_stations = pd.read_json('./tests/resources/routes/with_assigned_stations.json')

    def setUp(self):
        """ Set up before tests. """
        with open('./tests/resources/routes/base_with_departure.json', 'r', encoding='utf-8') as _f:
            self.route_json = _f.read()
        with open('./tests/resources/routes/base_no_departure.json', 'r', encoding='utf-8') as _f:
            self.no_dep_route_json = _f.read()

    @patch("ai_engine.station_assignment_unit.station_assignment_unit.assign_station_references")
    def test_get_estimation(self, assign_mock):
        """ Tests the method get_estimation from the estimation unit. """
        # Arrange
        assign_mock.return_value = self.route_with_assigned_stations

        # Act
        result = e_unit.get_estimation(self.route_json)

        # Assert
        assert result > 0

    @patch("ai_engine.station_assignment_unit.station_assignment_unit.assign_station_references")
    def test_get_estimation_no_departure(self, assign_mock):
        """ Tests the method get_estimation from the estimation unit. """
        # Arrange
        assign_mock.return_value = self.route_with_assigned_stations

        # Act
        result = e_unit.get_estimation(self.no_dep_route_json)

        # Assert
        assert result > 0


@patch("ai_engine.estimation_unit.estimation_unit.estimate_segment_time", side_effect=mock_estimate)
def test_calc_route_duration_no_departure(estimate_mock):
    """ Tests the method calc_route_duration from the estimation unit without departure time, so calculating an ETD. """
    # Arrange
    references = ['fs.MQ_1.002_HFB_NO_1', 'fs.MQ_1.002_HFB_NO_2', None]
    dataframe = arrange_calc_route_duration_data(references)
    time = pd.to_datetime(519843118618318495)
    departure = None
    expected_output = pd.Timestamp('1986-06-22 16:48:58.618318495')

    # Act
    result = e_unit.calc_route_duration(dataframe, time, departure)

    # Assert
    assert result == expected_output


@patch("ai_engine.estimation_unit.estimation_unit.estimate_segment_time", side_effect=mock_estimate)
def test_calc_route_duration_with_departure(estimate_mock):
    """ Tests the method calc_route_duration from the estimation unit with a depature time, so calculating an ETA. """
    # Arrange
    references = [None, None, 'fs.MQ_1.002_HFB_NO_3']
    dataframe = arrange_calc_route_duration_data(references)
    time = pd.to_datetime(519843118618318495)
    departure = pd.to_datetime(519843118618318495)
    expected_output = pd.Timestamp('1986-06-22 16:53:58.618318495')

    # Act
    result = e_unit.calc_route_duration(dataframe, time, departure)

    # Assert
    assert result == expected_output


def test_estimate_segment_time_with_model():
    """ Test with a given location reference and existing model for that station. Testing ETA and ETD. """
    # Arrange
    reference = 'fs.MQ_A40-10E_HFB_NO_1'
    references = [reference, None, None]
    dataframe = arrange_calc_route_duration_data(references)
    calculation_begin_time = pd.to_datetime(519843118618318495)

    # Act
    result_etd = e_unit.estimate_segment_time(reference, dataframe, calculation_begin_time, None)
    result_eta = e_unit.estimate_segment_time(reference, dataframe, calculation_begin_time, calculation_begin_time)

    # Assert
    assert result_etd < calculation_begin_time
    assert result_eta > calculation_begin_time


def test_estimate_segment_time_no_location():
    """ Test with location reference being None. Testing ETA and ETD. """
    # Arrange
    references = [None, None, None]
    dataframe = arrange_calc_route_duration_data(references)
    calculation_begin_time = pd.to_datetime(519843118618318495)

    # Act
    result_etd = e_unit.estimate_segment_time(None, dataframe, calculation_begin_time, None)
    result_eta = e_unit.estimate_segment_time(None, dataframe, calculation_begin_time, calculation_begin_time)

    # Assert
    assert result_etd < calculation_begin_time
    assert result_eta > calculation_begin_time


def test_create_ml_features():
    """ Tests the method create_ml_features in the estimation unit. """
    # Arrange
    dataframe = pd.DataFrame(data={pd.to_datetime(12829942197949895).round('15min'),
                                   pd.to_datetime(149019580543502912).round('15min'),
                                   pd.to_datetime(1442742198153886251).round('15min'),
                                   pd.to_datetime(851528185439451979).round('15min')},
                             columns={'time'})
    expected_output = dataframe.copy()
    expected_output['DayOfWeek'] = pd.to_datetime(expected_output['time']).dt.weekday
    expected_output['TimeOfDay'] = \
        pd.to_datetime(expected_output['time']).dt.hour * 60 + expected_output['time'].dt.minute
    expected_output.loc[expected_output['DayOfWeek'].isin([0, 1, 2, 3, 4]), 'WorkingDay'] = 1
    expected_output.loc[~(expected_output['DayOfWeek'].isin([0, 1, 2, 3, 4])), 'WorkingDay'] = 0

    # Act
    result = e_unit.create_ml_features(dataframe)
    # Assert
    assert_frame_equal(expected_output, result)


def arrange_calc_route_duration_data(references):
    """ Arrange a dataframe representing an assigned route df for ETA / ETD calculation. """
    street_name = 'Friedrich-Ebert-Stra\u00dfe'
    data = {'StreetName': {0: f'{street_name}', 1: f'{street_name}', 2: f'{street_name}'},
            'predefinedLocationReference': {0: references[0],
                                            1: references[1],
                                            2: references[2]},
            'SegDurationMins': {0: 0.0031000499, 1: 0.0248289009, 2: 0.0296677782},
            'avgSpeed': {0: 36.6691449814, 1: 36.6691449814, 2: 36.6691449814},
            'SegLength': {0: 0.001894603, 1: 0.0151742428, 2: 0.0181315344}
            }
    dataframe = pd.DataFrame(data=data, columns={'StreetName', 'predefinedLocationReference',
                                                 'SegDurationMins', 'avgSpeed', 'SegLength'})

    return dataframe
