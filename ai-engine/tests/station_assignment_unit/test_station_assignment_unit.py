#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the station assignment unit.
"""
import unittest
from unittest.mock import patch

import pandas as pd
import geopandas
import shapely.wkt
from shapely.geometry import Point, LineString
from pandas.testing import assert_frame_equal
from numpy import array_equal

import ai_engine.station_assignment_unit.station_assignment_unit as sau


class TestStationAssignmentUnit(unittest.TestCase):
    """ Test class for the station assignment unit (SAU). """
    route_df = pd.read_json('./tests/resources/routes/long_no_split.json')
    qv_locations_data = pd.read_json('./tests/resources/test_preprocessed_locations.json')

    def setUp(self):
        """ Set up before tests. """

    @patch("ai_engine.data_preprocessing_unit.qv_station_preprocessor.preprocess_qv_stations")
    def test_filter_and_geometrize_locations_df(self, qv_station_mock):
        """ Test the method filter_locations_df from the SAU. """
        # Arrange
        direction = 'SW'
        street_name = 'A 40'
        qv_locations_df = self.qv_locations_data.head(3).reset_index(drop=True)
        qv_station_mock.return_value = qv_locations_df
        expected_output = qv_locations_df.iloc[[1]]
        expected_output['geometry'] = Point(51.44999, 7.015504)
        expected_output = geopandas.GeoDataFrame(expected_output)

        # Act
        result = sau.filter_and_geometrize_locations_df(street_name, direction)

        # Assert
        assert_frame_equal(result.head(1).reset_index(drop=True), expected_output.reset_index(drop=True))

    def test_convert_section_to_path(self):
        """ Tests the convert_section_to_path method from the SAU. """
        # Arrange
        dataframe = self.route_df.head(15)

        expected_output = [[51.562462, 6.7348], [51.562447, 6.734813], [51.562344, 6.734957], [51.562193, 6.735056],
                           [51.562063, 6.735144], [51.561994, 6.735189], [51.561576, 6.735475], [51.561456, 6.735487],
                           [51.561167, 6.735672], [51.561086, 6.735725], [51.560772, 6.735944], [51.560603, 6.736075],
                           [51.560539, 6.73612], [51.560436, 6.736183], [51.56031, 6.736263], [51.560229, 6.736376]]
        expected_output = LineString(expected_output)

        # Act
        result = sau.convert_section_to_path(dataframe)

        # Assert
        self.assertEqual(expected_output, result)

    def test_add_geo_coordinates(self):
        """ Test the method add_geo_coordinates from the SAU. """
        # Arrange
        dataframe = self.route_df.head(1)
        expected_output = dataframe.copy()
        expected_output['_SegFrom'] = Point(51.562462, 6.7348)
        expected_output['_SegTo'] = Point(51.562447, 6.734813)

        # Act
        result = sau.add_geo_coordinates(dataframe)

        # Assert
        assert_frame_equal(result, expected_output)

    @patch("ai_engine.database_connector.read_dataframe")
    def test_assign_station_references(self, db_read_mock):
        """
        Should generate a list of the given route (as a data frame) and assign qv stations to
        the correct segments that will be merged into areas around the qv station.
        """
        # Arrange
        route_df = self.route_df.copy()
        route_df = route_df.loc[(route_df.index > 175) & (route_df.index < 179)]
        expected_output_data = [
            [0.031195, 69.499233, "L 462", 0.026931, Point(51.587834, 6.783108), Point(51.587937, 6.783528), None],
            [0.051986, 85.000000, "A 3", 0.036696, Point(51.587937, 6.783528), Point(51.587513, 6.783845),
             'fs.MQ_3.009_HFB_SW_1']
        ]
        expected_output = pd.DataFrame(
            columns=['SegLength', 'avgSpeed', 'StreetName', 'SegDurationMins',
                     'SegFrom', 'SegTo', 'predefinedLocationReference'],
            data=expected_output_data)
        db_read_mock.return_value = self.qv_locations_data

        # Act
        result = sau.assign_station_references(route_df)

        # Assert
        array_equal(result.values, expected_output.values)
        assert db_read_mock.call_count >= 1

    def test_fill_uncovered_segments_no_fill(self):
        """ Test fill uncovered segments where no filling is needed before and after the qv-section """
        # Arrange
        section_df, section_per_qv_df, expected_result_df = get_fill_uncovered_segments_data()
        section_df = section_df.loc[177:178]
        expected_result_df = expected_result_df.loc[[0]]

        # Act
        result = sau.fill_uncovered_segments(section_df, section_per_qv_df)

        # Assert
        print_df_infos(section_df, result, expected_result_df)
        assert_frame_equal(result, expected_result_df)

    def test_fill_uncovered_segments_fill_before(self):
        """ Test fill uncovered segments where filling is needed before the qv-section. """
        # Arrange
        section_df, section_per_qv_df, expected_result_df = get_fill_uncovered_segments_data()
        section_df = section_df.loc[176:178]
        expected_result_df = expected_result_df.head(2)

        # Act
        result = sau.fill_uncovered_segments(section_df, section_per_qv_df)

        # Assert
        print_df_infos(section_df, result, expected_result_df)
        assert_frame_equal(result, expected_result_df)

    def test_fill_uncovered_segments_fill_behind(self):
        """ Test fill uncovered segments where filling is needed behind the qv-section. """
        # Arrange
        section_df, section_per_qv_df, expected_result_df = get_fill_uncovered_segments_data()
        section_df = section_df.loc[177:179]
        expected_result_df = expected_result_df.tail(2)

        # Act
        result = sau.fill_uncovered_segments(section_df, section_per_qv_df)

        # Assert
        print_df_infos(section_df, result, expected_result_df)
        assert_frame_equal(result, expected_result_df)

    def test_fill_uncovered_segments_fill_both(self):
        """ Test fill uncovered segments where filling is needed before and behind the qv-section. """
        # Arrange
        section_df, section_per_qv_df, expected_result_df = get_fill_uncovered_segments_data()

        # Act
        result = sau.fill_uncovered_segments(section_df, section_per_qv_df)

        # Assert
        print_df_infos(section_df, result, expected_result_df)
        assert_frame_equal(result, expected_result_df)

    def test_append_location_references_empty_station(self):
        """ Tests the method append_location_references with an empty station. """
        # Arrange
        station_set = ["foo"]

        # Act
        result_set = sau.append_location_references(pd.DataFrame(), station_set)

        # Assert
        assert result_set[-1] is None


def test_get_distances():
    """ Test the method get_distances from the SAU. """
    # Arrange
    qv_section_sorted_df_data = [
        [Point(51.494443, 7.414872), Point(51.494496, 7.415263)],
        [Point(51.495163, 7.423962), Point(51.495046, 7.424764)]
    ]
    section_df_data = [
        [Point(51.494443, 7.414872), Point(51.494496, 7.415263), 2.5],
        [Point(51.495163, 7.423962), Point(51.495141, 7.424118), 6.6],
        [Point(51.495141, 7.424118), Point(51.495046, 7.424764), 3.4]
    ]
    qv_section_sorted_df = pd.DataFrame(columns=['_SegFrom', '_SegTo', ], data=qv_section_sorted_df_data)
    section_df = pd.DataFrame(columns=['_SegFrom', '_SegTo', 'SegLength'], data=section_df_data)
    distances = [2.5, 10]
    expected_output = qv_section_sorted_df.copy()
    expected_output['SegLength'] = distances

    # Act
    result = sau.get_distances(qv_section_sorted_df, section_df)

    # Assert
    assert_frame_equal(result, expected_output)


def test_get_direction():
    """ Test the method get_direction from the SAU. """
    # Arrange
    origin = [456.234651, 12.875669]
    destination = [489.201354, 45.802984]
    expected_result = 'NO'

    # Act
    result = sau.get_direction(origin, destination)

    # Assert
    assert expected_result == result


def get_fill_uncovered_segments_data():
    section_df = pd.read_json("./tests/resources/sau/test_sections_before_qv_matching.json")
    section_per_qv_df = pd.read_json("./tests/resources/sau/test_section_per_qv_df.json")
    expected_result_df = pd.read_json("./tests/resources/sau/test_route_with_filled_segments.json")
    section_df["_SegFrom"] = section_df["_SegFrom"].apply(lambda x: shapely.wkt.loads(x))
    section_df["_SegTo"] = section_df["_SegTo"].apply(lambda x: shapely.wkt.loads(x))
    section_df["avgSpeed"] = section_df["avgSpeed"].astype(float)
    section_per_qv_df["_SegFrom"] = section_per_qv_df["_SegFrom"].apply(lambda x: shapely.wkt.loads(x))
    section_per_qv_df["_SegTo"] = section_per_qv_df["_SegTo"].apply(lambda x: shapely.wkt.loads(x))
    section_per_qv_df["avgSpeed"] = section_per_qv_df["avgSpeed"].astype(float)
    expected_result_df["SegFrom"] = expected_result_df["SegFrom"].apply(lambda x: shapely.wkt.loads(x))
    expected_result_df["SegTo"] = expected_result_df["SegTo"].apply(lambda x: shapely.wkt.loads(x))

    return section_df, section_per_qv_df, expected_result_df


def print_df_infos(before, result, expected):
    print("________________")
    print("input:")
    print(before)
    print("Result (left): ")
    print(result)
    print("Expected (right):")
    print(expected)
