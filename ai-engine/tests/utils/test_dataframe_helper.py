#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

""" Test class for the dataframe helper. """
import unittest
import pandas
from pandas.testing import assert_frame_equal
import ai_engine.utils.dataframe_helper as df_helper


class TestDataframeHelper(unittest.TestCase):
    def test_add_calendar_features_to_dataframe(self):
        """ Tests the extraction and addition of date and time values to a given dataframe. """
        # Arrange
        dates = [pandas.to_datetime('2021-03-05T16:15:24+01:00'),
                 pandas.to_datetime('2022-11-25T15:07:43+01:00'),
                 pandas.to_datetime('2023-01-08T11:09:32+01:00')]

        input_dataframe = pandas.DataFrame(columns=['time'], data={'time': dates})

        expected_result = pandas.DataFrame(columns=['time', 'DayOfWeek', 'TimeOfDay', 'WorkingDay'],
                                           data={'time': dates,
                                                 'DayOfWeek': [4, 4, 6],
                                                 'TimeOfDay': [975, 907, 669],
                                                 'WorkingDay': [1.0, 1.0, 0.0],
                                                 }
                                           )

        # Act
        result = df_helper.add_calendar_features_to_dataframe(input_dataframe)

        # Assert
        assert_frame_equal(expected_result, result)
