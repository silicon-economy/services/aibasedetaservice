The test files are used in various tests and have special characteristics. This is a very brief description:

test_preprocessed_locations.json
	- used in: 
		- dpu/test_qv_station_preprocessor
		- dpu/test_road_partitioner
		- eu/test_estimation_unit
		- mfu/test_model_fitting_unit
		- sau/test_station_assignment_unit
	- content and meaning: 
		- The QV-stations with all preprocessed information i.e. with street names and directions attached.
		- pandas DataFrame
		- columns: highway, predefinedLocationReference, carriageway, lane, latitude, longitude, direction, 
		- result from data preprocessing
		- base data for every other unit

test_qvdata_2021-03-05_16.15.json
	- used in:
		- dpu/test_data_preprocessing_unit
		- mfu/test_model_fitting_unit
	- content and meaning:
		- for each QV-station, contains an entry for the QV-data at the timestamp 2021-03-05, 16:15
		- pandas DataFrame
		- columns: predefinedLocationReference, timestamp, anyVehicle_FlowRate, anyVehicle_Speed, lorryVehicle_FlowRate, lorry_Speed
		- Result of dpu
		- used as data for mfu 
		
ROUTES: 
base_no_departure.json
	- used in: eu/test_estimation_unit
	- content and meaning:
		- base json object (coming from external post) that contains a route with NO given departure time, meaning that the ETD will be calculated.
		
base_with_departure.json
	- used in: eu/test_estimation_unit
	- content and meaning: 
		- base json object (coming from external post) that contains a route with a departure time, meaning that the ETA will be calculated.
		
long_no_split.json
	- used in: sau/test_station_assignment_unit
	- content and meaning: 
		- Contains a long route that is already converted to a DataFrame
		- pandas DataFrame
		- columns: SegFrom, SegTo, SegLength, avgSpeed, StreetName, SegDurationMins
		
with_assigned_stations.json
	- used in: eu/test_estimation_unit
	- content and meaning: 
		- A route DataFrame that contains a route with assigned station references
		- pandas DataFrame
		- columns: SegLength, avgSpeed, StreetName, SegDurationMins, predefinedLocationReference, SegFrom, SegTo
		- in estimation unit tests, this route DataFrame is the return result of assign_station_references.


	