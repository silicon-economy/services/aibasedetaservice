#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
"""
Test class for the road partitioner.
"""
import unittest
from unittest.mock import patch
import pandas as pd
import ai_engine.data_preprocessing_unit.road_partitioner as road_partition


class TestRoadPartitioner(unittest.TestCase):
    """ Test class for the road partitioner. """

    qv_locations_data = pd.read_json('./tests/resources/test_preprocessed_locations.json')

    def setUp(self):
        """ Set up before tests. """

    @patch("ai_engine.database_connector.read_dataframe")
    def test_get_voronoi(self, db_read_mock):
        """
        Should return a voronoi diagram to the given street name and direction.
        If none is found, this method should return None.
        """
        # Prepare
        db_read_mock.return_value = self.qv_locations_data

        # Act
        none_result = road_partition.get_voronoi('a', 'b')
        result = road_partition.get_voronoi('A 40', 'SW')

        # Assert
        assert none_result is None
        assert result is not None  # Maybe assert a real voronoi object
        assert db_read_mock.call_count <= 1

    @patch("ai_engine.database_connector.read_dataframe")
    def test_is_in_diagram_list(self, db_read_mock):
        """ Returns true, if the diagram list contains an element with the given street name. """
        # Prepare
        db_read_mock.return_value = self.qv_locations_data

        # Act
        false_result = road_partition.is_in_diagram_list('XYZ')
        true_result = road_partition.is_in_diagram_list('A 40')

        # Assert
        assert not false_result
        assert true_result
        assert db_read_mock.call_count <= 1
