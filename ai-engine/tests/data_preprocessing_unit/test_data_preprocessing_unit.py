#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the data preprocessor.
"""
import unittest
from unittest.mock import patch
import pandas as pd
from pandas.testing import assert_frame_equal

import ai_engine.data_preprocessing_unit.data_preprocessing_unit as dpu


class TestDataPreprocessingUnit(unittest.TestCase):
    """ Test class for data preprocessor. """
    qv_station_mock_data = pd.read_json("./tests/resources/test_qvdata_2021-03-05_16.15.json").T

    def setUp(self):
        """ Set up before tests. """

    @patch("ai_engine.database_connector.read_dataframe")
    def test_get_preprocessed_qv_data(self, db_get_mock):
        """ Should collect and preprocess all data from the services accordingly. """
        # Prepare
        location_reference = "fs.MQ_1.110_AB_SW_R_1"
        columns = ['predefinedLocationReference', 'lorry_speed', 'time', 'DayOfWeek', 'TimeOfDay', 'WorkingDay']
        data = [[location_reference, 56.7, '2021-03-05 16:15:00+01:00', 4, 975, 1.0]]
        expected_output = pd.DataFrame(data, columns=columns)
        expected_output.time = pd.to_datetime(expected_output['time'])
        db_get_mock.return_value = self.qv_station_mock_data.loc[
                self.qv_station_mock_data['predefinedLocationReference'] == location_reference]

        # Act
        result = dpu.get_preprocessed_qv_data(location_reference)

        # Assert
        assert_frame_equal(result, expected_output)

    @patch("ai_engine.data_preprocessing_unit.data_preprocessing_unit.preprocess_locations")
    def test_thread(self, preprocessing_mock):
        # Act
        thread = dpu.PreprocessingThread("test thread", "tt", 1)
        thread.run()

        # Assert
        preprocessing_mock.assert_called_once()
