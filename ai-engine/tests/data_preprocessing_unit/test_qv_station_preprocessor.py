#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the qv station preprocessor.
"""

import unittest
from unittest.mock import patch

import pandas as pd
from pandas.testing import assert_frame_equal

from ai_engine.data_preprocessing_unit.qv_station_preprocessor import preprocess_qv_stations
from ai_engine.data_preprocessing_unit.qv_station_preprocessor import check_street_prefixes

expected_output = pd.read_json('./tests/resources/test_preprocessed_locations.json')


class MockHttpResponse:
    value = [{'display_name': 'A 1, XYZ'}]

    def __init__(self, val):
        self.value = [{"display_name": val['highway']}]

    def __init__(self):
        self.value = None

    def json(self):
        return self.value


def mock_geocode(value):
    row = expected_output.loc[expected_output['predefinedLocationReference']
                              == value['predefinedLocationReference']]
    if not row.empty:
        return MockHttpResponse(value), False
    return MockHttpResponse(), False


class TestQVStationPreprocessor(unittest.TestCase):
    """ Test class for qv station preprocessor. """

    def setUp(self):
        """ Set up before tests. """

    @patch("ai_engine.data_preprocessing_unit.qv_station_preprocessor.__geocode", side_effect=mock_geocode)
    @patch("ai_engine.database_connector.read_dataframe")
    def test_preprocess_qv_stations(self, db_read_mock, geocode_mock):
        """ Should collect qv stations, preprocess it and return a proper data frame. """
        # Prepare
        db_read_mock.return_value = None
        expected = expected_output.copy()

        # Act
        result = preprocess_qv_stations()

        # Assert
        assert_frame_equal(result, expected)
        assert db_read_mock.call_count == 2

    def test_check_street_prefixes(self):
        """ Test if street names get matched their correct prefix. """

        resp1 = [
            {'place_id': 20039545,
             'lon': '7.377258', 'display_name': 'A 45, Huckarde, Dortmund, Nordrhein-Westfalen, 44379, Deutschland'}]
        resp2 = [
            {'place_id': 20039545,
             'display_name': ' A 45, Huckarde, Dortmund, Nordrhein-Westfalen, 44379, Deutschland'}]
        resp3 = [
            {'place_id': 20039545,
             'display_name': 'Ruhrschnellweg, Huckarde, Dortmund, Nordrhein-Westfalen, 44379, Deutschland'}]
        resp4 = [
            {
                'place_id': 20039545,
                'display_name': 'Emscherschnellweg, Huckarde, Dortmund, Nordrhein-Westfalen, 44379, Deutschland'
            }]
        resp = [[resp1, 'A 45'], [resp2, 'A 45'], [resp3, 'A 40'], [resp4, 'A 42']]

        for _r in resp:
            result = check_street_prefixes(_r[0])
            assert result == _r[1]
