#!/bin/bash
# Command convenience file


function dev-install() {
  pipenv sync --dev
}

function dev-docker() {
  docker-compose -f docker-compose.dev.yml up && docker-compose -f docker-compose.dev.yml down -v
}

function dev-server() {
  FLASK_APP=eta_engine.server.py FLASK_ENV=development pipenv run python -m flask run
}

function lint() {
  pipenv run pylint *
}

function coverage() {
  PYTHONPATH=. pipenv run pytest --cov-report term:skip-covered --cov-report term-missing --cov=eta_engine
}

function clean() {
  sudo rm -rf docker/volumes.dev/*
}

function update() {
  echo "Updating pipenv ..."
  pipenv update
  pipenv clean
  echo "Updating docker ..."
  docker-compose -f docker-compose.dev.yml pull
}

function run_tests() {
  pipenv run pytest $1
}

function print_commands() {
  echo "Manage ETA-Engine commands"
  echo "Options:"
  echo " * dev-install                           - Install development dependencies"
  echo " * dev-docker                            - Run & cleanup docker for development"
  echo " * dev-server                            - Run the flask instance"
  echo " * lint                                  - Run pylint"
  echo " * coverage                              - Run pytest with coverage"
  echo " * clean                                 - Cleanup docker containers"
  echo " * update                                - Updates all dependencies, docker images, ... "
  echo " * test                                  - Run tests without coverage"
}

# === MENU ===
COMMAND=$1
COMMAND_MATCH=0

if [[ $COMMAND == "dev-install" ]] ; then
  COMMAND_MATCH=1
  dev-install
fi

if [[ $COMMAND == "dev-docker" ]] ; then
  COMMAND_MATCH=1
  dev-docker
fi

if [[ $COMMAND == "dev-server" ]] ; then
  COMMAND_MATCH=1
  dev-server
fi

if [[ $COMMAND == "lint" ]] ; then
  COMMAND_MATCH=1
  lint
fi

if [[ $COMMAND == "coverage" ]] ; then
  COMMAND_MATCH=1
  coverage
fi

if [[ $COMMAND == "clean" ]] ; then
  COMMAND_MATCH=1
  clean
fi

if [[ $COMMAND == "update" ]] ; then
  COMMAND_MATCH=1
  update
fi

if [[ $COMMAND == "test" ]] ; then
  COMMAND_MATCH=1
  run_tests $2
fi

if [ $COMMAND_MATCH -eq 0 ] ; then
  print_commands
fi
