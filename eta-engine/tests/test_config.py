#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Dummy test class for config.py to prevent negative effect on code coverage.
Should be replaced by a proper .coveragerc file to prevent config.py to be
included in the code coverage by SonarCube.
"""
import unittest

from eta_engine.config import ProductionConfig, DevelopmentConfig, TestConfig, TestAtServerConfig


class TestConfigDummy(unittest.TestCase):
    """ Test class for Config. """

    expected_segments = []

    def setUp(self):
        """ Set up before tests. """


def test_config():
    """ Calls all config code and asserts True to reach the code coverage. """
    # Prepare
    # Act
    prod_config = ProductionConfig()
    dev_config = DevelopmentConfig()
    test_configuration = TestConfig()
    test_server_config = TestAtServerConfig()

    # Assert
    assert prod_config.DEBUG is False
    assert prod_config.TESTING is False

    assert dev_config.DEBUG is True
    assert dev_config.TESTING is False

    assert test_configuration.DEBUG is False
    assert test_configuration.TESTING is True

    assert test_server_config.DEBUG is False
    assert test_server_config.TESTING is True
