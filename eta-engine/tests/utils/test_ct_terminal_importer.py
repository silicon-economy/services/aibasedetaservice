# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Test class for ct_terminal_importer.
"""
import unittest

from eta_engine.utils.ct_terminal_importer import CTTerminalImporter


class TestCTTerminalsImporter(unittest.TestCase):
    """ Test class for CT terminal importer. """

    ct_terminal_importer: CTTerminalImporter = None
    ctt_data = list()

    def setUp(self):
        """ Set up before tests. """
        data1 = dict()
        data1['name'] = 'KV Alb-Donau-Kreis (Ulm)'
        data1['node'] = 3834427
        data1['lat'] = 48.49008
        data1['lng'] = 9.86212
        data1['ct_type'] = 'WATER'

        data2 = dict()
        data2['name'] = 'KV Altötting'
        data2['node'] = 4135895
        data2['lat'] = 48.13783
        data2['lng'] = 12.78985
        data2['ct_type'] = 'TRIMODAL'

        data3 = dict()
        data3['name'] = 'KV Anhalt-Bitterfeld'
        data3['node'] = 4254606
        data3['lat'] = 51.85413
        data3['lng'] = 12.05334
        data3['ct_type'] = 'RAIL'

        self.ctt_data.append(data1)
        self.ctt_data.append(data2)
        self.ctt_data.append(data3)

        self.ct_terminal_importer = CTTerminalImporter('./tests/resources/test_ct_terminals.csv')

    def test_import_ct_terminals(self):
        """ Should import CT terminals correctly. """
        # Act
        self.ct_terminal_importer.import_ct_terminals()
        result = self.ct_terminal_importer.ct_terminal_collection

        # Assert
        for index in range(0, len(self.ctt_data)):
            assert self.ctt_data[index] == result[index]
