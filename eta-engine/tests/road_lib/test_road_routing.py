#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Test class for the route preprocessor.
"""
import os
import json
import unittest

from eta_engine.road_lib.road_routing import calc_route
import eta_engine.parameter_names

TIMESTAMP = '2020-11-16T14:48:31.950Z'


class TestRoadRouting(unittest.TestCase):
    """ Test class for Road Routing. """

    json_object = {"source": "Emil-Figge-Straße, 44227 Dortmund",
                   "destination": "Rosemeyerstraße 4-2, 44139 Dortmund",
                   "timeCorridor": {"from": TIMESTAMP, "to": TIMESTAMP},
                   "departure": TIMESTAMP}
    expected_json = ''
    expected_segments = []

    def setUp(self):
        """ Set up before tests. """
        with open('./tests/resources/test_route_json', mode='r', encoding='utf-8') as file:
            self.expected_json = file.read()
        with open('./tests/resources/test_route_segments', mode='r', encoding='utf-8') as file:
            self.expected_segments = json.loads(file.read())

    def test_calc_road(self):
        """ Should preprocess the test route data and return a proper data frame. """
        # Arrange
        location_source = (51.4944429, 7.4148716)
        location_destination = (51.4955761, 7.4419208)

        valhalla_host = os.environ.get(eta_engine.parameter_names.VALHALLA_HOSTNAME,
                                       "https://osm-valhalla.apps.sele.iml.fraunhofer.de")
        valhalla_port = os.environ.get(eta_engine.parameter_names.VALHALLA_PORT, "443")
        routing_url = f"{valhalla_host}:{valhalla_port}/route?json="

        # Act
        route_result, segments_result, duration = \
            calc_route(location_source, location_destination, routing_url, self.json_object)

        # Assert
        assert route_result == self.expected_json
        assert segments_result == self.expected_segments
        assert duration != 0
