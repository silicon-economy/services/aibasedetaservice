#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Test class for the route preprocessor
"""

import unittest
import json

import pandas as pd
from eta_engine.road_lib.route_preprocessor import preprocess


class TestRoutePreprocessor(unittest.TestCase):
    """ Test class for Route Preprocessor. """

    route_data = b''
    expected_segment_df = pd.read_json('./tests/resources/test_segment_df.json', orient='split')
    expected_segments = []

    def setUp(self):
        """ Set up before tests. """
        with open('./tests/resources/test_route_preprocessor_req_data', mode='rb') as file:
            self.route_data = file.read()
        with open('./tests/resources/test_route_segments', mode='r', encoding='utf-8') as file:
            self.expected_segments = json.loads(file.read())

    def test_preprocess(self):
        """ Should preprocess the test route data and return a proper data frame. """
        # Act
        route_result, segments_result, duration = preprocess(self.route_data)

        # Assert
        assert route_result == self.expected_segment_df.to_json(orient='split', index=False)
        assert segments_result == self.expected_segments
        assert duration != 0

    def test_preprocess_no_trip_in_data(self):
        """ The preprocess function should abort. """
        # Arrange
        data = json.dumps({"key": "This is a test object."})
        data = data.encode('utf-8')

        # Act & Assert
        with self.assertRaises(KeyError):
            preprocess(data)
