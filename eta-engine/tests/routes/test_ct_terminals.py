#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Tests for the CT terminal resource.
"""


CT_TERMINAL_API = 'api/v1/ct-terminals'


def test_get_ct_terminals(client):
    """ Should return all CT terminals. """
    # Act
    response = client.get(CT_TERMINAL_API)

    # Assert
    assert b'''"lat": 48.49008''' in response.data
    assert b'''"lng": 9.86212''' in response.data
    assert b'''"name": "KV Alb-Donau-Kreis (Ulm)"''' in response.data
