#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Test class for the route preprocessor.
"""
import json
import unittest
from unittest.mock import patch

from eta_engine.intermodal_routing import intermodal_routing, get_geo_coordinate_from_address

TIMESTAMP = '2020-11-16T14:48:31.950Z'


class TestIntermodalRouting(unittest.TestCase):
    """ Test class for Intermodal Routing. """

    json_object_eta = {"source": "Emil-Figge-Straße, 44227 Dortmund",
                       "destination": "Rosemeyerstraße 4-2, 44139 Dortmund",
                       "timeCorridor": {"from": TIMESTAMP, "to": TIMESTAMP},
                       "departure": TIMESTAMP}
    json_object_etd = {"source": "Emil-Figge-Straße, 44227 Dortmund",
                       "destination": "Rosemeyerstraße 4-2, 44139 Dortmund",
                       "timeCorridor": {"from": TIMESTAMP, "to": TIMESTAMP}}
    expected_json = ''
    expected_segments = []

    def setUp(self):
        """ Set up before tests. """
        with open('./tests/resources/test_route_json', mode='r', encoding='utf-8') as file:
            self.expected_json = file.read()
        with open('./tests/resources/test_route_segments', mode='r', encoding='utf-8') as file:
            self.expected_segments = json.loads(file.read())

    @patch('eta_engine.intermodal_routing.requests.post')
    def test_intermodal_routing_eta(self, mock_post):
        """ Should preprocess the test route data and return a proper data frame. """
        # Arrange
        mock_post.return_value.json.return_value = 3.31
        expected_output = {
            "source": {"lat": 51.4944429, "lng": 7.4148716},
            "destination": {"lat": 51.4955761, "lng": 7.4419208},
            "timestamp": "2020-11-16T14:51:50.550000Z",
            "type": "ETA",
            "routeSegmentsByTransportMode": [{"transportMode": "ROAD", "routeSegments": self.expected_segments}]}

        # Act
        result = intermodal_routing(self.json_object_eta)

        # Assert
        assert result == expected_output

    @patch('eta_engine.intermodal_routing.requests.post')
    def test_intermodal_routing_etd(self, mock_post):
        """ Should preprocess the test route data and return a proper data frame. """
        # Arrange
        mock_post.return_value.json.return_value = 3.31
        expected_output = {
            "source": {"lat": 51.4944429, "lng": 7.4148716},
            "destination": {"lat": 51.4955761, "lng": 7.4419208},
            "timestamp": "2020-11-16T14:45:13.350000Z",
            "type": "ETD",
            "routeSegmentsByTransportMode": [{"transportMode": "ROAD", "routeSegments": self.expected_segments}]}

        # Act
        result = intermodal_routing(self.json_object_etd)

        # Assert
        assert result == expected_output

    @patch("eta_engine.intermodal_routing.request_geocoder_with_address")
    def test_get_geo_coordinate_from_address(self, mock_request):
        """ Should extract latitude and longitude from location objects. """
        # Arrange
        address = "Test"
        mock_request.return_value = [{'place_id': 20999099, 'lat': '51.4944429', 'lon': '7.4148716'}]
        expected = (51.4944429, 7.4148716)

        # Act
        result = get_geo_coordinate_from_address(address)

        # Assert
        assert result == expected

    @patch("eta_engine.intermodal_routing.request_geocoder_with_address")
    def test_get_geo_coordinate_from_address_with_error(self, mock_request):
        """ Should extract latitude and longitude from location objects. """
        # Arrange
        address = "Test"
        mock_request.return_value = [{'place_id': 20999099, 'class': 'highway', 'type': 'tertiary'}]
        expected = (0, 0)

        # Act
        result = get_geo_coordinate_from_address(address)

        # Assert
        assert result == expected
