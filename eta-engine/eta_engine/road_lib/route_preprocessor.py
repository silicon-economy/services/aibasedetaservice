#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function to decode the route information from Valhalla and enrich them with additional
information.
"""
import json
import pandas as pd
from geopy.distance import great_circle


def preprocess(data):
    """
    Takes route data from a routing service and extracts the route segments. This method also preprocesses the route
    data to obtain additional information for the route segments such as the length of the segments and the estimated
    duration a truck needs to pass them.

    :param data: The actual route from the routing service.
    :type data: utf-8 encoded json data

    :returns: The route segments in json format.
    :rtype: string

    :returns: The route segments, as a List
    :rtype: list

    :returns: The duration of the route
    :rtype: float
    """
    maneuvers, shape = __decode_request_data(data)

    # Get maneuvers and adjust maximum avgSpeed and re-calculate driving durations
    df_maneuvers = __get_maneuvers(maneuvers)
    df_maneuvers = df_maneuvers.loc[~(df_maneuvers['avgSpeed'].isna())]
    df_maneuvers.loc[df_maneuvers['avgSpeed'] > 85, 'avgSpeed'] = 85
    df_maneuvers[2] = df_maneuvers[3] / df_maneuvers['avgSpeed'] * 3600

    # Decode the segments shape and order them
    segments = __decode(shape)
    segments_ordered = []
    for i in segments:
        segments_ordered.append([i[1], i[0]])

    # Calculate distance / length for each segment
    segment_len = []
    for j in range(0, len(segments_ordered) - 1):
        coord1 = segments_ordered[j]
        coord2 = segments_ordered[j + 1]
        distance = great_circle(coord1, coord2).km
        segment_len.append([segments_ordered[j], segments_ordered[j + 1], distance])

    # Prepare final data frame (init with segment_len and merge with df_maneuvers)
    segment_df = pd.DataFrame(segment_len).reset_index()
    segment_df = segment_df.merge(df_maneuvers, left_on='index', right_on=0, how='left').ffill().drop(
        columns={'index', '0_y', '1_y', '2_y', 3, 4}, axis=1).rename(
        columns={'0_x': 'SegFrom', '1_x': 'SegTo', '2_x': 'SegLength'})
    segment_df['SegDurationMins'] = (segment_df['SegLength'] / segment_df['avgSpeed']) * 60
    segments = list(segment_df["SegFrom"])
    duration = segment_df["SegDurationMins"].sum()
    json_string = segment_df.to_json(orient='split', index=False)

    return json_string, segments, duration


def __decode_request_data(data):
    """
    Decode the request data, extracts the maneuvers and the shape of the trip.
    :param data: The actual route from the routing service.
    :type data: utf-8 encoded json data

    :returns: The maneuvers of the trip within the route data.
    :rtype: dictionary

    :returns: The shape of the trip within the route data.
    :rtype: utf-8 encoded json data
    """
    data_string = data.decode('utf-8')
    data_dict = json.loads(data_string)
    try:
        maneuvers = data_dict['trip']['legs'][0]['maneuvers']
        shape = data_dict['trip']['legs'][0]['shape']
    except KeyError as key_error:
        raise KeyError('No trip found in data:\n' + str(data_dict) + '\n\nError:\n' + str(key_error)) from KeyError
    return maneuvers, shape


def __decode(encoded):
    """
    Decodes the route shape.
    This method is kindly granted by the valhalla project:
    https://github.com/valhalla/valhalla-docs/blob/master/decoding.md
    """
    # Six degrees of precision in valhalla
    inv = 1.0 / 1e6

    decoded = []
    previous = [0, 0]
    i = 0
    # For each byte
    while i < len(encoded):
        # For each coordinate (lat, lon)
        lat_lon = [0, 0]
        for j in [0, 1]:
            shift = 0
            byte = 0x20

            # Keep decoding bytes until you have this coord
            while byte >= 0x20:
                byte = ord(encoded[i]) - 63
                i += 1
                lat_lon[j] |= (byte & 0x1f) << shift
                shift += 5

            # Get the final value adding the previous offset and remember it for the next
            lat_lon[j] = previous[j] + (~(lat_lon[j] >> 1) if lat_lon[j] & 1 else (lat_lon[j] >> 1))
            previous[j] = lat_lon[j]

        # Scale by the precision and chop off long coordinates also flip the positions, so
        # it's the far more standard lon,lat instead of lat,lon
        decoded.append([float(f'{(lat_lon[1] * inv):.6f}'), float(f'{(lat_lon[0] * inv):.6f}')])

    # Hand back the list of coordinates
    return decoded


def __get_maneuvers(maneuvers):
    """
    Writes the decoded data from the given dictionary into a DataFrame.
    Also fills up the na-values and generates the avgSpeed per Entry.

    :param maneuvers: The maneuvers of a route
    :type maneuvers: dictionary

    :returns: DataFrame containing maneuvers
    :rtype: Function call with a DataFrame as Parameter
    """
    _maneuvers = []

    for i in maneuvers:
        try:
            _maneuvers.append([i['begin_shape_index'], i['end_shape_index'], i['time'], i['length'], i['street_names']])
        except KeyError:
            _maneuvers.append([i['begin_shape_index'], i['end_shape_index'], i['time'], i['length'], None])

    df_maneuvers = pd.DataFrame(_maneuvers)
    df_maneuvers[4] = df_maneuvers[4].fillna(method='ffill')
    df_maneuvers[4] = df_maneuvers[4].fillna(method='bfill')
    df_maneuvers['avgSpeed'] = df_maneuvers[3] / df_maneuvers[2] * 3600

    return __determine_street_names(df_maneuvers)


def __determine_street_names(maneuvers):
    """
    Extracts the street names from the maneuvers data frame. For the extraction the common street name prefixes are
    used. In a special case if either of the highways A40 or A42 is found with its common name 'Ruhrschnellweg' or
    'Emscherschnellweg' respectively this is changed to the A40 or A42 representation instead.

    :param maneuvers: DataFrame containing all maneuvers of a route.
    :type maneuvers: Pandas DataFrame

    :returns: The street names of a maneuver
    :rtype: Pandas DataFrame
    """
    _prefixes = ['A ', 'B ', 'E ', 'L ', 'K ']
    _streets = []

    for j in maneuvers[4]:
        _streets.append(j[0])
        for i in j:
            if any(prefix in i for prefix in _prefixes):
                _streets[len(_streets) - 1] = i
            elif 'Ruhrschnellweg' in i:
                _streets[len(_streets) - 1] = 'A 40'
            elif 'Emscherschnellweg' in i:
                _streets[len(_streets) - 1] = 'A 42'

    return maneuvers.merge(pd.DataFrame(_streets, columns=['StreetName']), left_index=True, right_index=True)
