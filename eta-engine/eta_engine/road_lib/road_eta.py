#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
This module offers functions to calculate arrival/departure time depending on a given timestamp and the duration
of a route.
"""

import datetime as dt


def calc_estimated_time(timestamp, duration, eta: bool):
    """
    Calculates the estimated time of arrival (ETA) or estimated time of departure (ETD) for the given
    timestamp and duration. Whether an ETA or an ETD is calculated can be derived from the bool parameter eta.

    :param timestamp: Timestamp, when the transport should depart or arrive.
    :type timestamp: string

    :param duration: Duration of the transport (e.g. calculated by the AI engine) in minutes.
    :type duration: float

    :param eta: True, if you like to determine the ETA, else an ETD is calculated
    :type eta: bool

    :returns: ETA (eta = True) or ETD (eta = False) of the transport,
    :rtype: datetime object
    """
    try:
        date_time_obj = dt.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')
    except ValueError:
        date_time_obj = dt.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')

    if eta:
        estimated_time = date_time_obj + dt.timedelta(minutes=duration)
    else:
        estimated_time = date_time_obj - dt.timedelta(minutes=duration)
    estimated_time = estimated_time.isoformat() + "Z"

    return estimated_time
