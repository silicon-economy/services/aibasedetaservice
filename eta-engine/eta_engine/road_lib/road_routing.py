#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function to calculate a route between two GeoCoordinates.
"""

import json
import urllib3
from flask import abort
from eta_engine.road_lib.route_preprocessor import preprocess


def calc_route(source, destination, routing_url, json_object):
    """
    Calculates a route between source and destination.

    :param source: GeoCoded information on the transport source.
    :type source: Tuple[float, float]

    :param destination: GeoCoded information on the transport destination.
    :type destination: Tuple[float, float]

    :param routing_url: URL of routing service.
    :type routing_url: string

    :param json_object: the request from the frontend
    :type json_object: dict

    :returns: the route information, i.e. segments as well as the information from the given json object in json format.
    :rtype: string

    :returns: List of route segments to be traversed.
    :rtype: list
    """
    # Create a dictionary for the routing request
    payload = {"locations": [{"lat": source[0], "lon": source[1]},
                             {"lat": destination[0], "lon": destination[1]}],
               "costing": "truck",
               "costing_options":
                   {"truck": {"maneuver_penalty": 66}},
               "language": "de"
               }

    # Request the routing service
    http = urllib3.PoolManager()
    encoded_data = json.dumps(payload).encode('utf-8')
    req = {}
    try:
        req = http.request('GET', routing_url, body=encoded_data, headers={'Content-Type': 'application/json'})
    except (TimeoutError, ConnectionError) as exception:
        abort(503, 'no connection to routing engine: ' + str(exception))

    route = {}
    segments = []
    duration = -1
    try:
        route, segments, duration = preprocess(req.data)
    except KeyError as key_error:
        abort(500, 'Route-Preprocessing failed: ' + str(key_error))

    # Add parameters for use in AI-engine
    route = route[:-1] + ',' + json.dumps(json_object)[1:]

    return route, segments, duration
