#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Utils module for little utility methods.
"""


def cast_to_type(value: str, cast_type: type):
    """
    Casts a string to a given type and returns None, if the cast fails.
    """
    try:
        cast = cast_type(value)
    except ValueError:
        cast = None
    return cast
