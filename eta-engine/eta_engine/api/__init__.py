#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
"""
API version 1 specification

All resources are added in their own namespaces
"""


from flask import Blueprint
from flask_restx import Api

from eta_engine.routes.eta_engine import api as eta_engine
from eta_engine.routes.ct_terminals import api as ct_terminals


# create the api endpoint
api_v1_bp = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(api_v1_bp,
          version='1.0',
          title='ETA engine API',
          description='The ETA-Engine for the route calculations.')


# add namespace here
api.add_namespace(ct_terminals)
api.add_namespace(eta_engine)


__all__ = ["api_v1_bp"]
