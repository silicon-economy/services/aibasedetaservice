#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
API routes for the ETA-Engine routes calculation.
"""
from flask import request
from flask_restx import Namespace, Resource
from eta_engine.intermodal_routing import intermodal_routing

api = Namespace('eta-engine', description="The ETA engine.")


@api.route('')
class EtaEngine(Resource):
    """ The ETA engine. """

    def post(self):
        """
        Routes an ETA-request to the intermodal routing engine.

        Receives a JSON with the following structure:
        {
        "source": "string",
        "destination": "string",
        "timeCorridor": {
            "from": "yyyy-MM-ddTHH:mm:ss.SSSZ",
            "to": "yyyy-MM-ddTHH:mm:ss.SSSZ"
        },
        "departure": "yyyy-MM-ddTHH:mm:ss.SSSZ",
        "transportModes": [
            "ROAD",
            "RAIL",
            "WATER",
            "AIR
        ],
        "optimizeBy": "TIME"
        }

        Returns a JSON with structure:
        {
        "source": {
            "lat": 51.493,
            "lng": 7.407
        },
        "target": {
            "lat": 52.562,
            "lng": 12.513
        },
        "timestamp": "yyyy-MM-ddTHH:mm:ss.SSSZ",
        "type": "ETA",
        "routeSegments": [
            {
            "transportMode": "ROAD",
            "routeSegments": [
                [51.493, 7.407], ..., [52.562, 12.513]
            ]
            }
        ]
        } """
        return intermodal_routing(request.get_json(force=True))
