#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Returns the CT terminals as stored in the database.
"""
import json
from flask_restx import Namespace, Resource, fields

api = Namespace('ct-terminals', description="The CT terminals.")

ct_model = api.model(
    "CTTerminal",
    {
        'name': fields.String(required=False, description='The name for this Terminal.'),
        'node': fields.Integer(required=False, description='The OSM node for this Terminal.'),
        'lat': fields.Float(required=False, description='The latitude.'),
        'lng': fields.Float(required=False, description='The longitude.'),
        'ct_type': fields.String(required=True,
                                 description='The type representing this CT terminal.',
                                 enum=['RAIL', 'WATER', 'TRIMODAL'])
    }
)


@api.route('')
class EtaEngine(Resource):
    """ This endpoint returns the CT terminals as stored in the database. """

    @api.marshal_list_with(ct_model)
    def get(self):
        """ Returns all CT terminals as stored in the database. """
        with open('./resources/ct_terminals.json', encoding='utf-8') as file:
            cursor = json.load(file)
        return list(_ctt for _ctt in cursor), 200
