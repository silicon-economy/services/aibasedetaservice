#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
Some constants for parameter names.
"""

VALHALLA_HOSTNAME = "VALHALLA_HOSTNAME"
VALHALLA_PORT = "VALHALLA_PORT"
NOMINATIM_HOSTNAME = "NOMINATIM_HOSTNAME"
NOMINATIM_PORT = "NOMINATIM_PORT"
AI_ENGINE_HOSTNAME = "AI_ENGINE_HOSTNAME"
AI_ENGINE_PORT = "AI_ENGINE_PORT"
