#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
CT terminals importer. Imports CT terminals from a given csv file to the database.
"""
import csv
import json

from eta_engine.util import cast_to_type


class CTTerminalImporter:
    """
    An importer for importing all CT terminals from a csv file stored in the resources folder to the database.
    """
    path: str = None
    ct_terminal_collection = None

    def __init__(self, path: str) -> None:
        """
        Init collection and path for csv import.

        :param path: a string containing the path to the desired terminals csv file.
        :type path: string
        """
        self.path = path
        self.ct_terminal_collection = list()

    def import_ct_terminals(self) -> None:
        """ Import the CT terminals from the csv file. The path was submitted in the constructor."""
        with open(self.path, newline='', encoding='utf-8') as csvFile:
            workbook = csv.reader(csvFile, delimiter=';')
            for row in workbook:
                ct_terminal = dict()
                ct_terminal['name'] = row[0]
                ct_terminal['node'] = cast_to_type(row[1], int)
                ct_terminal['lat'] = cast_to_type(row[2].replace(',', '.'), float)
                ct_terminal['lng'] = cast_to_type(row[3].replace(',', '.'), float)
                rail = row[5]
                water = row[6]
                if rail == '1' and water == '1':
                    ct_terminal['ct_type'] = 'TRIMODAL'
                elif water == '1':
                    ct_terminal['ct_type'] = 'WATER'
                else:
                    ct_terminal['ct_type'] = 'RAIL'
                self.ct_terminal_collection.append(ct_terminal)

        json_string = json.dumps(self.ct_terminal_collection)
        json_file = open('./resources/ct_terminals.json', 'w')
        json_file.write(json_string)
        json_file.close()
