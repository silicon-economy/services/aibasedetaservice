#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
This module offers a function to determine a route between two geocoded locations.
"""
import logging
import os
from urllib import parse
import requests
from flask import abort
from eta_engine.road_lib import road_routing
from eta_engine.road_lib import road_eta

import eta_engine.parameter_names

nominatim_host = os.environ.get(eta_engine.parameter_names.NOMINATIM_HOSTNAME,
                                "https://osm-nominatim.apps.sele.iml.fraunhofer.de")
nominatim_port = os.environ.get(eta_engine.parameter_names.NOMINATIM_PORT, "443")
GEOCODE_URL = f"{nominatim_host}:{nominatim_port}/search?q="

valhalla_host = os.environ.get(eta_engine.parameter_names.VALHALLA_HOSTNAME,
                               "https://osm-valhalla.apps.sele.iml.fraunhofer.de")
valhalla_port = os.environ.get(eta_engine.parameter_names.VALHALLA_PORT, "443")
ROUTING_URL = f"{valhalla_host}:{valhalla_port}/route?json="

ai_engine_host = os.environ.get(eta_engine.parameter_names.AI_ENGINE_HOSTNAME, "http://localhost")
ai_engine_port = os.environ.get(eta_engine.parameter_names.AI_ENGINE_PORT, "5003")
AI_ENGINE_URL = f"{ai_engine_host}:{ai_engine_port}/api/v1/ai-engine"


def intermodal_routing(json_object):
    """
    Intermodal Routing Engine.
    Performs geocoding on the given JSON input, calculates route segments and transport time.

    :param json_object:
        JSON object containing the necessary information to process the request. (see routes/eta_engine.py)
    :type json_object: dict

    :returns: Source, destination, ETA or ETD and segmented route by mode. (see routes/eta_engine.py)
    :rtype: dict

    :raises Exception: In case of false routing input parameters (source and/or destination), not established
    connection with geocoding service or routing service or impossible routing request (i.e. no connecting route
    existing between source and destination)
    """
    logging.info("ETA Engine was called with the request\n %s", str(json_object))
    source = json_object['source']
    destination = json_object['destination']
    arrival = json_object['timeCorridor']['from']
    departure = json_object['departure'] if 'departure' in json_object else None

    # Get Latitude and Longitude from locations
    lat_lon_src = get_geo_coordinate_from_address(source)
    lat_lon_dst = get_geo_coordinate_from_address(destination)

    # Determine route between source and destination
    route, segments, duration = road_routing.calc_route(lat_lon_src, lat_lon_dst, ROUTING_URL, json_object)

    # Get ETA / ETD from AI engine
    duration = calculate_estimated_duration(route, duration)

    # Build dictionary with obtained information
    if departure is None:
        output = {"source": {"lat": lat_lon_src[0],
                             "lng": lat_lon_src[1]},
                  "destination": {"lat": lat_lon_dst[0],
                                  "lng": lat_lon_dst[1]},
                  "timestamp": road_eta.calc_estimated_time(arrival, duration, False),
                  "type": "ETD",
                  "routeSegmentsByTransportMode":
                      [{"transportMode": "ROAD", "routeSegments": segments}]}
    else:
        output = {"source": {"lat": lat_lon_src[0],
                             "lng": lat_lon_src[1]},
                  "destination": {"lat": lat_lon_dst[0],
                                  "lng": lat_lon_dst[1]},
                  "timestamp": road_eta.calc_estimated_time(departure, duration, True),
                  "type": "ETA",
                  "routeSegmentsByTransportMode":
                      [{"transportMode": "ROAD", "routeSegments": segments}]}

    logging.info("Final route output from intermodal routing: \n %s", str(output))
    return output


def get_geo_coordinate_from_address(location):
    """
    Gets a string address of a location and tries to map that to a geo coordinate in form
    of latitude and longitude.
    :param location: the address
    :type location: str

    :returns: latitude and longitude of the given location
    """
    geo_location = request_geocoder_with_address(location)
    try:
        lat_lon = (float(geo_location[0]['lat']), float(geo_location[0]['lon']))
    except KeyError:
        lat_lon = (0, 0)
        logging.error("No coordinate extractable from geo location. See message below leading to (0,0).")

    logging.info("Source extraction: %s\n -> %s\n -> %s", str(location), str(geo_location), str(lat_lon))

    return lat_lon


def request_geocoder_with_address(location):
    """
    Takes a location reference as input and tries to extract the respective
    geo-coordinate from the geocoder service.

    :param location: The location information
    :type location: JSON Object

    :returns geo_location: The geo-coordinate for the location.

    :raises Exceptions: In case of invalid location parameter or in case of inability to connect to the
                        geocoder service.
    """
    geo_location = [{}]
    try:
        geo_location = requests.get(GEOCODE_URL + parse.quote_plus(location) + '&format=json&limit=1', timeout=5).json()
    except IndexError:
        abort(400, 'location not valid')
    except requests.RequestException as request_exception:
        abort(503, 'no connection to geocoding engine: ' + str(request_exception))

    return geo_location


def calculate_estimated_duration(route, duration):
    """
    Connects with the AI_Engine to determine the estimated arrival or departure time for the given route.

    :param route: The information of the route.
    :type route: JSON String
    :param duration: The initial route duration without use of the AI_Engine.
    :type duration:

    :returns duration: The arrival or departure time as calculated by the AI_Engine.

    :raises Exception: In case of incapability to connect with the AI_Engine.
    """
    _duration = duration
    try:
        _duration = requests.post(AI_ENGINE_URL, json=route, timeout=5).json()
        logging.info("Connected to AI Engine and got estimated duration of %f", _duration)
    except requests.RequestException as request_exception:
        logging.warning("Connection to AI engine failed. Use standard route duration estimation of %f -- "
                        "Caught Exception: %s", _duration, str(request_exception))

    return _duration
