#  Copyright Open Logistics Foundation
#  #
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3

"""
This module is designed to start a flask server. This is the entry point for external consumers to
interact with the python-backend.

Initializes flask server.
Initializes loggers.
"""

import logging
import os

from flask import Flask
from flask_cors import CORS

from eta_engine.utils.ct_terminal_importer import CTTerminalImporter
from eta_engine.api import api_v1_bp

BACKEND_DEBUG_MODE_PARAM_NAME = "BACKEND_DEBUG_MODE"
backend_debug = os.getenv(BACKEND_DEBUG_MODE_PARAM_NAME, "False") == "True"  # don't use bool()


def create_app(config='production') -> Flask:
    """
    Creates flask app.
    Registers blueprint for the eta api.
    Removes CORS filter.

    :return: the flask app
    """
    flask_app = Flask(__name__)

    if flask_app.config['ENV'] == 'development' or config == 'development':
        flask_app.config.from_object('eta_engine.config.DevelopmentConfig')
    elif config == 'production':
        flask_app.config.from_object('eta_engine.config.ProductionConfig')
    elif config == 'test':
        if os.environ.get('WHERE_ARE_WE_TESTING') is not None:
            flask_app.config.from_object('eta_engine.config.TestAtServerConfig')
        else:
            flask_app.config.from_object('eta_engine.config.TestConfig')

    # Init loggers
    init_loggers()

    # Register blueprints
    register_blueprints(flask_app)

    # Add CORS header
    CORS(flask_app)

    if flask_app.config['ENV'] == 'development' or config == 'production':
        # Import CT terminals
        import_ct_terminals()
#
#        # Import CT schedules
#        import_ct_schedules()

    return flask_app


def init_loggers():
    """
    Initializes loggers.
    """
    logging.basicConfig(level=logging.INFO)


def register_blueprints(flask_app: Flask):
    """
    Register all blueprints used in the python-backend.
    """
    flask_app.register_blueprint(api_v1_bp)


def import_ct_terminals():
    """
    Imports CT terminals to the DB.
    """
    logging.info("Start importing CT terminals...")

    # Import CT terminals into the DB (future release)
    CTTerminalImporter('./resources/ct_terminals.csv').import_ct_terminals()
#
    logging.info("Finished importing CT terminals")


if __name__ == '__main__':
    app = create_app()
    logging.info("Starting ETA-Engine Python backend!")

    app.run(host="0.0.0.0", debug=True)
