#  Copyright Open Logistics Foundation
#
#  Licensed under the Open Logistics Foundation License 1.3.
#  For details on the licensing terms, see the LICENSE file.
#  SPDX-License-Identifier: OLFL-1.3


""" Setup of the ETA engine. """

from setuptools import setup

setup(
    name='aibasedetaservice.eta-engine',
    version='1.0.0',
    packages=['tests', 'eta_engine', 'eta_engine.api', 'eta_engine.routes', 'eta_engine.road_lib',
              'eta_engine.rail_lib', 'eta_engine.utils'],
    url='https://www.siliconeconomy.org',
    license='Open Logistics License 1.0',
    author='Kai Hannemann',
    author_email='kai.hannemann@iml.fraunhofer.de',
    description='The backend for the ETA-Engine.',
    install_requires=['Flask',
                      'Flask-Cors',
                      'flask-restx',
                      'openpyxl'],
    entry_points={
        "console_scripts": [
            'server=eta_engine:server'
        ]
    }
)
