> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

# AI based ETA service

## Project Overview
The AI based ETA service provides the user with an ability to predict an optimized (intermodal) route and associated ETA (Estimated Time of Arrival) or ETD (Estimated Time of Departure) for a freight transport.
Furthermore the user is able to pick individual sub-services from this software product as components for his own buisness model.

The AI baset ETA service is decomposed into five sub-services:
* The **angular frontend** (The web-interface for the user to interact with the service),
* the **ETA service** (The management of data that comes from the frontend, like created transports from the user),
* the **ETA engine** (The core engine that takes the information from the ETA service, calculates a route by utilizing geocoding and routing services and gets an ETA or ETD from the AI engine),
* the **AI engine** (Computes an ETA / ETD based on route data as well as data from the QV service.),
* the **QV service** (Fetches, stores and delivers q,V-data)

For further detail on the functionalities and features of the services, see each services own readme file. The full documentation according to [arc42](https://arc42.org) can be found in the [documentation directory](documentation/index.adoc).

The current first version (1.0) contains the functional components of all sub-services.

|Component|Status|
|---------|------|
|angular frontend|release 1.0|
|ETA service|release 1.0|
|ETA engine|release 1.0|
|AI engine|release 1.0|
|QV service|release 1.0|

As a goal, the routing engine will get the functionality to route not only by road (lorry) but also via rail (train). This is not possible with the current version of the ETA engine.

## Technology stack
* Java (ETA-Service)
* Angular (Frontend)
* Python (ETA-Engine, AI-Engine, QV-Engine)
* PostgreSQL
* OpenStreetMap Tileserver
* Nominatim (Geocoding engine)
* Valhalla (Routing engine)

## Setting up the project

  * You need to setup a data base (we use postgreSQL). If you are using another data base, changes in code might be necessary.
  * If not using the web based versions of [OpenStreetMap Tileserver](https://wiki.openstreetmap.org/wiki/Tile_servers), [Nominatim Geocoder](https://nominatim.org/) and [Valhalla Routing Engine](https://github.com/valhalla/valhalla), set up these services.
  * Setup an own gitlab-ci.yml, if you are using a gitlab-ci
  * Configure the config files (/config/ and /helm/<service>/values.yml) in the services according to your technical requirements. Pay attention to the URLs to the required services and set them to your needs.
	* You have to generate your own .env files in the config folder. The necessary information you must provide can be taken from the .env.template files.

## Running the project (locally)
  * You can start the complete service by `docker-compose -f docker-compose.yml up`
  * If you setup everything correctly, you can now access the AI based ETA service by the URL you setup in the angular-frontend-params.env
  
## Running single services locally

You can also start single services (with the dependencies in mind). You can either use an IDE of your choice or do it the manual way. On how to do that, please refer to the respective readme.md files in the services' directories. The following order should be considered to startup the complete service: 
	* Setup a postgreSQL locally (for the QV service)
	* Start the QV service (see readme in qv-service/)
	* Start the AI engine (see readme in ai-engine/)
	* Start the ETA engine (see readme in eta-engine/)
	* Start the ETA service (see readme in eta-service/)
	* Start the angular frontend (see readme in angular-frontend/)


### License

Licensed under the Open Logistics Foundation License 1.3. For details on the licensing terms, see the LICENSE file.

### Contact information
  * Product Owner
    * Alex Rotgang ([Mail](mailto:alex.rotgang@iml.fraunhofer.de))
    * Kai Hannemann ([Mail](mailto:kai.hannemann@iml.fraunhofer.de))
  * Development Team
    * Ebrahim Ehsanfar
    * Kai Hannemann
    * Sandra Jankowski
    * Thomas Kirks
    * Christoph Schlüter
    * Pajtim Thaqi
    * Johannes Waltmann
