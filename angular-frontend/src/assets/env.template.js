/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

(function(window){
    window["env"]=window["env"]||{}
    window['env']['production'] = "${production}";
    window["env"]["SOCKET_IO_HOST"] = "${SOCKET_IO_HOST}";
    window["env"]["SOCKET_IO_PORT"] = "${SOCKET_IO_PORT}";
    window["env"]["API_HOST"] = "${API_HOST}";
    window["env"]["API_PORT"] = "${API_PORT}";
    window["env"]["ETA_ENGINE_HOST"] = "${ETA_ENGINE_HOST}";
    window["env"]["ETA_ENGINE_PORT"] = "${ETA_ENGINE_PORT}";
    window["env"]["OSM_TILE_SERVER_HOST"] = "${OSM_TILE_SERVER_HOST}";
    window["env"]["OSM_TILE_SERVER_PORT"] = "${OSM_TILE_SERVER_PORT}";
}(this))
