/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

(function(window){
    window["env"]=window["env"]||{}
    window['env']['production'] = false;
    window["env"]["SOCKET_IO_HOST"] = "http://localhost";
    window["env"]["SOCKET_IO_PORT"] = "5001";
    window["env"]["API_HOST"] = "http://localhost";
    window["env"]["API_PORT"] = "8080";
}(this));
