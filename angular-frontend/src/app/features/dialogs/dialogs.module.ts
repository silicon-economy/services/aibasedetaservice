/**
* COPYRIGHT 2022 Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';


@NgModule({
  declarations: [ConfirmationDialogComponent, ErrorDialogComponent, LoadingDialogComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule
  ],
  providers: [],
  exports: [ ConfirmationDialogComponent, ErrorDialogComponent, LoadingDialogComponent],
})
export class DialogsModule { }
