/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ConfirmationDialogComponent, ConfirmDialogModel } from './confirmation-dialog.component';

/*
 *@author Sandra Jankowski, Thomas Kirks
 */

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let fixture: ComponentFixture<ConfirmationDialogComponent>;
  let fixtureModel: ComponentFixture<ConfirmDialogModel>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationDialogComponent ], imports: [HttpClientTestingModule, TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      }),],
      providers: [
        TranslateService, 
        { provide: MatDialogRef, useValue: {open: () => { },
        close: () => { }}},
        { provide: MAT_DIALOG_DATA, useValue: [] }
      ]      
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('dialog should be closed after onConfirm', () => {
    let spy = spyOn(component.dialogRef, 'close').and.callThrough();
    component.onConfirm();
    expect(spy).toHaveBeenCalled(); 
  });
  
  it('dialog should be closed after onDismissed', () => {
    let spy = spyOn(component.dialogRef, 'close').and.callThrough();
    component.onDismiss();
    expect(spy).toHaveBeenCalled();    
  });

  it('should create Model', () => {
    const dialogData  = new ConfirmDialogModel("title","message",1);
    expect(dialogData).toBeTruthy();
    expect(dialogData.title).toEqual("title");
    expect(dialogData.message).toEqual("message");
    expect(dialogData.transportID).toEqual(1);
  });
});
