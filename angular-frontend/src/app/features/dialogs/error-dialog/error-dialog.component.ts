/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { Component, EventEmitter, Input, Output } from '@angular/core';

/** 
 * @Author Sandra Jankowski
 */

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
/**
* Generalized error Dialog. 
*/
export class ErrorDialogComponent{
  @Input() set errorMessage(_message: string) {
    this.errorMessageText = _message;
  }
  @Input() set errorOrigMessage(_message: string) {
    this.errorOrigMessageText = _message;
  }
  @Output() closeEvent = new EventEmitter<boolean>();

  errorMessageText: string = "error";
  errorOrigMessageText: string = "error";

  closeError() {
    this.closeEvent.emit(true);
  }
}
