/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

/**
 * @Author Sandra Jankowski
 */

/**
* Get date without time from string "dd.mm.yyy, hh:mm:ss".
*/
export const getDate = (value: string): Date => {
    if (value.indexOf('.', 0) == 2) {
        return new Date(getDateString(value.substr(0, 10)));
    } else {
        return new Date(getDateString(value.substr(0, 9)));
    }
}

/**
* Get string needed to initialize new date without time from string "dd.mm.yyy, hh:mm:ss".
*/
export const getDateString = (curVal: string) : string => {
    let dotIndex = curVal.indexOf('.', 0);
    let day = curVal.substring(0, dotIndex);
    curVal = curVal.substr(dotIndex + 1, 7);
    dotIndex = curVal.indexOf('.', 0);
    let month = curVal.substring(0, dotIndex);
    let year = curVal.substr(dotIndex + 1, 4);

    if (Number.parseInt(month) < 10 && !month.startsWith('0')) month = '0' + month;
    if (Number.parseInt(day) < 10 && !day.startsWith('0')) day = '0' + day;
    return year + "-" + month + "-" + day;
}

/**
* Get date with time from string "dd.mm.yyy, hh:mm:ss".
*/
export const getDateWithTime = (curVal: string) : Date => {
    let dotIndex = curVal.indexOf('.', 0);
    let day = Number.parseInt(curVal.substring(0, dotIndex));
    curVal = curVal.substr(dotIndex + 1, 14);
    dotIndex = curVal.indexOf('.', 0);
    let month = Number.parseInt(curVal.substring(0, dotIndex));
    let year = Number.parseInt(curVal.substr(dotIndex + 1, 4));
    curVal = curVal.substr(curVal.indexOf(',', 0) + 2, 10);
    let hour = Number.parseInt(curVal.substr(0, 2));
    let min = Number.parseInt(curVal.substr(3, 5));
    return new Date(year, month, day, hour, min);
  }
