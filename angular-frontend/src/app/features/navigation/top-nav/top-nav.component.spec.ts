/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { DebugElement } from '@angular/core';
import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TopNavComponent } from './top-nav.component';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

let translations: any = {"show map": "Show Route"};

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}

describe('TopNavComponent', () => {
  let component: TopNavComponent;
  let fixture: ComponentFixture<TopNavComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TopNavComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useClass: FakeLoader
        }
      })],
      providers: [
        TranslateService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change lang correctly', () => {
    // arrange
    let debugElement: DebugElement = fixture.debugElement;
    let element: HTMLElement = debugElement.nativeElement.querySelector('#features-top-nav-link-eta-map');

    // act
    component.changeLang('en');

    fixture.detectChanges();

    // assert
    expect(component.actualLanguage).toEqual('en');
    expect(element.textContent).toEqual('Show Route');
  });

  it('should navigate to eta-map correctly with query params', inject(
    [Router],
    (router: Router) => {
      // arrange
      spyOn(router, "navigate").and.stub();

      // act
      component.openMap();

      // assert
      expect(router.navigate).toHaveBeenCalledWith(["/eta-map"], {
        queryParams: { createTransport: 'false' }
      });
    }
  ));
});
