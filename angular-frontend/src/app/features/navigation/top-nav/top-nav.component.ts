/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

/** 
 * @Author Thomas Kirks, Pajtim Thaqi
 */

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
/**
* Navigation for Top-Menu.
*/
export class TopNavComponent {
  actualLanguage: string = 'de';

  constructor(private translate: TranslateService, private router: Router) {
  }

  changeLang(language: string) {
    this.actualLanguage = language;
    this.translate.use(language);
  }

  openMap() {
    this.router.navigate(['/eta-map'], {
      queryParams: { createTransport: 'false' }
    })
  }
}
