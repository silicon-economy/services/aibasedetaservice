/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, of as observableOf } from 'rxjs';
import { map } from 'rxjs/operators';
import { getDateWithTime } from '../utils/DateTimeUtil';


/**
 * Data source for the NgxDataTable view with eta-specialized sorting. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class eDataSource extends DataSource<any> {
  data: any[];
  priorityMap: Map<string, string>;

  constructor(private paginator: MatPaginator, private _data: any[], private sort: MatSort, private pmap: Map<string, string>) {
    super();
    this.priorityMap = pmap;
    this.data = _data;
    this.data.sort((a, b) => {
      const isAsc = true;
      return this.compare(a.to, b.to, isAsc);
    });
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<any[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginators length
    this.paginator.length = this.data.length;

    return merge([...dataMutations]).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() { }

  /**
   * Paginate the data client-side.
   */
  private getPagedData(data: any[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data.
   */
  private getSortedData(data: any[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      if (isNaN(a[this.sort.active])) {
        return this.compare(a[this.sort.active], b[this.sort.active], isAsc);
      } else {
        return this.compare(+a[this.sort.active], +b[this.sort.active], isAsc);
      }
    });
  }


  /** Simple sort comparator for client-side sorting including the priority sorting for handeling translation. */
  compare(a, b, isAsc) {
    if (this.priorityMap.get(a) != undefined && this.priorityMap.get(b) != undefined) {
      a = this.priorityMap.get(a);
      b = this.priorityMap.get(b);
    }
    return compare(a, b, isAsc);
  }
}

/** Simple sort comparator for client-side sorting. */
function compare(a, b, isAsc) {
  if (a != undefined && b != undefined) {
    let date1 = getDateWithTime(a);
    let date2 = getDateWithTime(b);

    if (!isNaN(date1.getTime()) && !isNaN(date2.getTime())) {
      return ((date1.getTime() - date2.getTime()) < 0 ? -1 : 1) * (isAsc ? 1 : -1);
    }
  }
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
