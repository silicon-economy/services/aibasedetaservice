/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EtaService } from 'src/app/core/services/eta-service';
import { ConfirmationDialogComponent, ConfirmDialogModel } from '../dialogs/confirmation-dialog/confirmation-dialog.component';
import { getDate, getDateString, getDateWithTime } from '../utils/DateTimeUtil';
import { eDataSource } from './etaDataSource';

/** 
 * @Author Sandra Jankowski, Thomas Kirks
 **/

@Component({
    selector: 'app-data-table',
    templateUrl: './data-table.component.html',
    styleUrls: ['./data-table.component.scss']
})
/**
* Dynamic datatable with client-side sorting, pagination, filtering and translation. Names of header cells are generated from data-set.
*/
export class DataTableComponent implements AfterViewInit {
    /** Input Data */
    @Input() set data(_data: any[]) {
        this.origData = _data;
        this.setData(_data);
    }

    @Output() selected = new EventEmitter<number>();
    @Output() refreshNeeded = new EventEmitter<boolean>();
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild('inputSource') inputSource: ElementRef;
    @ViewChild('inputTarget') inputTarget: ElementRef;
    @ViewChild('inputTransportId') inputTransportId: ElementRef;
    @ViewChild('inputClient') inputClient: ElementRef;
    //@ViewChild('matSelectMode', { static: false }) matSelectMode: MatSelect;
    //modes: String[] = ["RAIL", "ROAD", "WATER", ""];
    @ViewChild('matSelectPrio') matSelectPrio: MatSelect;
    prios: string[] = ['LOW', 'MEDIUM', 'HIGH', 'VERY_HIGH', ''];
    @ViewChild('inputFrom') inputFrom: ElementRef;
    @ViewChild('inputTo') inputTo: ElementRef;
    @ViewChild('inputStartDate') inputStartDate: ElementRef;
    @ViewChild('inputDeleteAction') inputDeleteAction: ElementRef;
    @ViewChild('table') private myTable: MatTable<any>;

    eDataSource: eDataSource;
    origData: any[];
    displayedColumns: Array<string>;
    selectedRowIndex: number = -1;
    currentStartFilter: string = '';
    currentTargetFilter: string = '';
    currentTransportIdFilter: string = '';
    currentClientFilter: string = '';
    currentPriorityFilter: string = '';
    currentTransportMode: string = '';
    currentFromFilter: string = '';
    currentToFilter: string = '';
    currentSTartDateFilter: string = '';

    options: FormGroup;
    hideRequiredControl = new FormControl(false);
    floatLabelControl = new FormControl('auto');

    // Related to the "last updated" - text above the table
    counter: number = 0;
    refreshCounter: number = 0;
    lastUpdate: any = 0;

    IsHidden = true;
    deleteTransportResult: string = 'false';
    transportToDelete: string = '-1';

    private intervalId = this.setInterval()

    constructor(
        private translate: TranslateService,
        private router: Router,
        private fb: FormBuilder,
        private renderer: Renderer2,
        private etaService: EtaService,
        public dialogDeleteTransport: MatDialog
    ) {
        this.options = fb.group({
            hideRequired: this.hideRequiredControl,
            floatLabel: this.floatLabelControl,
        });
        //set interval to check if filter have changed and update table automatically on the fly
        setInterval(() => {
            this.counter = this.counter + 1;
            if (this.inputSource.nativeElement.value != this.currentStartFilter
                || this.inputTarget.nativeElement.value != this.currentTargetFilter
                || this.inputTransportId.nativeElement.value != this.currentTransportIdFilter
                || this.inputClient.nativeElement.value != this.currentClientFilter
                || this.inputFrom.nativeElement.value != this.currentFromFilter
                || this.inputTo.nativeElement.value != this.currentToFilter
                || this.inputStartDate.nativeElement.value != this.currentSTartDateFilter) {
                this.update();
            }
        }, 1000)
    }

    //set interval to update "last updated before"-field every 60 seconds
    private setInterval() {
        return setInterval(() => {
            this.refreshCounter = this.counter + 1;
            if (this.refreshCounter % 60 == 0)
                this.lastUpdate = this.refreshCounter / 60;
            this.addButtons();
        }, 1000);
    }

    onSelect() {
        this.IsHidden = !this.IsHidden;
    }

    deleteRowEntry(index) {
        if (index != "-1") {
            if (this.deleteTransportResult) {
                this.etaService.deleteTransportItemById(index)
                    .subscribe({
                        next: () => {
                            this.refresh();
                        },
                        error: (error) => {
                            console.log(error);
                        }
                    });
            }
        }
    }

    createTransport() {
        this.router.navigate(['/eta-map'], { queryParams: { createTransport: 'true' } })
    }

    clearFilters() {
        this.inputTransportId.nativeElement.value = '';
        this.inputClient.nativeElement.value = '';
        this.inputSource.nativeElement.value = '';
        this.inputTarget.nativeElement.value = '';
        this.matSelectPrio.value = '';
        this.inputFrom.nativeElement.value = '';
        this.inputTo.nativeElement.value = '';
        this.inputStartDate.nativeElement.value = '';

        this.currentStartFilter = '';
        this.currentTargetFilter = '';
        this.currentTransportIdFilter = '';
        this.currentClientFilter = '';
        this.currentPriorityFilter = '';
        this.currentTransportMode = '';
        this.currentFromFilter = '';
        this.currentToFilter = '';
        this.currentSTartDateFilter = '';
        this.update();
    }

    fillMap() {
        let map: Map<string, string> = new Map();
        map.set(this.translate.instant("VERY_HIGH"), "4");
        map.set(this.translate.instant("HIGH"), "3");
        map.set(this.translate.instant("MEDIUM"), "2");
        map.set(this.translate.instant("LOW"), "1");
        return map;
    }

    /**
    * Format and translate input-data, add internal id for identification after translation,
    * calculate if transport is in time and add actions column.
    */
    setData(_data: any[]) {
        var objKeys = Object.keys(_data);
        var newObjData: any[] = [];

        for (var objCounter = 0; objCounter < objKeys.length; objCounter++) {
            var objData = Object.values(_data)[objCounter];
            var keys: any[] = Object.keys(objData);
            var values: any[] = Object.values(objData);
            var newData = {};
            let failed: boolean = false;
            var eta: Date;
            var from: Date;
            var to: Date;

            for (var forCounter = 0; forCounter < keys.length; forCounter++) {
                var tempKey;
                var origKey;
                try {
                    origKey = keys[forCounter];
                    //needed for "in time"-calculation
                    if (origKey == "from") from = getDateWithTime(values[forCounter]);
                    if (origKey == "to") to = getDateWithTime(values[forCounter]);
                    if (origKey == "eta") eta = getDateWithTime(values[forCounter]);
                    this.translate.get(keys[forCounter]).subscribe(value => { tempKey = value });
                } catch (e) {
                    tempKey = keys[forCounter];
                }
                try {
                    this.translate.get(values[forCounter]).subscribe(value => {
                        newData[tempKey] = value;
                    });
                } catch (e) {
                    newData[tempKey] = values[forCounter];
                }
                failed = this.filter(origKey, tempKey, newData, failed);
            }

            newData["internalId"] = values[0]; // needed to identify items after translation
            newData["late"] = (eta < from || eta > to);

            if (!failed) {
                newObjData.push(newData);
            }
        }

        this.eDataSource = new eDataSource(this.paginator, newObjData, this.sort, this.fillMap());

        //if there is no item in the dataset, column names can not be calculated from the dataset, so old displayed columns stay
        if (!this.isOneFilterActive || newObjData.length > 0) {
            this.displayedColumns = Object.keys(newObjData[0]).filter(item => item != "internalId" && item != "late");
            this.displayedColumns.push(this.translate.instant("actions_cell"));
        }
    }

    private isOneFilterActive() {
        return (this.currentStartFilter != ''
            || this.currentTargetFilter != ''
            || this.currentTransportIdFilter != ''
            || this.currentTransportMode != ''
            || this.currentClientFilter != ''
            || this.currentPriorityFilter != ''
            || this.currentFromFilter != ''
            || this.currentToFilter != ''
            || this.currentSTartDateFilter != '');
    }

    public isSortingDisabled(columnText: string): boolean {
        return (columnText == this.translate.instant("actions_cell"));
    }

    /**
    * Add delete button for every single row, refering on this item when its clicked.
    */
    addButtons() {
        var header = document.getElementsByClassName('mat-header-cell mat-column-' + this.translate.instant("actions_cell"));
        if (header != null && header[0] != null) header[0].setAttribute("style", "max-width:40px");

        var actionButtonCells = document.getElementsByClassName('mat-cell mat-column-' + this.translate.instant("actions_cell"));

        for (let i = 0; i < actionButtonCells.length; i++) {
            actionButtonCells[i].setAttribute("style", "max-width:40px");
            if (actionButtonCells[i].querySelector('.actionButtonDelete') == null) {
                const imgButton = this.createImageButton();
                this.renderer.appendChild(actionButtonCells[i], imgButton);
            }
        }
    }

    createImageButton() {
        const imgButton = this.renderer.createElement('img');
        this.renderer.setAttribute(imgButton, "src", "/assets/img/icon_delete.png");
        this.renderer.setAttribute(imgButton, "width", "16px");
        this.renderer.setAttribute(imgButton, "class", "actionButtonDelete");
        imgButton.addEventListener(onclick, this.deleteRowEntry, false);
        imgButton.onmouseover = function () {
            this.style.filter = "brightness(70%)";
        }
        imgButton.onmouseout = function () {
            this.style.filter = "brightness(100%)";
        }
        return imgButton;
    }

    /**
    * Check if item is fitting the filters.
    */
    filter(origKey: any, tempKey: any, newData: any, failed: boolean): boolean {
        if (this.filterAndKeyCheck(this.currentStartFilter, origKey, 'source')) {
            let curVal: string = newData[tempKey];
            failed = !curVal.toLocaleLowerCase().includes(this.currentStartFilter.toLocaleLowerCase());
        }
        if (this.filterAndKeyCheck(this.currentTargetFilter, origKey, 'destination')) {
            let curVal: string = newData[tempKey];
            failed = !curVal.toLocaleLowerCase().includes(this.currentTargetFilter.toLocaleLowerCase());
        }
        if (this.filterAndKeyCheck(this.currentTransportIdFilter, origKey, 'transportId')) {
            let curVal: string = newData[tempKey].toString();
            failed = !curVal.includes(this.currentTransportIdFilter.toString());
        }
        if (this.filterAndKeyCheck(this.currentClientFilter, origKey, 'client')) {
            let curVal: string = newData[tempKey];
            failed = !curVal.toLocaleLowerCase().includes(this.currentClientFilter.toLocaleLowerCase());
        }
        if (this.filterAndKeyCheck(this.currentPriorityFilter, origKey, 'timelinessPriority')) {
            let curVal: string = newData[tempKey];
            failed = !curVal.toLocaleLowerCase().includes(this.currentPriorityFilter.toLocaleLowerCase());
        }
        if (this.filterAndKeyCheck(this.currentTransportMode, origKey, 'transportMode')) {
            let curVal: string = newData[tempKey];
            failed = !curVal.toLocaleLowerCase().includes(this.currentTransportMode.toLocaleLowerCase());
        }
        if (this.filterAndKeyCheck(this.currentFromFilter, origKey, 'from')) {
            let curVal: Date;
            curVal = getDate(newData[tempKey]);
            let checkVal: Date = new Date(getDateString(this.currentFromFilter));
            failed = (curVal < checkVal);
        }
        if (this.filterAndKeyCheck(this.currentToFilter, origKey, 'to')) {
            let curVal: Date;
            curVal = getDate(newData[tempKey]);
            let checkVal: Date = new Date(getDateString(this.currentToFilter));
            failed = (curVal > checkVal);
        }

        if (this.filterAndKeyCheck(this.currentSTartDateFilter, origKey, 'departure')) {
            let curVal: Date;
            curVal = getDate(newData[tempKey]);
            let checkVal: Date = new Date(getDateString(this.currentSTartDateFilter));
            failed = (curVal > checkVal || curVal < checkVal);
        }
        return failed;
    }

    filterAndKeyCheck(filter: string, givenOrigKey: any, desiredOrigKey: string): boolean {
        return (filter != '' && givenOrigKey == desiredOrigKey)
    }

    highlight(event, row, index) {
        if (event.srcElement) {
            if (event.srcElement.classList.contains('actionButtonDelete')) {
                this.transportToDelete = index;
                this.confirmDialog(index);
            }
            else {
                this.selected.emit(row.internalId);
                this.selectedRowIndex = row.internalId;
            }
        }
    }

    confirmDialog(transportID): void {
        const message = this.translate.instant('deleteTransportDescription');
        const title = this.translate.instant('deleteTransport');
        const dialogData = new ConfirmDialogModel(title, message, transportID);

        const dialogRef = this.dialogDeleteTransport.open(ConfirmationDialogComponent, {
            maxWidth: "400px",
            data: dialogData
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            this.deleteTransportResult = dialogResult;

            if (this.deleteTransportResult) {
                this.deleteRowEntry(this.transportToDelete);
            }
        });
    }

    refresh() {
        this.refreshNeeded.emit(true);
        this.lastUpdate = 0;
        this.refreshCounter = 0;
    }

    ngAfterViewInit() {
        this.matSelectPrio.valueChange.subscribe(value => {
            if (value != "")
                this.currentPriorityFilter = this.translate.instant(value);
            else this.currentPriorityFilter = '';
            this.update();
        });
    }

    /**
    * Get current filter values and update data based in them.
    */
    update() {
        this.currentStartFilter = this.inputSource.nativeElement.value;
        this.currentTransportIdFilter = this.inputTransportId.nativeElement.value;
        this.currentClientFilter = this.inputClient.nativeElement.value;
        this.currentTargetFilter = this.inputTarget.nativeElement.value;
        this.currentFromFilter = this.inputFrom.nativeElement.value;
        this.currentToFilter = this.inputTo.nativeElement.value;
        this.currentSTartDateFilter = this.inputStartDate.nativeElement.value;
        this.setData(this.origData);
    }
}
