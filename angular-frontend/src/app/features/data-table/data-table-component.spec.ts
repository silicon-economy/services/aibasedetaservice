/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DataTableComponent } from './data-table.component';

/*
 *@author Sandra Jankowski
 */

const TRANSPORT_ITEMS: any[] = [
    {
        transportId: "1231",
        client: "Alpha",
        timelinessPriority: "LOW",
        source: "München",
        transportMode: "ROAD",
        destination: "Dortmund",
        departure: "7.8.2020, 09:00:00",

        from: "18.08.2020, 09:00:00",
        to: "18.08.2020, 20:00:00"
        ,
        eta: "18.08.2020, 09:00:00"
    },
    {
        transportId: "3234",
        client: "Butter",
        timelinessPriority: "VERY_HIGH",
        source: "Prien",
        transportMode: "ROAD",
        destination: "Frankfurt",
        departure: "17.08.2020, 09:00:00",
        from: "18.08.2020, 09:00:00",
        to: "18.08.2020, 20:00:00"
        ,
        eta: "18.08.2020, 09:00:00"
    }
];

export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

describe('DataTable', () => {
    let component: DataTableComponent;
    let fixture: ComponentFixture<DataTableComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [DataTableComponent],
            imports: [
                RouterTestingModule,
                CommonModule,
                MatDatepickerModule,
                MatDialogModule,
                MatNativeDateModule,
                MatPaginatorModule,
                MatSortModule,
                MatOptionModule,
                MatSelectModule,
                MatFormFieldModule,
                MatTableModule,
                HttpClientTestingModule,
                RouterTestingModule,
                BrowserAnimationsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [HttpClient]
                    }
                }),
                FormsModule,
                ReactiveFormsModule,
                MatInputModule
            ],
            providers: [
                TranslateService
            ]
        })
            .compileComponents();
    }));

    let timer;
    beforeEach(() => {
        fixture = TestBed.createComponent(DataTableComponent);
        component = fixture.componentInstance;
        component.data = TRANSPORT_ITEMS;
        fixture.autoDetectChanges();
        timer = jasmine.createSpy("timerCallback");
        jasmine.clock().install();
    });

    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it("timer should be called", function () {
        setTimeout(function () {
            timer();
        }, 100);
        expect(timer).not.toHaveBeenCalled();
        jasmine.clock().tick(101);
        expect(timer).toHaveBeenCalled();
    });

    it('should create data-table', () => {
        expect(component).toBeTruthy();
    });

    it('should select', () => {
        let isHidden = component.IsHidden;
        component.onSelect();
        expect(component.IsHidden).not.toEqual(isHidden);
    });

    it('should navigate to map clicked', inject(
        [Router],
        (router: Router) => {
            spyOn(router, "navigate").and.stub();
            component.createTransport();
            expect(router.navigate).toHaveBeenCalledWith(["/eta-map"], {
                queryParams: { createTransport: 'true' }
            });
        }
    ));

    it('should clear filter', () => {
        component.clearFilters();
        expect(component.inputTransportId.nativeElement.value).toEqual('');
        expect(component.inputClient.nativeElement.value).toEqual('');
        expect(component.inputSource.nativeElement.value).toEqual('');
        expect(component.inputTarget.nativeElement.value).toEqual('');
        expect(component.matSelectPrio.value).toEqual('');
        expect(component.inputFrom.nativeElement.value).toEqual('');
        expect(component.inputTo.nativeElement.value).toEqual('');
        expect(component.inputStartDate.nativeElement.value).toEqual('');

        expect(component.currentStartFilter).toEqual('');
        expect(component.currentTargetFilter).toEqual('');
        expect(component.currentTransportIdFilter).toEqual('');
        expect(component.currentClientFilter).toEqual('');
        expect(component.currentPriorityFilter).toEqual('');
        expect(component.currentTransportMode).toEqual('');
        expect(component.currentFromFilter).toEqual('');
        expect(component.currentToFilter).toEqual('');
        expect(component.currentSTartDateFilter).toEqual('');
    });

    it('should refresh', () => {
        spyOn(component.refreshNeeded, 'emit');
        component.refresh();
        expect(component.refreshNeeded.emit).toHaveBeenCalled();
        expect(component.refreshNeeded.emit).toHaveBeenCalledWith(true);
        expect(component.lastUpdate).toEqual(0);
        expect(component.refreshCounter).toEqual(0);
    });

    it('should update', () => {
        component.update();
        expect(component.currentStartFilter).toEqual(component.inputSource.nativeElement.value);
        expect(component.currentTransportIdFilter).toEqual(component.inputTransportId.nativeElement.value);
        expect(component.currentClientFilter).toEqual(component.inputClient.nativeElement.value);
        expect(component.currentTargetFilter).toEqual(component.inputTarget.nativeElement.value);
        expect(component.currentFromFilter).toEqual(component.inputFrom.nativeElement.value);
        expect(component.currentToFilter).toEqual(component.inputTo.nativeElement.value);
        expect(component.currentSTartDateFilter).toEqual(component.inputStartDate.nativeElement.value);
    });

    it('filter source', () => {
        let failed = false;
        component.currentStartFilter = "M";
        failed = component.filter("source", "source", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentStartFilter = "Dortmu";
        failed = component.filter("source", "source", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('filter destination', () => {
        let failed = false;
        component.currentTargetFilter = "Do";
        failed = component.filter("destination", "destination", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentTargetFilter = "Mü";
        failed = component.filter("destination", "destination", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });
  
    it('filter transportId', () => {
        let failed = false;
        component.currentTransportIdFilter = "12";
        failed = component.filter("transportId", "transportId", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentTransportIdFilter = "98";
        failed = component.filter("transportId", "transportId", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('filter client', () => {
        let failed = false;
        component.currentClientFilter = "al";
        failed = component.filter("client", "client", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentClientFilter = "bu";
        failed = component.filter("client", "client", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('filter timelinessPriority', () => {
        let failed = false;
        component.currentPriorityFilter = "low";
        failed = component.filter("timelinessPriority", "timelinessPriority", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentPriorityFilter = "high";
        failed = component.filter("timelinessPriority", "timelinessPriority", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('filter transportMode', () => {
        let failed = false;
        component.currentTransportMode = "road";
        failed = component.filter("transportMode", "transportMode", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentTransportMode = "rail";
        failed = component.filter("transportMode", "transportMode", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('filter from', () => {
        let failed = false;
        component.currentFromFilter = "18.08.2020, 09:00:00";
        failed = component.filter("from", "from", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentFromFilter = "19.08.2020, 09:00:00";
        failed = component.filter("from", "from", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('filter to', () => {
        let failed = false;
        component.currentToFilter = "18.08.2020, 09:00:00";
        failed = component.filter("to", "to", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentToFilter = "17.08.2020, 09:00:00";
        failed = component.filter("to", "to", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });
    //departure  true and false
    it('filter departure', () => {
        let failed = false;
        component.currentSTartDateFilter = "7.08.2020, 09:00:00";
        failed = component.filter("departure", "departure", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(false);
        component.currentSTartDateFilter = "8.08.2020, 09:00:00";
        failed = component.filter("departure", "departure", TRANSPORT_ITEMS[0], failed);
        expect(failed).toEqual(true);
    });

    it('should call highlight and select row', () => {
        let index = 5;
        component.highlight({srcElement: {classList: { contains: () => false}}}, {internalId: index}, null);
        expect(component.selectedRowIndex).toEqual(index);
    });

    it ('should check, if one filter is active', () => {
        expect(component['isOneFilterActive']()).toBeFalse();
        component.currentClientFilter = "foo"
        expect(component['isOneFilterActive']()).toBeTrue();
    });

    it ('should delete row entry', () => {
        component.deleteRowEntry("1");
        // no error passes test.
    });

    it ('should show confirm dialog', () => {
        component.confirmDialog(2);
        // no error passes test.
    });

    it ('should create an image button', () => {
        component.createImageButton();
        // no error passes test.
    });
});
