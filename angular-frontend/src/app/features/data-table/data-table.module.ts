/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatOptionModule, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { I18nMatPaginatorIntl } from './I18nMatPaginatorIntl';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableComponent } from './data-table.component';

@NgModule({
  declarations: [DataTableComponent],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule, 
    MatSortModule, 
    MatOptionModule, 
    MatSelectModule , 
    MatFormFieldModule,
    MatTableModule,
    TranslateModule,
    FormsModule, 
    ReactiveFormsModule,
    MatInputModule
  ],
  exports:[
    DataTableComponent
  ],
  providers: [
    {
      provide: MatPaginatorIntl, deps: [TranslateService],
      useFactory: (translateService: TranslateService) => new I18nMatPaginatorIntl(translateService).getPaginatorIntl()
    }
  ]
})
export class DataTableModule { }
