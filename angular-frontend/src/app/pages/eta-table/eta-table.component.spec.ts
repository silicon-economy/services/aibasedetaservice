/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Component, Input } from '@angular/core';
import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable, of } from 'rxjs';
import { CoreModule } from 'src/app/core/core.module';
import { TransportItem } from 'src/app/core/models/transport-item';
import { EtaService } from 'src/app/core/services/eta-service';
import { EtaMapComponent } from '../eta-map/eta-map.component';
import { EtaMapModule } from '../eta-map/eta-map.module';
import { EtaTableComponent } from './eta-table.component';

/*
 *@author Sandra Jankowski, Pajtim Thaqi
 */

const TRANSPORT_ITEMS: TransportItem[] = [
  {
    transportId: "1231",
    client: "Alpha",
    timelinessPriority: "LOW",
    source: "München",
    transportMode: "ROAD",
    destination: "Dortmund",
    departure: "2020-08-17T10:00:00",
    timeCorridor: {
      from: "2020-08-18T09:00:00",
      to: "2020-08-18T12:00:00"
    },
    eta: "2020-08-18T09:00:00"
  }
];

const TABLE_DATA: any =
{
  transportId: '1231',
  client: 'Alpha',
  timelinessPriority: 'LOW',
  source: 'München',
  transportMode: 'ROAD',
  destination: 'Dortmund',
  departure: 'Aug 17, 2020, 10:00:00 AM',
  from: 'Aug 18, 2020, 9:00:00 AM',
  to: 'Aug 18, 2020, 12:00:00 PM',
  eta: 'Aug 18, 2020, 9:00:00 AM'
};

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@Component({ selector: 'app-data-table', template: '' })
class FakeDataTableComponent {
  @Input() data: any;
}

class EtaServiceFake {
  getTransportItems(): Observable<TransportItem[]> {
    return of(TRANSPORT_ITEMS);
  }

  getTransportItemById(transportId: string): Observable<TransportItem> {
    return of(TRANSPORT_ITEMS[0]);
  }
}

describe('EtaTableComponent', () => {
  let component: EtaTableComponent;
  let fixture: ComponentFixture<EtaTableComponent>;
  let httpClient: HttpTestingController;
  let translate: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EtaTableComponent, FakeDataTableComponent],
      providers: [{
        provide: EtaService,
        useClass: EtaServiceFake
      }, TranslateService],
      imports: [
        EtaMapModule,
        RouterTestingModule.withRoutes([
          { path: 'eta-map', component: EtaMapComponent }
        ]), CoreModule, HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [HttpClient]
          }
        })]
    }).compileComponents();

    httpClient = TestBed.get(HttpTestingController);
    translate = TestBed.get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaTableComponent);
    component = fixture.componentInstance;
    component.sort = new MatSort();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create Pipe', () => {
    translate.setDefaultLang("de");
    expect(component.pipe).toBeTruthy();
    let spy = spyOn(translate, 'setDefaultLang').and.callThrough();
    translate.setDefaultLang("en");
    expect(spy).toHaveBeenCalled();
    expect(component.pipe).toBeTruthy();
  });

  it('should highlight', () => {
    component.onHighlight(1231);
    expect(component.selected).toEqual(1231);
  });

  it('should refrsh', () => {
    component.refresh();
    expect(component.childData).toBeTruthy();
  });

  it('should setChildData', () => {
    component.setChildData();
    expect(component.childData).toBeTruthy();
    expect(component.childData[0].transportId).toEqual('1231');
    expect(component.childData[0].client).toEqual('Alpha');
    expect(component.childData[0].timelinessPriority).toEqual('LOW');
    expect(component.childData[0].source).toEqual('München');
    expect(component.childData[0].transportMode).toEqual('ROAD');
    expect(component.childData[0].destination).toEqual('Dortmund');
    expect(component.childData[0].departure).toEqual('Aug 17, 2020, 10:00:00 AM');
    expect(component.childData[0].from).toEqual('Aug 18, 2020, 9:00:00 AM');
    expect(component.childData[0].to).toEqual('Aug 18, 2020, 12:00:00 PM');
    expect(component.childData[0].eta).toEqual('Aug 18, 2020, 9:00:00 AM');
  });

  it('should createTransport', inject(
    [Router],
    (router: Router) => {
      spyOn(router, "navigate").and.stub();
      component.createTransport();
      expect(router.navigate).toHaveBeenCalledWith(["/eta-map"], {
        queryParams: { createTransport: 'true' }
      });
    }
  ));

  it('should navigate to map clicked', inject(
    [Router],
    (router: Router) => {
      spyOn(router, "navigate").and.stub();
      component.onNavigateToMapClicked(TRANSPORT_ITEMS[0]);
      expect(router.navigate).toHaveBeenCalledWith(["/eta-map"], {
        queryParams: { transportId: '1231' },
        queryParamsHandling: 'merge'
      });
    }
  ));
});
