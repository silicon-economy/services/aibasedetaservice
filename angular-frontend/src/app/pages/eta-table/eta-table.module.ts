/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { EtaTableRoutingModule } from './eta-table-routing.module';
import { EtaTableComponent } from './eta-table.component';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmationDialogComponent } from 'src/app/features/dialogs/confirmation-dialog/confirmation-dialog.component';
import { DataTableModule } from 'src/app/features/data-table/data-table.module';
import { DialogsModule } from 'src/app/features/dialogs/dialogs.module';

@NgModule({
    declarations: [EtaTableComponent],
    imports: [
        CommonModule,
        EtaTableRoutingModule,
        MatTableModule,
        DataTableModule,
        TranslateModule,
        DialogsModule
    ]
})

export class EtaTableModule { }
