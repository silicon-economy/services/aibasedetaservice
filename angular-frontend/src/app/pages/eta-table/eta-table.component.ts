/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { Params, Router } from '@angular/router';
import { TransportItem } from 'src/app/core/models/transport-item';
import { EtaService } from 'src/app/core/services/eta-service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';

/** 
 * @Author Sandra Jankowski, Thomas Kirks
 */

@Component({
  selector: 'app-eta-table',
  templateUrl: './eta-table.component.html',
  styleUrls: ['./eta-table.component.scss']
})

/**
* Overlying structure for data-table component, preparing input data for 
* data-table (e.g. make two elements of timewindow and pipe dates).
**/
export class EtaTableComponent implements OnInit {
  childData: any[] = [{}];
  selected: number = -1;
  pipe: DatePipe = new DatePipe('en-DE');

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private etaService: EtaService, private router: Router, private translate: TranslateService, private http: HttpClient) {
    if (this.translate.getDefaultLang() != undefined)
      this.pipe = new DatePipe(this.translate.getDefaultLang());

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.setChildData();
    });
  }

  onHighlight(selected: number) {
    this.selected = selected;
    this.etaService
      .getTransportItemById(this.selected.toString())
      .subscribe(data => this.onNavigateToMapClicked(data));
  }

  refresh() {
    this.setChildData();
  }

  createTransport() {
    this.router.navigate(['/eta-map'], {
      queryParams: { createTransport: 'true' }
    })
  }

  ngOnInit() {
    this.setChildData();
  }

  setChildData() {
    this.etaService.getTransportItems().subscribe(_data => {
      this.setChildDataFromItem(_data);
    })
  }

  private setChildDataFromItem(_data: TransportItem[]) {
    var objKeys = Object.keys(_data);
    var newObjData: any[] = [];

    for (var objCounter = 0; objCounter < objKeys.length; objCounter++) {
      this.setObject(_data, objCounter, newObjData, objKeys);
    }
    this.childData = newObjData;
  }

  private setObject(_data: TransportItem[], objCounter: number, newObjData: any[], objKeys: string[]) {
    var objData = Object.values(_data)[objCounter];
    var keys: any[] = Object.keys(objData);
    var values: any[] = Object.values(objData);
    var newData: any[] = [];

    for (var forCounter = 0; forCounter < keys.length; forCounter++) {
      this.checkForSpecialValuesAndSetData(keys, forCounter, values, newData);
    }
    newObjData[objKeys[objCounter]] = newData;
  }

  /**
  * Split timeCorridor object in the two attributes "to" and "from", 
  * transform dates in easily readable formats.
  **/
  private checkForSpecialValuesAndSetData(keys: any[], forCounter: any, values: any[], newData: any[]) {
    if (keys[forCounter] == "timeCorridor") {
      var x = values[forCounter];
      var tcKeys: any[] = Object.keys(x);
      var tcValues: any[] = Object.values(x);

      let date1: any = tcValues[0];
      newData[tcKeys[0]] = this.pipe.transform(new Date(date1), 'medium');
      let date2: any = tcValues[1];
      newData[tcKeys[1]] = this.pipe.transform(new Date(date2), 'medium');
    } else {
      if (keys[forCounter] == 'departure' || keys[forCounter] == 'eta') {
        newData[keys[forCounter]] = this.pipe.transform(new Date(values[forCounter]), 'medium');
      } else {
        newData[keys[forCounter]] = values[forCounter];
      }
    }
  }

  onNavigateToMapClicked(item: TransportItem) {
    this.router.navigate(['/eta-map'], {
      queryParams: this.setQueryParams(item),
      queryParamsHandling: 'merge'
    })
  }

  private setQueryParams(item: TransportItem): Params {
    return {
      transportId: item.transportId
    };
  }
}
