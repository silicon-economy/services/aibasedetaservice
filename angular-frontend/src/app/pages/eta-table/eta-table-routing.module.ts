/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EtaTableComponent } from './eta-table.component';

const routes: Routes = [{ path: '', component: EtaTableComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtaTableRoutingModule {}
