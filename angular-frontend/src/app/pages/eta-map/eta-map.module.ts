/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EtaMapComponent } from './eta-map.component';
import { EtaMapRoutingModule } from './eta-map-routing.module';
import { EtaInfoComponent } from './eta-info/eta-info.component';
import { EtaCreateTransportComponent } from './eta-create-transport/eta-create-transport.component';
import { TranslateModule } from '@ngx-translate/core';
import { EtaNavigationComponent } from './eta-navigation/eta-navigation.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { DialogsModule } from 'src/app/features/dialogs/dialogs.module';

@NgModule({
  declarations: [EtaMapComponent, EtaInfoComponent, EtaNavigationComponent, EtaCreateTransportComponent],
  imports: [
    CommonModule,
    EtaMapRoutingModule,
    TranslateModule,
    RouterModule,
    HttpClientModule,
    DialogsModule
  ]
})

export class EtaMapModule { }
