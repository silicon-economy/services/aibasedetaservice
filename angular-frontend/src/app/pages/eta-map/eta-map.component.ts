/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as L from 'leaflet';
import { antPath } from 'leaflet-ant-path';
import { EtaService } from 'src/app/core/services/eta-service';
import { TransportModeRoutesColorMap } from 'src/app/core/utils/transport-mode-routes-color-map';
import { TransportMode } from 'src/app/core/models/transport-mode';
import { TranslateService } from '@ngx-translate/core';
import { TransportItem } from 'src/app/core/models/transport-item';
import { HttpClient } from '@angular/common/http';
import { EtaEngineService } from 'src/app/core/services/eta-engine-service';
import { environment } from 'src/environments/environment';

/**
 * @Author Sandra Jankowski, Thomas Kirks
 */

@Component({
  selector: 'app-eta-map',
  templateUrl: './eta-map.component.html',
  styleUrls: ['./eta-map.component.scss']
})

/**
* OSM-map, able to the show the route of the selected transport including the transport modes, the legend,
* the source and destination of the transport with icons,
* and the eta-info component or eta-create-transport component
*/
export class EtaMapComponent implements OnInit, AfterViewInit {

  transportInfo: TransportItem = undefined;

  map: any;

  start: any;
  target: any;
  public show: boolean = false;
  showLoading = false;
  showError = false;
  errorMessage: string = 'error';
  origMessage: string = 'error';
  lastZoomLevel: number;

  constructor(
    // Angular - Used to get the values from the URL
    private route: ActivatedRoute,
    private etaService: EtaService,
    private etaEngingService: EtaEngineService,
    private router: Router,
    private http: HttpClient,
    private translate: TranslateService) {}

  ngAfterViewInit(): void {

    // initiate a map with a given Position and a predefined zoom level
    this.map = L.map('map', {
      center: [51.494648, 7.408408],
      zoom: 15
    });

    // the tiles from Open street map
    const osmUrl = `${environment.osmTileServerHost}:${environment.osmTileServerPort}/tile/{z}/{x}/{y}.png`;

    const tiles = L.tileLayer(osmUrl, {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    // add the map to the tiles layer
    tiles.addTo(this.map);

    //add CT terminals
    this.fetchCTTerminals();
  }

  /**
  * Get color for the current transport mode and render segments of route as ant path.
  */
  private setPath(ll: any, type: TransportMode) {
    var color: string = TransportModeRoutesColorMap.getColorAsRGB(type, false);
    var pathToDrive = antPath(ll, { "delay": 5000, "weight": 5, "color": color, "paused": false, "reverse": false }
    ).addTo(this.map);
    this.map.addLayer(pathToDrive);
  }

  /**
  * Set marker for source and destination.
  */
  private setMarker() {
    var startIcon = L.icon({
      iconUrl: 'assets/img/startIcon.png',
      iconSize: [50, 50], // size of the icon
      iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
      popupAnchor: [25, -10] // point from which the popup should open relative to the iconAnchor
    });

    L.marker([this.start.lat, this.start.lng], { icon: startIcon }).addTo(this.map);

    var targetIcon = L.icon({
      iconUrl: 'assets/img/targetIcon.png',
      iconSize: [30, 30], // size of the icon
      iconAnchor: [15, 30], // point of the icon which will correspond to marker's location
      popupAnchor: [25, -10] // point from which the popup should open relative to the iconAnchor
    });
    L.marker([this.target.lat, this.target.lng], { icon: targetIcon }).addTo(this.map);
  }

  ngOnInit() {
    var transportId;
    var createTransport;
    this.route.queryParams.subscribe((queryParams: Params) => {
      transportId = queryParams['transportId'];
      createTransport = queryParams['createTransport'];
    })
    this.updateInitialView(createTransport,transportId);
    
  }

  updateInitialView(createTransport: any, transportId: any): void {
          // select eta-info or create-transport-component
          if (createTransport == 'true') {
            this.show = true;
          }
          else {
            this.show = false;
          }
    
          // fetch the information from the query
          if (transportId) {
            this.fetchInformation(transportId);
          }
  }
  /**
  * Fetch information from the server and update path, marker and eta-info.
  */
  fetchInformation(transportId: string): void {
    this.showLoading = true;
    this.etaService.getRouteById(transportId).subscribe(
      valRoute => {
        this.start = valRoute.source;
        this.target = valRoute.destination;
        this.setMarker();
        valRoute.routeSegmentsByTransportMode.forEach(element => {
          this.setPath(element.routeSegments, element.transportMode);
        });
        this.map.fitBounds(antPath([[this.start.lat, this.start.lng], [this.target.lat, this.target.lng]]).getBounds());

        this.etaService.getTransportItemById(transportId).subscribe(
          val => {
            this.transportInfo = val;
            this.show = false;
            this.showLoading = false;
          },
          error => {
            this.showErrorDialog("While loading transport item", error.message);
          });
      },
      error => {
        this.showErrorDialog("While loading route", error.message);
      }
    );
  }

 /**
  * Load CT terminals from eta engine, set listeners to map zoom and render markers for terminals
  */
  fetchCTTerminals(): void {
    this.etaEngingService.getCTTerminals().subscribe(
      terminals => {
        terminals.forEach(element => {
          var icon;
          var type: string;
          if (element.ct_type == "RAIL") type = 'KV_Schiene.jpg';
          else if (element.ct_type == "TRIMODAL") type = 'KV_Trimodal.jpg';
          else type = 'KV_Wasser.jpg';

          icon = L.icon({
            iconUrl: 'assets/img/' + type,
            iconSize: [30, 30], // size of the icon
            iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
            popupAnchor: [-10, -20] // point from which the popup should open relative to the iconAnchor
          });

          const iconMarker = L.marker([element.lat, element.lng], { icon: icon });
          iconMarker.bindPopup(element.name);
          iconMarker.on('mouseover', function (e) {
            this.openPopup();
          });
          iconMarker.on('mouseout', function (e) {
            this.closePopup();
          });

          var point = L.icon({
            iconUrl: 'assets/img/currentIcon.png',
            iconSize: [10, 15], // size of the icon
            iconAnchor: [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor: [-5, -10] // point from which the popup should open relative to the iconAnchor
          });
          const pointMarker = L.marker([element.lat, element.lng], { icon: point });
          pointMarker.bindPopup(element.name);
          pointMarker.on('mouseover', function (e) {
            this.openPopup();
          });
          pointMarker.on('mouseout', function (e) {
            this.closePopup();
          });

          if(this.map.getZoom() >= 10) iconMarker.addTo(this.map);
          else pointMarker.addTo(this.map);
          this.lastZoomLevel = this.map.getZoom();

          this.map.on("zoomend", function (e) {
            if(this.getZoom() >= 10) {
              this.removeLayer(pointMarker);
              iconMarker.addTo(this);
            }
            else {
              this.removeLayer(iconMarker);
              pointMarker.addTo(this);
            }
          });
        });
      },
      error => {
        this.showErrorDialog("While loading CT Terminals", error.message);
      }
    )
  }

  /**
  * Create transport and update map-view on the new item when item in eta-create-transport component is triggered.
  */
  onItemTriggered(item: TransportItem) {
    this.showLoading = true;
    let _id: string;
    this.etaService.createTransportItem(item).subscribe(
      id => {
        _id = id.transportId;
        this.fetchInformation(_id);
        this.router.navigate([], {});
      },
      error => {
        this.showErrorDialog("while create transport item", error.message);
      });
  }

  async showErrorDialog(message: string, origMessage: string) {
    if (message != null) this.errorMessage = this.translate.instant(message);
    if (origMessage != null) this.origMessage = this.translate.instant(origMessage);
    this.showLoading = false;
    this.showError = true;
  }

  /**
  * When error dialog is closed navigate back to eta-table.
  */
  closeError() {
    this.showError = false;
    this.router.navigate(['/eta-table']);
  }
}

