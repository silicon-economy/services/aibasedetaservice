/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { EtaInfoComponent } from './eta-info.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

/*
 *@author Sandra Jankowski, Thomas Kirks
 */

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

describe('EtaInfoComponent', () => {
  let component: EtaInfoComponent;
  let fixture: ComponentFixture<EtaInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EtaInfoComponent]
      , imports: [HttpClientTestingModule, TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      }),],
      providers: [TranslateService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaInfoComponent);
    component = fixture.componentInstance;
    component.info = {
      transportId: "1231",
      client: "Alpha",
      timelinessPriority: "LOW",
      source: "München",
      transportMode: "ROAD",
      destination: "Dortmund",
      departure: "2020-08-17T10:00:00",
      timeCorridor: {
          from: "2020-08-18T09:00:00",
          to: "2020-08-18T12:00:00"
      },
      eta:"2020-08-18T09:00:00"
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
