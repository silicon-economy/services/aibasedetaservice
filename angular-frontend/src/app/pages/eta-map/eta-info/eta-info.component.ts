/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { Component, Input } from '@angular/core';
import { TransportItem } from 'src/app/core/models/transport-item';
import { DatePipe } from '@angular/common';

/** 
 * @Author Sandra Jankowski, Thomas Kirks
 */

@Component({
  selector: 'app-eta-info',
  templateUrl: './eta-info.component.html',
  styleUrls: ['./eta-info.component.scss']
})

/**
* Informationfield refering the selected transport. 
*/
export class EtaInfoComponent {
  @Input() public info: TransportItem;
  datePipe = new DatePipe('en-DE')

  transform(value: string) {
    value = this.datePipe.transform(value, 'dd.MM.yyyy - HH:mm');
    return value;
  }
}
