/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Input } from '@angular/core';
import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable, of, throwError} from 'rxjs';
import { CTTerminal } from 'src/app/core/models/ct-terminal';
import { TransportItem } from 'src/app/core/models/transport-item';
import { TransportRoute } from 'src/app/core/models/transport-route';
import { EtaEngineService } from 'src/app/core/services/eta-engine-service';
import { EtaService } from 'src/app/core/services/eta-service';
import { DialogsModule } from 'src/app/features/dialogs/dialogs.module';
import { EtaCreateTransportComponent } from './eta-create-transport/eta-create-transport.component';
import { EtaMapComponent } from './eta-map.component';
import { EtaNavigationComponent } from './eta-navigation/eta-navigation.component';


export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@Component({ selector: 'app-eta-info', template: '' })
class FakeEtaInfoComponent {
  @Input() info: TransportItem

}

class EtaServiceFake {
  createTransportItem(item: TransportItem): Observable<TransportItem> {
    return of(transportItem);
  }

  getTransportItemById(transportId: string): Observable<TransportItem> {
    return of(transportItem);
  }

  getRouteById(transportId: string): Observable<TransportRoute> {
    return of(transportRoute);
  }
}

class EtaEngineServiceFake {
  getCTTerminals(): Observable<CTTerminal[]> {
    return of(terminal);
  }
}

let terminal: CTTerminal[] = [{
  name: "1231",
  node: 1,
  lat: "51.123",
  lng: "52.247",
  ct_type: "RAIL"
},
{
  name: "1231",
  node: 1,
  lat: "51.123",
  lng: "52.247",
  ct_type: "TRIMODAL"
},
{
  name: "1231",
  node: 1,
  lat: "51.123",
  lng: "52.247",
  ct_type: "WATER"
}];

let transportItem: TransportItem = {
  transportId: "1231",
  client: "Alpha",
  timelinessPriority: "LOW",
  source: "München",
  transportMode: "ROAD",
  destination: "Dortmund",
  departure: "2020-08-17T10:00:00",
  timeCorridor: {
    from: "2020-08-18T09:00:00",
    to: "2020-08-18T12:00:00"
  },
  eta: "2020-08-18T09:00:00"
};

let transportRoute: TransportRoute = {
  source: {
    lat: "",
    lng: ""
  },
  destination: {
    lat: "",
    lng: ""
  },
  routeSegmentsByTransportMode: [{
    transportMode: 'ROAD',
    routeSegments: ""
  }]
}

class ActivatedRouteMock {
  queryParams = new Observable(observer => {
    const urlParams = {
      target: 'Frankfurt',
      from: '2020-08-18T09:00:00',
      to: '2020-08-18T12:01:00',
      latestEta: '2020-08-17T18:00:00',
      transportMode: 'RAIL',
      start: 'Dortmund',
      client: 'butter'
    }
    observer.next(urlParams);
    observer.complete();
  });
}

describe('EtaMapComponent', () => {
  let component: EtaMapComponent;
  let fixture: ComponentFixture<EtaMapComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        EtaMapComponent, 
        EtaCreateTransportComponent, 
        FakeEtaInfoComponent, 
        EtaNavigationComponent
      ],
      imports: [
        HttpClientTestingModule, 
        RouterTestingModule.withRoutes([
          { path: 'eta-table', component: EtaMapComponent }
        ]), 
        DialogsModule, 
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [
        {
          provide: EtaEngineService,
          useClass: EtaEngineServiceFake
        },
        {
          provide: EtaService,
          useClass: EtaServiceFake
        },
        {
          provide: ActivatedRoute,
          useClass: ActivatedRouteMock,
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test zoom under 10', () => {
    component.map.zoom = 5;
    component.fetchCTTerminals();
  });

  it('should test with parameters', () => {
    const fetchSpy = spyOn(component, 'fetchInformation').and.callThrough();
    component.updateInitialView('true', 1);
    expect(fetchSpy).toHaveBeenCalled();
    
  });


  it('should show error dialog', () => {
    component.showErrorDialog("error", "orig");
    expect(component.showLoading).toEqual(false);
    expect(component.showError).toEqual(true);
  });

  it('should close error dialog', inject(
    [Router],
    (router: Router) => {
      spyOn(router, "navigate").and.stub();
      component.closeError();
      expect(component.showError).toEqual(false);
      expect(router.navigate).toHaveBeenCalledWith(["/eta-table"])
    }
  ));

  it('should work on item triggered', inject(
    [EtaService],
    (s: EtaService) => {
      const serviceSpy = spyOn(s, 'createTransportItem').and.callThrough();;
      component.onItemTriggered(transportItem);
      expect(serviceSpy).toHaveBeenCalled();
    }));

  it('should work on item triggered', inject(
    [EtaService],
    (s: EtaService) => {
      const createSpy = spyOn(s, 'createTransportItem').and.callThrough();
      const fetchSpy = spyOn(s, 'getRouteById').and.callThrough();
      component.onItemTriggered(transportItem);
      expect(createSpy).toHaveBeenCalled();
      expect(fetchSpy).toHaveBeenCalled();
    }));

  it('should fetch CT terminal', inject(
    [EtaEngineService],
    (s: EtaEngineService) => {
      const fetchSpy = spyOn(s, 'getCTTerminals').and.callThrough();
      component.fetchCTTerminals();
      expect(fetchSpy).toHaveBeenCalled();
    }));   

    it('should test exception in fetching CT Terminals', () => {
      const xService = fixture.debugElement.injector.get(EtaEngineService);
      const mockCall = spyOn(xService,'getCTTerminals').and.returnValue(throwError({status: 404}));
      component.fetchCTTerminals();

      expect(mockCall).toHaveBeenCalled();
    });

    it('should test exception on item triggered', () => {
      const xService = fixture.debugElement.injector.get(EtaService);
      const mockCall = spyOn(xService,'createTransportItem').and.returnValue(throwError({status: 404}));
      component.onItemTriggered(transportItem);
      expect(mockCall).toHaveBeenCalled();
    });

    it('should test exception on get transport Item', () => {
      const xService = fixture.debugElement.injector.get(EtaService);
      const mockCall = spyOn(xService,'getTransportItemById').and.returnValue(throwError({status: 404}));
      component.fetchInformation("1");
      expect(mockCall).toHaveBeenCalled();
    });

    it('should test exception on get Route', () => {
      const xService = fixture.debugElement.injector.get(EtaService);
      const mockCall = spyOn(xService,'getRouteById').and.returnValue(throwError({status: 404}));
      component.fetchInformation("1");
      expect(mockCall).toHaveBeenCalled();
    });
});
