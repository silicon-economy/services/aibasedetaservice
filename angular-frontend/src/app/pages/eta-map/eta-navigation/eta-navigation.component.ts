/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { AfterViewInit, Component } from '@angular/core';
import { TransportModeRoutesColorMap } from '../../../core/utils/transport-mode-routes-color-map';

/** 
 * @Author Sandra Jankowski
 */

@Component({
  selector: 'app-eta-navigation',
  templateUrl: './eta-navigation.component.html',
  styleUrls: ['./eta-navigation.component.scss']
})
/**
* Legend for the transport modes shown as route on the map. 
*/
export class EtaNavigationComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    document.getElementById("legendColorRoad").style.backgroundColor = TransportModeRoutesColorMap.getColorHex("ROAD", false);
    document.getElementById("legendColorRail").style.backgroundColor = TransportModeRoutesColorMap.getColorHex("RAIL", false);
    document.getElementById("legendColorWater").style.backgroundColor = TransportModeRoutesColorMap.getColorHex("WATER", false);
    document.getElementById("legendColorIncident").style.backgroundColor = TransportModeRoutesColorMap.getIncidentColorHex();
  }
}
