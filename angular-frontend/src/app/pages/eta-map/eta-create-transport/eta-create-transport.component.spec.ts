/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Subject } from 'rxjs';
import { TransportItem } from 'src/app/core/models/transport-item';
import { EtaCreateTransportComponent } from './eta-create-transport.component';

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

describe('EtaCreateTransportComponent', () => {
  let component: EtaCreateTransportComponent;
  let fixture: ComponentFixture<EtaCreateTransportComponent>;

  let validItem: TransportItem = {
    transportId: "1231",
    client: "Alpha",
    timelinessPriority: "LOW",
    source: "München",
    transportMode: "ROAD",
    destination: "Dortmund",
    departure: "2020-08-17T10:00:00",
    timeCorridor: {
      from: "2020-08-18T09:00:00",
      to: "2020-08-18T12:00:00"
    },
    eta: "2020-08-18T09:00:00"
  };

  let invalidItem: TransportItem = {
    transportId: "1231",
    client: "",
    timelinessPriority: "LOW",
    source: "",
    transportMode: "ROAD",
    destination: "",
    departure: "",
    timeCorridor: {
      from: "",
      to: ""
    },
    eta: "2020-08-18T09:00:00"
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EtaCreateTransportComponent]
      , imports: [HttpClientTestingModule, TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: createTranslateLoader,
          deps: [HttpClient]
        }
      }),],
      providers: [TranslateService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaCreateTransportComponent);
    component = fixture.componentInstance;
    component.item = new Subject();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set is ETA/ETD', () => {
    component.isETA();
    expect(component.IsHidden).toEqual(false);
    component.isETD();
    expect(component.IsHidden).toEqual(true);
  });


  it('should set is now/later', () => {
    component.isNow();
    expect(component.IsLater).toEqual(false);
    component.isLater();
    expect(component.IsLater).toEqual(true);
  });

  it('should reset Form', () => {
    component.resetForm();
    expect((<HTMLInputElement>document.getElementById("client")).style.border).toEqual('none');
    expect((<HTMLInputElement>document.getElementById("source")).style.border).toEqual('none');
    expect((<HTMLInputElement>document.getElementById("target")).style.border).toEqual('none');
    expect((<HTMLInputElement>document.getElementById("to")).style.border).toEqual('none');
    expect((<HTMLInputElement>document.getElementById("from")).style.border).toEqual('none');
    expect((<HTMLInputElement>document.getElementById("departure")).style.border).toEqual('none');
  });

  it('should validate Input', () => {
    expect(component.validateInput(validItem)).toEqual("");
    expect(component.validateInput(invalidItem)).toEqual(";from;to;client;source;target");
  });

  it('should highlight invalid fields', () => {
    component.highlightInvalidFields("");
    component.highlightInvalidFields("client;source;target;to;from;departure;date");
    expect((<HTMLInputElement>document.getElementById("client")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("source")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("target")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("to")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("from")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("departure")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("to")).style.border).toEqual('2px solid rgb(255, 200, 200)');
    expect((<HTMLInputElement>document.getElementById("from")).style.border).toEqual('2px solid rgb(255, 200, 200)');
  });

  it('should work on submit clicked', () => {
    component.onSubmitClicked();
    component.IsHidden = true;
    component.onSubmitClicked();
    expect(component.item).toBeTruthy();
  });
});

