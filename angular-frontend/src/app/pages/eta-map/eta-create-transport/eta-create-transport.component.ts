/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { Component, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { TransportItem } from 'src/app/core/models/transport-item';
import { TransportMode } from 'src/app/core/models/transport-mode';
import { TimelinessPriority } from 'src/app/core/models/timeliness-priority';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

/** 
 * @Author Thomas Kirks, Sandra Jankowski
 */

@Component({
  selector: 'app-eta-create-transport',
  templateUrl: './eta-create-transport.component.html',
  styleUrls: ['./eta-create-transport.component.scss']
})
/**
* Mask for transport creation by the user. ETA and ETD calculation choosable. When ETA is choosen, then the departure can be configured now or later. 
*/
export class EtaCreateTransportComponent {

  @Output() item: Subject<TransportItem> = new Subject();
  IsHidden = false;
  IsLater = false;
  tooltipText = '';
  //hack to set tooltip translation
  private intervalId = setInterval(() => {
    if (this.tooltipText == '') {
      this.tooltipText = this.translate.instant("ETD-Tooltip");
      (<HTMLInputElement>document.getElementById("tip-above")).setAttribute('data-tip', this.tooltipText);
    }
  }, 1000)

  constructor(private translate: TranslateService) {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.tooltipText = this.translate.instant("ETD-Tooltip");
      if ((<HTMLInputElement>document.getElementById("tip-above")) != null)
        (<HTMLInputElement>document.getElementById("tip-above")).setAttribute('data-tip', this.tooltipText);
    });
  }

  /**
  * Show departure field.
  */
  isETA() {
    this.IsHidden = false;
    (<HTMLInputElement>document.getElementById("departure")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("to")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("from")).style.border = 'none';
  }
  /**
  * Hide departure field.
  */
  isETD() {
    this.IsHidden = true;
    (<HTMLInputElement>document.getElementById("departure")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("to")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("from")).style.border = 'none';
  }
  /**
  * Disable departure field.
  */
  isNow() {
    this.IsLater = false;
    (<HTMLInputElement>document.getElementById("departure")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("to")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("from")).style.border = 'none';
  }
  /**
  * Enable departure field.
  */
  isLater() {
    this.IsLater = true;
    (<HTMLInputElement>document.getElementById("departure")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("to")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("from")).style.border = 'none';
  }

  /**
  * Prepare and validate output item after confirmation button is clicked.
  */
  onSubmitClicked() {
    let _client: string = (<HTMLInputElement>document.getElementById("client")).value;
    let _source: string = (<HTMLInputElement>document.getElementById("source")).value;
    let _target: string = (<HTMLInputElement>document.getElementById("target")).value;
    let _from: string = (<HTMLInputElement>document.getElementById("from")).value;
    let _to: string = (<HTMLInputElement>document.getElementById("to")).value;
    let _departure: string = (<HTMLInputElement>document.getElementById("departure")).value;
    let _transportMode: TransportMode = <TransportMode>(<HTMLInputElement>document.getElementById("transportMode")).value;
    let _timelinessPriority: TimelinessPriority = <TimelinessPriority>(<HTMLInputElement>document.getElementById("timelinessPriority")).value;

    let _item: TransportItem;
    //it ETD
    if (this.IsHidden) {
      _departure = '';
      //is ETA
    } else {
      var d = new Date();
      d.setTime(d.getTime() - new Date().getTimezoneOffset() * 60 * 1000)
      if (!this.IsLater) _departure = d.toISOString().substr(0, 16);
    }
    _item = {
      'client': _client,
      'source': _source,
      'destination': _target,
      'transportMode': _transportMode,
      'timelinessPriority': _timelinessPriority,
      'timeCorridor': {
        'from': _from,
        'to': _to,
      },
      'departure': _departure,
      'eta': _from
    };


    let sValid: string = this.validateInput(_item);

    if (sValid == '') {
      // Input is valid
      this.item.next(_item);
    }
    else {
      // Input is not valid
      this.highlightInvalidFields(sValid);
    }
  }

  /** Highlight specific invalid fields */
  highlightInvalidFields(sValid: string) {
    var possibleIds: string[] = ["client", "source", "target", "to", "from"];

    for (var id of possibleIds) {
      if (sValid.includes(id)) {
        this.setBorderStyle(id);
      }
    }
    if (sValid.includes('departure')) {
      this.setBorderStyle("departure");
      this.setBorderStyle("to");
      this.setBorderStyle("from");
    }
    if (sValid.includes('date')) {
      this.setBorderStyle("to");
      this.setBorderStyle("from");
    }
  }

  setBorderStyle(id: string) {
    (<HTMLInputElement>document.getElementById(id)).style.border = '2px solid #ffc8c8';
  }

  /** Validate the input fields. */
  validateInput(item: TransportItem): string {
    this.resetForm();
    let sValidation = '';

    if (new Date(item.timeCorridor.from) >= new Date(item.timeCorridor.to)) {
      sValidation = 'date';
    }
    if (this.isEmpty(item.timeCorridor.from)) {
      sValidation += ';from';
    }
    if (this.isEmpty(item.timeCorridor.to)) {
      sValidation += ';to';
    }
    if (!this.IsHidden && this.IsLater) {
      if (this.isEmpty(item.departure) || new Date(item.departure) >= new Date(item.timeCorridor.to)) {
        sValidation = ';departure';
      }
    } else {
      if (new Date(item.departure) >= new Date(item.timeCorridor.to)) {
        sValidation = ';departure';
      }
    }
    if (this.isEmpty(item.client)) {
      sValidation += ';client';
    }
    if (this.isEmpty(item.source)) {
      sValidation += ';source';
    }
    if (this.isEmpty(item.destination)) {
      sValidation += ';target';
    }

    return sValidation;
  }

  isEmpty(str: string): boolean {
    return (str == undefined || str == "");
  }

  /** Reset the potentially highlighted input fields to default. */
  resetForm() {
    (<HTMLInputElement>document.getElementById("client")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("source")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("target")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("to")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("from")).style.border = 'none';
    (<HTMLInputElement>document.getElementById("departure")).style.border = 'none';
  }
}
