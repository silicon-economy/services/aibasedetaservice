/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { TimelinessPriority } from './timeliness-priority';
import { TransportMode } from './transport-mode';
import { TimeCorridor } from './time-corridor';


/**
 * The Transport item which represents a row in the ETA-Table.
 *
 *
 * @author Pajtim Thaqi
 */
export interface TransportItem {
  // The transport id.
  transportId?: string;

  // The client name.
  client: string;

  // The timeliness priority.
  timelinessPriority: TimelinessPriority;

  // The source of this transport.
  source: string;

  // The transport mode.
  transportMode: TransportMode;

  // The target for this transport.
  destination: string;

  // The time when this transport started.
  departure: string;

  //The time  when this transport aproximatly will arrive.
  eta?: string;

  //the timewindow this transport should arrive.
  timeCorridor: TimeCorridor;
}
