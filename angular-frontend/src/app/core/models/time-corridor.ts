/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

/**
 * The time corridor for a transport to arrive.
 */
export interface TimeCorridor {
  from: string;
  to: string;
}

