/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

export class LatLng {
	/**
	 * The latitude in degrees.
	 * 
	 * Latitude is specified in degrees within the range [-90, 90].
	 */
	lat: any;
	
	/**
	 * The langitude in degrees.
	 * 
	 * Longitude is specified in degrees within the range [-180, 180].
	 */
	lng: any;
}
