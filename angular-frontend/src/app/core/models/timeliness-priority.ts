/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

/**
 * An enum for the timeliness priority of a transport.
 */
export type TimelinessPriority = 'LOW' | 'MEDIUM' | 'HIGH' | 'VERY_HIGH';

