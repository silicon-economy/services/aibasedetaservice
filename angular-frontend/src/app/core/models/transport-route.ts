/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import {LatLng} from './LatLng';
import { RouteSegmentsByTransportMode } from './transport-mode-routes';

export interface TransportRoute {
	/* The start position of the transporter. */
	source: LatLng;

	/* The target position of the transporter. */
	destination: LatLng;

	/* List of route segments. */
	routeSegmentsByTransportMode: RouteSegmentsByTransportMode[];
}
