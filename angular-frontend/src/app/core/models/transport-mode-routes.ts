/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import {TransportMode} from './transport-mode';

export class RouteSegmentsByTransportMode {

	/* The transport mode for the segments */
	transportMode: TransportMode;

	/* The segments of the route to render. */
	routeSegments: any;

}
