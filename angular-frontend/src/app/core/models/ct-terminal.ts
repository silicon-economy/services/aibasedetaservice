/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { CTType } from './ct_type';


/**
 * The CT Terminals
 *
 *
 * @author Sandra Jankowski
 */
export interface CTTerminal {

  name: string; 

  node: number;

  lat:any;

  lng: any;

  ct_type: CTType;
}
