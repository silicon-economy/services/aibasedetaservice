/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { TransportMode } from '../models/transport-mode';

export abstract class TransportModeRoutesColorMap {
  public static getColorAsRGB(mode: TransportMode, isIncident: boolean): string {
    if (isIncident) return this.getIncidentColor();
    if (mode == "RAIL") return "rgb(102,0,102)";
    if (mode == "ROAD") return "rgb(0,0,0)";
    if (mode == "WATER") return "rgb(0,51,255)";
    return "grey";
  }

  public static getColorHex(mode: TransportMode, isIncident: boolean): string {
    if (isIncident) return this.getIncidentColorHex();
    if (mode == "RAIL") return "#660066";
    if (mode == "ROAD") return "#000000";
    if (mode == "WATER") return "#0033ff";
    return "grey";
  }

  public static getIncidentColor(): string {
    return "rgb(255,0,0)";
  }

  public static getIncidentColorHex(): string {
    return "#ff0000";
  }
}
