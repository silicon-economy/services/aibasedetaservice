/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { TransportMode } from "../models/transport-mode";
import { TransportModeRoutesColorMap } from "./transport-mode-routes-color-map";

describe('TransportModeRoutesColorMap', () => {
  it('getColorAsRGB should return color correctly for ROAD', () => {
    // arrange
    const transportMode: TransportMode = 'ROAD';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorAsRGB(transportMode, isIncident);

    // assert
    expect(result).toEqual("rgb(0,0,0)");
  });

  it('getColorAsRGB should return color correctly for RAIL', () => {
    // arrange
    const transportMode: TransportMode = 'RAIL';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorAsRGB(transportMode, isIncident);

    // assert
    expect(result).toEqual("rgb(102,0,102)");
  });

  it('getColorAsRGB should return color correctly for WATER', () => {
    // arrange
    const transportMode: TransportMode = 'WATER';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorAsRGB(transportMode, isIncident);

    // assert
    expect(result).toEqual("rgb(0,51,255)");
  });

  it('getColorAsRGB should return color correctly for an incident', () => {
    // arrange
    const transportMode: TransportMode = 'WATER';
    const isIncident = true;

    // act
    const result =
      TransportModeRoutesColorMap.getColorAsRGB(transportMode, isIncident);

    // assert
    expect(result).toEqual("rgb(255,0,0)");
  });

  it('getColorAsRGB should return color correctly for an unknown transport mode', () => {
    // arrange
    const transportMode: any = 'AIR';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorAsRGB(transportMode, isIncident);

    // assert
    expect(result).toEqual("grey");
  });

  it('getColorHex should return color correctly for ROAD', () => {
    // arrange
    const transportMode: TransportMode = 'ROAD';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorHex(transportMode, isIncident);

    // assert
    expect(result).toEqual("#000000");
  });

  it('getColorHex should return color correctly for RAIL', () => {
    // arrange
    const transportMode: TransportMode = 'RAIL';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorHex(transportMode, isIncident);

    // assert
    expect(result).toEqual("#660066");
  });

  it('getColorHex should return color correctly for WATER', () => {
    // arrange
    const transportMode: TransportMode = 'WATER';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorHex(transportMode, isIncident);

    // assert
    expect(result).toEqual("#0033ff");
  });

  it('getColorHex should return color correctly for an incident', () => {
    // arrange
    const transportMode: TransportMode = 'ROAD';
    const isIncident = true;

    // act
    const result =
      TransportModeRoutesColorMap.getColorHex(transportMode, isIncident);

    // assert
    expect(result).toEqual("#ff0000");
  });

  it('getColorHex should return color correctly for an unknown transport mode', () => {
    // arrange
    const transportMode: any = 'AIR';
    const isIncident = false;

    // act
    const result =
      TransportModeRoutesColorMap.getColorHex(transportMode, isIncident);

    // assert
    expect(result).toEqual("grey");
  });

});
