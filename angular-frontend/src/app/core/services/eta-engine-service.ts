/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CTTerminal } from '../models/ct-terminal';

/**
 * @author Sandra Jankowski
 */

@Injectable({
  providedIn: 'root'
})
export class EtaEngineService {
  private static readonly API_URL_CTTERMINALS = `${environment.etaEngineHost}:${environment.etaEnginePort}/api/v1/ct-terminals`;
  constructor(private http: HttpClient) { }

  /**
   * Over this endpoint, all ct-terminals can be fetched.
   */
  getCTTerminals(): Observable<CTTerminal[]> {
    return this.http.get<CTTerminal[]>(EtaEngineService.API_URL_CTTERMINALS);
  }
}
