/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { environment } from 'src/environments/environment';
import { CTTerminal } from '../models/ct-terminal';
import { EtaEngineService } from './eta-engine-service';

const TERMINALS: CTTerminal[] = [
  {
    name: "1231",
    node: 1,
    lat: "51.123",
    lng: "52.247",
    ct_type: "RAIL"
  },
  {
    name: "1231d",
    node: 2,
    lat: "51.123",
    lng: "52.247",
    ct_type: "RAIL"
  }
];

describe('EtaEngineService', () => {
  let service: EtaEngineService;
  let httpClient: HttpTestingController;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [ EtaEngineService ],
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.get(EtaEngineService);
    httpClient = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([EtaEngineService], () => {
    expect(service).toBeTruthy();
  }));

  it('getTerminals should work', () => {
    // arrange
    service.getCTTerminals().subscribe(
      data => {
        // assert
        expect(data.length).toEqual(2);
        expect(data[0].name).toEqual("1231");
        expect(data[1].name).toEqual("1231d");
      }
    )

    // act
    httpClient.expectOne(`${environment.etaEngineHost}:${environment.etaEnginePort}/api/v1/ct-terminals`).event(
      new HttpResponse<{}>({body: TERMINALS})
    )
  });
});
