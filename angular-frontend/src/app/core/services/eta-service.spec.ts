/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { environment } from 'src/environments/environment';
import { TransportItem } from '../models/transport-item';
import { TransportRoute } from '../models/transport-route';
import { EtaService } from "./eta-service";

const TRANSPORT_ITEMS: TransportItem[] = [
  {
      transportId: "1231",
      client: "Alpha",
      timelinessPriority: "LOW",
      source: "München",
      transportMode: "ROAD",
      destination: "Dortmund",
      departure: "2020-08-17T10:00:00",
      timeCorridor: {
          from: "2020-08-18T09:00:00",
          to: "2020-08-18T12:00:00"
      },
      eta:"2020-08-18T09:00:00"
  },
  {
      transportId: "3234",
      client: "Butter",
      timelinessPriority: "VERY_HIGH",
      source: "Prien",
      transportMode: "ROAD",
      destination: "Frankfurt",
      departure: "2020-08-17T10:00:00",
      timeCorridor: {
          from: "2020-08-18T09:00:00",
          to: "2020-08-18T12:01:00"
      },
      eta:"2020-08-18T09:00:00"
  }
];

describe('EtaService', () => {
  let service: EtaService;
  let httpClient: HttpTestingController;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [ EtaService ],
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.get(EtaService);
    httpClient = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([EtaService], () => {
    expect(service).toBeTruthy();
  }));

  it('getTransportItems should work', () => {
    // arrange
    service.getTransportItems().subscribe(
      data => {
        // assert
        expect(data.length).toEqual(2);
        expect(data[0].transportId).toEqual("1231");
        expect(data[1].transportId).toEqual("3234");
      }
    )

    // act
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/eta-transports`).event(
      new HttpResponse<{}>({body: TRANSPORT_ITEMS})
    )
  });

  it('getTransportItem by ID should work', () => {
    // arrange
    service.getTransportItemById('1231').subscribe(
      data => {
        // assert
        expect(data.transportId).toEqual("1231");
      }
    )

    // act
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/eta-transports/1231`).event(
      new HttpResponse<{}>({body: TRANSPORT_ITEMS[0]})
    )
  });

  it('createTransportItem should work', () => {
    // arrange
    const transportItem: TransportItem = {
        client: "Test",
        timelinessPriority: "VERY_HIGH",
        source: "Dortmund",
        transportMode: "ROAD",
        destination: "Bochum",
        departure: "2020-08-17T10:00:00",
        timeCorridor: {
            from: "2020-08-18T09:00:00",
            to: "2020-08-18T12:01:00"
        },
    };

    // act
    service.createTransportItem(transportItem).subscribe(
      res => {
        expect(res.transportId).toEqual("1");
      }
    );

    transportItem.transportId = "1"
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/eta-transports`).event(
      new HttpResponse<{}>({body: transportItem})
    )
  });

  it('deleteTransportItem for a given id', () => {
    // act
    service.deleteTransportItemById("1").subscribe(res => {
      // assert
      expect(res).toEqual({});
    });

    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/eta-transports/1`).event(
      new HttpResponse<{}>({body: {}})
    )
  })

  it('updateTransportItem for a given id', () => {
    // act
    service.updateTransportItemById("1", TRANSPORT_ITEMS[0]).subscribe(res => {
      // assert
      expect(res).toEqual({});
    })
    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/eta-transports/1`).event(
      new HttpResponse<{}>({body: {}})
    )
  });

  it('getRouteById should return the route for a given id', () => {
    // arrange
    const route: TransportRoute = {
      source: {
        lat: 1.0,
        lng: 1.0
      },
      destination: {
        lat: 2.0,
        lng: 2.0
      },
      routeSegmentsByTransportMode: [
        {
          transportMode: 'ROAD',
          routeSegments: [[1.0, 2.0]]
        }
      ]
    };

    // act
    service.getRouteById("1231").subscribe(res => {
      // assert
      expect(res).toEqual(route);
    });

    httpClient.expectOne(`${environment.apiHost}:${environment.apiPort}/eta-routes/1231`).event(
      new HttpResponse<{}>({body: route})
    )
  });
});
