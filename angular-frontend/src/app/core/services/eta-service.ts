/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TransportItem } from '../models/transport-item';
import { TransportRoute } from '../models/transport-route';

/**
 * @author Pajtim Thaqi
 */

@Injectable({
  providedIn: 'root'
})
export class EtaService {
  private static readonly API_URL_TRANSPORTS = `${environment.apiHost}:${environment.apiPort}/eta-transports`;
  private static readonly API_URL_ROUTE = `${environment.apiHost}:${environment.apiPort}/eta-routes`;

  constructor(private http: HttpClient) { }

  /**
   * Over this endpoint, all transports can be fetched.
   */
  getTransportItems(): Observable<TransportItem[]> {
    return this.http.get<TransportItem[]>(EtaService.API_URL_TRANSPORTS);
  }

  /**
   * Get a single transport for the given transport id.
   *
   * @param transportId the transport id
   */
  getTransportItemById(transportId: string): Observable<TransportItem> {
    const url = `${EtaService.API_URL_TRANSPORTS}/${transportId}`;
    return this.http.get<TransportItem>(url);
  }

  /**
   * A new transport with the data in the request body will be created.
   *
   * @param transportItem the new transport item
   */
  createTransportItem(transportItem: TransportItem): Observable<TransportItem> {
    return this.http.post<TransportItem>(EtaService.API_URL_TRANSPORTS, transportItem);
  }

  /**
   * Over this endpoint, it is possible to delete a transport item for the given transport id as provided in the path.
   *
   * @param transportId the transport id to delete
   */
  deleteTransportItemById(transportId: string): Observable<{}> {
    const url = `${EtaService.API_URL_TRANSPORTS}/${transportId}`;
    return this.http.delete<{}>(url);
  }

  /**
   * Over this endpoint, it is possible to update a transport item for the given transport id as provided in the path.
   *
   * @param transportId the transport id to update
   * @param transportItem the updated informations
   */
  updateTransportItemById(transportId: string, transportItem: TransportItem): Observable<{}> {
    const url = `${EtaService.API_URL_TRANSPORTS}/${transportId}`;
    return this.http.put<{}>(url, transportItem);
  }

  /**
   * For a given transport id,
   * this endpoint will return the segment for the route to render in the Frontend.
   * It will also return the coordinates for the start and target position.
   *
   * @param transportId the transport id to fetch the route
   */
  getRouteById(transportId: string): Observable<TransportRoute> {
    const url = `${EtaService.API_URL_ROUTE}/${transportId}`;
    return this.http.get<TransportRoute>(url);
  }

}
