/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { CoreModule } from "./core.module";

describe('CoreModule', () => {
    it('should test static method', () => {
        CoreModule.forRoot();
      });
});
