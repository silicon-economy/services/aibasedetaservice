/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/eta-table/eta-table.module').then(m => m.EtaTableModule) },
  { path: 'eta-map', loadChildren: () => import('./pages/eta-map/eta-map.module').then(m => m.EtaMapModule) },
  { path: 'eta-table', loadChildren: () => import('./pages/eta-table/eta-table.module').then(m => m.EtaTableModule) },  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
