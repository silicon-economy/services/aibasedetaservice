/**
* COPYRIGHT Open Logistics Foundation
*
* Licensed under the Open Logistics License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

// This loads environment variables.
// Following this tutorial: https://pumpingco.de/blog/environment-variables-angular-docker/
// To add an evironment variable you also have to add it to 'env.template.js'.

export const environment = {
  // Check if fields are defined before reading them, they are not in e.g. headless chrome
  production: window["env"] && window["env"]["production"] ? window["env"]["production"] : false,
  // API_HOST has to include the protocol since we can not rely on servers redirecting us to https when hardcoding http
  apiHost: window["env"] && window["env"]["API_HOST"] ? window["env"]["API_HOST"] : 'http://localhost',
  apiPort: window["env"] && window["env"]["API_PORT"] ? window["env"]["API_PORT"] : '8080',
  //ETA-Engine Host and port
  etaEngineHost: window["env"] && window["env"]["ETA_ENGINE_HOST"] ? window["env"]["ETA_ENGINE_HOST"] : 'http://localhost',
  etaEnginePort: window["env"] && window["env"]["ETA_ENGINE_PORT"] ? window["env"]["ETA_ENGINE_PORT"] : '5000',
  // OSM Tile Server Host and Port
  osmTileServerHost: window["env"] && window["env"]["OSM_TILE_SERVER_HOST"] ? window["env"]["OSM_TILE_SERVER_HOST"] : 'https://osm-tileserver.apps.sele.iml.fraunhofer.de',
  osmTileServerPort: window["env"] && window["env"]["OSM_TILE_SERVER_PORT"] ? window["env"]["OSM_TILE_SERVER_PORT"] : ''
};
