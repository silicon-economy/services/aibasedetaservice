# Angular Frontend Readme

This readme is a supplement to the main documentation you can find under Documentation/ at the top level folder of this project. 

# HTTPS and SSL - Certificate creation.
Followed mostly this [guide](https://medium.com/faun/setting-up-ssl-certificates-for-nginx-in-docker-environ-e7eec5ebb418) to implement HTTOS.

<strong>KEEPING PRIVATE KEY IN GIT MIGHT BE NOT CLEVER</strong>

# Tech Stack
* ETA-Service
* ETA-Engine
* OSM Tile Server ( https://wiki.openstreetmap.org/wiki/Tile_servers )

# Environment Variables
Followed mostly this [guide](https://pumpingco.de/blog/environment-variables-angular-docker/
) to load environment-variables <br>
To add an environment variable you have to add it to:
* /src/assets/env.js
* /src/assets/env.template.js
* /src/environments/environment.ts

# AngularFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.10.

# Docker and nginx

Front will be deployed with nginx. Configurations could found in /nginx/default.conf 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports

This project uses the [license-checker](https://www.npmjs.com/package/license-checker) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:

`npx license-checker --unknown --csv --out ./third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`npx license-checker --unknown --summary > ./third-party-licenses/third-party-licenses-summary.txt`


