#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the import_thread.
"""
import os.path
from pathlib import Path
import secrets
import string
import unittest

from qv_service.feature.importer_thread import ImportThread, prepare_file_and_directory, write_minute_data_to_pickle

PATH_TO_MINUTE_DATA = './tests/resources/test_minute_data.xml'


class TestImporterThread(unittest.TestCase):
    """ Test class for the importer thread. """

    import_thread: ImportThread = None

    def setUp(self):
        self.import_thread = ImportThread(secrets.choice(string.digits),
                                          "minute-importer", secrets.choice(string.digits))

    def test_prepare_file_and_directory(self):
        """ Tests the preparation of the pickle file and directory checks. """
        # Arrange
        test_path = "./tests/resources/123"

        # Act
        result = prepare_file_and_directory(test_path)

        # Assert
        assert os.path.exists(test_path)
        assert str(result).startswith(test_path)
        assert str(result).endswith(".pickle")

    def test_write_minute_data_to_pickle(self):
        """ Tests the function to write the minute data to a pickle file. """
        # Arrange
        test_dir = "./tests/resources/123"
        Path(test_dir).mkdir(parents=True, exist_ok=True)
        test_file = test_dir + "/test.pickle"

        # Act
        with open(PATH_TO_MINUTE_DATA, 'r', encoding="utf-8") as _f:
            write_minute_data_to_pickle(_f, test_file)

        # Assert
        assert os.path.exists(test_file)
        os.remove(test_file)

    def test_set_url(self):
        """ Test setting the url. """
        # Act
        self.import_thread.set_url("Test")

        # Assert
        assert self.import_thread.org_url == "Test"
