#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the preprocessor_thread.
"""
import os
import unittest
from unittest.mock import patch
import pandas as pd
from pandas.testing import assert_frame_equal

import qv_service.feature.preprocessor_thread as ppt


class TestPreprocessorThread(unittest.TestCase):
    """ Test class for the Preprocessor Thread. """

    def test_transform_and_group_data(self):
        """ Test whether the aggregated Data is grouped correctly. """
        # Arrange
        initial_dataframe = pd.read_csv("./tests/resources/ppt_concated_list_of_df.csv")
        transformed_df = pd.read_csv("./tests/resources/ppt_transformed_df_result.csv")
        # Act
        resulting_df = ppt.transform_and_group_data(initial_dataframe)

        # Assert
        pd.testing.assert_frame_equal(resulting_df, transformed_df)

    def test_read_minute_data(self):
        """ Tests the read minute data with pickle and non-pickle files. """
        # Arrange
        test_path = "./tests/resources/123"
        expected_data = {'col1': [1, 2, 5, 6], 'col2': [3, 4, 7, 8]}
        expected_df = pd.DataFrame(data=expected_data)
        resulting_df = pd.DataFrame()

        # Act
        for root, _, files in os.walk(test_path):
            resulting_df = ppt.read_minute_data(files, root)

        assert_frame_equal(resulting_df, expected_df)

    @patch("qv_service.feature.preprocessor_thread.aggregate_minute_data")
    def test_thread(self, aggregate_mock):
        """ Tests if the QV service starts the preprocessor as a thread. """
        # Act
        thread = ppt.PreprocessorThread("test thread", "tt", 1)
        thread.run()

        # Assert
        aggregate_mock.assert_called_once()

    @patch("pandas.DataFrame.to_sql")
    def test_write_row_to_db(self, sql_mock):
        """ Test writing a row to the database. """
        # Arrange
        ser = pd.Series(
            data=['fs.MQ_A40-10E_HFB_NO_1', "bar"],
            index=["predefinedLocationReference", "foo"])

        # Act
        ppt.write_row_to_db(None, "test-time", (0, ser))

        # Assert
        assert sql_mock.call_count == 1

    @patch("qv_service.feature.preprocessor_thread.read_minute_data")
    @patch("qv_service.feature.preprocessor_thread.transform_and_group_data")
    @patch("qv_service.feature.preprocessor_thread.write_data_to_db")
    @patch("os.environ.get")
    @patch("os.remove")
    def test_aggregate_minute_data(self, mock_os_remove, mock_os_get, mock_write_db, mock_transform, mock_read):
        """ Test the aggregate function by just running through. """
        # Arrange
        mock_read.return_value = pd.DataFrame()
        mock_transform.return_value = pd.DataFrame()
        mock_os_get.return_value = "./tests/resources/12345"

        # Act
        ppt.aggregate_minute_data()

        # Assert
        mock_os_get.assert_called_once()
        mock_read.assert_called_once()
        mock_transform.assert_called_once()
        mock_write_db.assert_called_once()
        mock_os_remove.assert_called_once()
