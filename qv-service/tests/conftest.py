#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This test script creates flask fixtures.
"""

import pytest
from qv_service.server import create_app

API_URL = 'api/v1/'


@pytest.fixture
def app():
    """ Fixture to use the flask app in tests. """
    print("------------------Fixturing")
    app_instance = create_app(config='test')
    yield app_instance


@pytest.fixture
def client(app):
    """ A test client for the app. """
    return app.test_client()


def test_reach():
    """ If we're here we misinterpreted the conftest as a test. """
    assert False
