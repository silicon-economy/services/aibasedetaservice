#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the station_importer.
"""
import os
import unittest
from unittest.mock import patch

import pandas as pd

from qv_service.feature import station_importer

PATH_TO_STATIONS_DE = './tests/resources/test_stations.xml'


class TestStationImporter(unittest.TestCase):
    """ Test class for Station Importer. """

    def test_remove_xml_from_directory(self):
        """ Test if files are deleted correctly. """
        # Arrange
        test_directory = './tests/resources/xml_delete_test'
        test_file = 'test.xml'
        with open(test_directory + '/' + test_file, "wb") as file:
            file.write(b'test')

        # Act
        station_importer.remove_xml_from_directory(test_directory)

        # Assert given directory has no xml
        assert len([file for file in os.listdir(test_directory) if os.path.isfile(file)]) == 0

    def test_condition_2_request_and_write_file(self):
        """ Tests the request_and_write_file Method without a given _url. """
        # Arrange
        _url = ''
        test_path = PATH_TO_STATIONS_DE

        directory_assumed = './tests/resources/'
        locations_assumed = pd.DataFrame({
            "predefinedLocationReference": ['fs.MQ_A40-10E_HFB_NO_1', 'fs.MQ_A40-10E_HFB_NO_2',
                                            'fs.MQ_Bergh.09_HFB_SW_1', 'fs.MQ_Bergh.09_HFB_SW_2',
                                            'fs.MQ_ZR_Wa-Do-FG_ZU_NO_R_2'],
            "carriageway": ['mainCarriageway', 'mainCarriageway', 'mainCarriageway', 'mainCarriageway',
                            'mainCarriageway'],
            "lane": ['lane1', 'lane2', 'lane1', 'lane2', 'lane1'],
            "latitude": [51.437344, 51.437344, 51.474907, 51.474907, 51.474243],
            "longitude": [6.946535, 6.946535, 7.545113, 7.545113, 7.137906]
        })

        # Act
        directory_returned, locations_returned = station_importer.request_and_write_file(_url, test_path)

        # Assert
        self.assertEqual(directory_assumed, directory_returned)
        pd.testing.assert_frame_equal(locations_assumed, locations_returned.head(5))

    @patch("qv_service.feature.station_importer.remove_xml_from_directory")
    @patch("qv_service.feature.station_importer.write_to_db")
    @patch("qv_service.feature.station_importer.request_and_write_file")
    def test_import_stations(self, mock_request, mock_write_db, mock_remove_xml):
        """ Test if the import_stations method performs its relevant operations. """
        # Arrange
        mock_request.return_value = ("foo", pd.DataFrame())

        # Act
        station_importer.import_stations("foo")

        # Assert
        mock_request.assert_called_once()
        mock_write_db.assert_called_once()
        mock_remove_xml.assert_called_once()
