#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Test class for the utils.
"""

import unittest
import logging

from qv_service import util


class TestUtil(unittest.TestCase):
    """ Test class for util. """

    def setUp(self):
        """ Set up before tests. """
        logging.basicConfig(level=logging.INFO)

    def test_log(self):
        """ Should log something in the logging framework. """
        # prepare
        single_log = "log_single"
        log_elements = ["log", "log2"]

        # act
        util.log(single_log, log_elements)

        # assert
        # no exceptions
