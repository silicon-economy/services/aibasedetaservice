#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
"""
Test class for the database connector.
"""

import unittest
from unittest.mock import patch

from qv_service.database_connector import connect_to_database

PATH_TO_STATIONS_DE = './tests/resources/test_stations.xml'


class TestDatabaseConnector(unittest.TestCase):
    """ Test class for the Database Connector. """

    @patch("qv_service.database_connector._create_engine")
    def test_connect_to_database(self, mock_db):
        """ Test if the database can connect. """
        # Arrange
        mock_db.return_value = "Test"
        # Act
        result = connect_to_database()
        # Assert
        assert result == "Test"
