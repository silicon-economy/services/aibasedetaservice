# Flask Backend Server for the QV-Service.

## Technology stack
* Python
* Flask
* postgreSQL

## Build, Run and Test: General Preparations
If not declared explicitly otherwise, please run all commands in the qv-service directory.
* First of all, make sure that you configured the env-files properly to your needs (see project readme) 
* Prepare your MDM certificate (see section "Get access to a MDM machine certificate")
* Check that Docker is running and, for development purposes, you run `docker-compose -f docker-compose.dev.yml up` in the project's root directory "aibasedetaservice". This will startup a postgreSQL in a docker for temporary testing. 


## Build, Run and Test (no IDE)
### Further preparations
* Create a virtual environment with `virtualenv venv`.
* Make sure, all dependencies are installed by executing the Pipfile wit `pipenv install`
* Open the virtual environment with `pipenv shell`.

### Start the flask server
You can start the flask server (i.e. the qv-service) with `./application.sh dev-server` (if you do not execute via your IDE).

### run tests
Simply run `pytest` in console.

## Build, Run and Test with PyCharm
### Further preparations

* Then open the project qv-service with PyCharm. If you do so for the first time, PyCharm will install the pipfile dependencies on its own and set up a proper virtual environment.
* Preparations for Running locally:
  * In the PyCharm explorer, find `./qv_service/server.py` and `right click -> Modify Run Configurations...`
  * Configure the following settings:
    * Optional: Define a configuration name (e.g. "Server").
    * Set the environment variables to `PYTHONUNBUFFERED=1;FLASK_APP=qv_service.server.py;FLASK_ENV=development`
    * Set the working directory to `<your local directory>\aibasedetaservice\qv-service\` (note that there is no \qv_service\.)
* Preparations for testing
  * In the PyCharm explorer, find `./tests` and `right click -> Modify Run Configurations...`
  * Configure the following settings:
    * Optional: Set a name (e.g. "Tests").
    * Set the working directory to `<your local directory>\aibasedetaservice\qv-service\` (note that there is no \tests\.)

### Run the qv-service flask server

You can run your "Server" configuration by selecting it and then pressing the green play button in the top right corner.

### Run tests

You can run your "Tests" configuration by selecting it and then pressing the green play button in the top right corner.

## Create and import a necessary MDM certificate for the QV-Service

### Get access to a MDM machine certificate
* Login to your account or create new account for your organization at the [mdm-portal](https://service.mdm-portal.de/mdm-portal-application/).
* When logged into your account, under „My Organisation“ request a Machine Account.
  You should receive a .p12 certificate via E-Mail and an associated password via SMS to your phone.
* Save the received .p12 certificate and note the associated password separately.

### Create a .pem-file from the given .p12-file to use in ETA-Service
Background: Python can’t handle .p12-files accordingly
* Open a Terminal (Linux) in the folder of the saved .p12-file to create the .pem-file:
	* openssl pkcs12 -in my_certificate.p12 -out my_certificate.pem
* The Terminal will then ask:
	* Enter Import Password:
* There, type the via SMS received password.
  Afterwards you can assign your own password for the .pem-file:
	* Enter PEM pass phrase:
	* Verifying - Enter PEM pass phrase:
The file is then created in your current folder and is protected by your assigned password.

## Logging
Use a logging framework e.g. the package logging. printf won't work with docker properly.

## Licenses of third-party dependencies
The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports
This project uses [pip-licenses](https://pypi.org/project/pip-licenses/) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --with-urls --format=csv --output-file=third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --summary --output-file=third-party-licenses/third-party-licenses-summary.txt`
