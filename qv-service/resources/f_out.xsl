<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:pub="http://datex2.eu/schema/2/2_0"
                              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                              xsi:type="D2LogicalModel">
  <xsl:output method="text"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="pub:d2LogicalModel">
    <xsl:apply-templates select="pub:payloadPublication"/>
  </xsl:template>

  <xsl:template match="pub:payloadPublication">
    <!-- HEADERS -->        
    <xsl:text>publicationTime,predefinedLocationReference,vehicleType,vehicleFlowRate,speed</xsl:text>
    <xsl:text>&#xa;</xsl:text>     <!-- LINE BREAK -->
    <xsl:apply-templates select="pub:elaboratedData"/>
  </xsl:template>  

  <xsl:template match="pub:elaboratedData">
    <!-- ROWS --> 
    <xsl:value-of select="concat(ancestor::pub:payloadPublication/pub:publicationTime,',',
                                 descendant::pub:predefinedLocationReference/@id,',',
                                 descendant::pub:vehicleType,',',
                                 descendant::pub:vehicleFlowRate,',',
                                 descendant::pub:speed)"/>
    <xsl:text>&#xa;</xsl:text>     <!-- LINE BREAK -->
  </xsl:template>

</xsl:stylesheet>