#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

""" Setup of the QV service. """

from setuptools import setup
""" Setup. """
setup(
    name='aibasedetaservice.qv-service',
    version='1.0.0',
    packages=['tests', 'qv_service'],
    url='https://www.siliconeconomy.org',
    license='Open Logistics License 1.0',
    author='Sandra Jankowski',
    author_email='sandra.jankowski@iml.fraunhofer.de',
    description='The backend for the QV-Service.',
    install_requires=['Flask',
                      'Flask-Cors',
                      'flask-restx'],
    entry_points={
        "console_scripts": [
            'server=qv_service:server'
        ]
    }
)
