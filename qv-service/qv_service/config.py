#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Configuration for the QV-Service.
"""

# pylint: disable=too-few-public-methods


class BaseConfig:
    """
    Base configuration - allow specific configurations to override settings.
    """
    DEBUG = False
    TESTING = False
    SECRET_KEY = ''
    SESSION_COOKIE_SECURE = True


class ProductionConfig(BaseConfig):
    """
    Configuration is the same as in the base object, which should be safe as default.
    You at least need to override the secret key for production.
    """


class DevelopmentConfig(BaseConfig):
    """
    Production used for local development.
    """
    DEBUG = True
    SESSION_COOKIE_SECURE = False


class TestConfig(BaseConfig):
    """
    Configuration used for tests.
    """
    TESTING = True
    SESSION_COOKIE_SECURE = False


class TestAtServerConfig(BaseConfig):
    """
    Configuration used for tests in within the gitlab ci environment.
    """
    TESTING = True
    SESSION_COOKIE_SECURE = False
