#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
"""
Establish a connection to the database.
"""

import os
from sqlalchemy import create_engine

import qv_service.parameter_names as param


def connect_to_database():
    """
    Reads the needed params and connects to the corresponding database.

    :returns: The created engine to communicate with the database.
    """
    db_user = os.environ.get(param.POSTGRES_USER, '12345')
    db_password = os.environ.get(param.POSTGRES_PASSWORD, '12345')
    db_database = os.environ.get(param.POSTGRES_DB, '12345')
    db_host = os.environ.get(param.POSTGRES_HOST, 'localhost:5432')
    return _create_engine(f'postgresql://{db_user}:{db_password}@{db_host}/{db_database}')


def _create_engine(url):
    """ Creates the engine with the url and returns the engine."""
    return create_engine(url)
