#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
API version 1 specification

All resources are added in their own namespaces
"""


from flask import Blueprint
from flask_restx import Api

# create the api endpoint
api_v1_bp = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(api_v1_bp,
          version='1.0',
          title='QV-Service API',
          description='The QV-Service.')


# add namespace here

__all__ = ["api_v1_bp"]
