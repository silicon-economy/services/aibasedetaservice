#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3
"""
Utils module for little utility methods.
"""
import logging


def log(*args):
    """ Logs the arguments followed by a blank line to the console. """
    for arg in args:
        logging.info(arg)
    logging.info("")
