#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3


"""
Import station data from mdm source.
"""
from datetime import datetime
import glob
import os
import shutil
import time
from io import StringIO
import lxml.etree as et
import pandas as pd
import urllib3

from qv_service.database_connector import connect_to_database
import qv_service.parameter_names as param


def remove_xml_from_directory(file_directory):
    """
    Walks a given directory and removes every file of the xml type stored in that directory.

    :param file_directory: The path to a file directory.
    :type file_directory: String
    """
    files = glob.glob(file_directory + '*.xml')
    for _f in files:
        os.remove(_f)


def request_and_write_file(_url, test_path: str = ""):
    """
    Prepares the filepath and -name. Then tries to connect to the MDM-service using the given url and writes the
    content gotten from the http request into the prepared file-structure.

    :param _url: Stores the url of the MDM-service
    :type _url: String

    :param test_path: Stores the path used for testing. Only used when :param _url: is empty.
    :type test_path: String

    :returns: Returns the directory (str) and station locations (df).
    """

    subscription_id = os.environ.get(param.MDM_SUBSCRIPTION_ID1, '1234567')

    local_filename = subscription_id + '_' + str(
        datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H.%M.%S')) + '.xml'
    file_directory = './resources/station_temp_files/'

    if _url != "":
        os.makedirs(os.path.dirname(file_directory + '/test.txt'), exist_ok=True)
        write_file_from_http(_url, file_directory, local_filename)
    else:
        file_directory = test_path.replace("test_stations.xml", "")
        local_filename = "test_stations.xml"
    locations = read_file_get_locations(file_directory, local_filename)

    return file_directory, locations


def write_file_from_http(_url, file_directory, local_filename):
    """
    Connect to Webservice and write data into a local file as defined through the parameters file_directory and
    local_filename.

    :param _url: Stores the URL to the webservice
    :type _url: String

    :param file_directory: Directory the file is stored in
    :type file_directory: String

    :param local_filename: Name of the file on disk.
    :type local_filename: String
    """
    http = urllib3.PoolManager(cert_file='./resources/certificates/certs.pem',
                               key_password=os.environ.get(param.QV_SERVICE_PASSWORD, 'password'))
    with http.request('GET', _url, preload_content=False) as _r:
        with open(file_directory + local_filename, 'wb') as _f:
            shutil.copyfileobj(_r, _f)


def read_file_get_locations(file_directory, local_filename):
    """
    Reads a file from a given directory and extracts the location data from the file.

    :param file_directory: Stores the base path of the file to be read.
    :type file_directory: String

    :param local_filename: Stores the name of the file to be read.
    :type local_filename: String

    :returns: The location information gathered from the file read.
    """
    with open(file_directory + local_filename, 'rb') as f_in:
        _doc = et.parse(f_in)
        _xsl = et.parse('./resources/locations.xsl')
        _ac = et.XSLTAccessControl.DENY_ALL
        transform = et.XSLT(_xsl, access_control=_ac)
        result = str(transform(_doc))
        locations = pd.read_csv(StringIO(result))

    return locations


def write_to_db(locations):
    """
    Writes QV station location information to postgresql database.

    :param locations: The location information gathered from the QV stations
    :type locations: Pandas DataFrame
    """
    engine = connect_to_database()

    with engine.begin() as connection:
        locations.to_sql('qv_locations', con=connection, if_exists='replace', index=False)


def import_stations(_url: str):
    """
    Imports stations from mdm and writes them to the database.

    :param _url: Stores the url of the MDM-service
    :type _url: String
    """
    file_directory, locations = request_and_write_file(_url)
    write_to_db(locations)
    remove_xml_from_directory(file_directory)
