#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3


"""
Loads minute qv data from database, aggregates it in 15 minutes time windows and stores it in the database.
"""

import threading
import time
import os
import pickle
import pandas as pd

from qv_service.database_connector import connect_to_database
import qv_service.parameter_names as param


def aggregate_minute_data():
    """
    For each qV station the qV data from the last 15 minutes is aggregated to one entry.
    Then the pickle files are removed from disk.

    :returns: The aggregated data as a postgresql-database-entry
    """
    subscription_id = os.environ.get(param.MDM_SUBSCRIPTION_ID2, '12345')

    # Walk all files associated to the subscription_id
    for root, _, files in os.walk(subscription_id):
        _df = read_minute_data(files, root)
        _df = transform_and_group_data(_df)
        write_data_to_db(_df)

        # Remove files from storage
        for i in files:
            os.remove(root + '/' + i)


def read_minute_data(files, root):
    """
    Reads all files and concatenates them to one single dataframe containing the pickle contents.
    The concatenated dataframe then is re-indexed and afterwards the index column is dropped.

    :param files: The files (e.g. from os.walk)
    :param root: The root directory (e.g. from os.walk)

    :returns: A data frame with the concatenated data of the pickle files.
    """
    _list_of_df = []
    # Walks all files in directory and adds pickle content read to a list
    for i in files:
        try:
            _list_of_df.append(pd.read_pickle(root + '/' + i))
        except pickle.UnpicklingError:
            continue

    # Transforms the list into a dataframe
    _df = pd.concat(_list_of_df)
    _df = _df.reset_index().drop(columns={"index"})

    return _df


def write_data_to_db(_df):
    """
    Adds a timestamp to a given dataframe and writes the dataframe into the database.

    :param _df: Given Dataframe
    """
    current_time = pd.to_datetime('now', utc=True)

    engine = connect_to_database()
    for row in _df.iterrows():
        with engine.begin() as connection:
            write_row_to_db(connection, current_time, row)


def write_row_to_db(connection, current_time, row):
    """
    Adds the given timestamp to a row of a data frame. Then writes the row to the database.

    :param connection: Given Database connection
    :param current_time: The current_time that is used as an index
    :param row: the actual row of a dataframe as (index, series) pair.
    """
    table_name = row[1]['predefinedLocationReference']
    data = pd.DataFrame(row[1]).T
    data['timestamp'] = current_time
    data = data.drop(columns={'predefinedLocationReference'})
    data = data.set_index('timestamp')
    data.to_sql(table_name, con=connection, if_exists='append')


def transform_and_group_data(_df):
    """
    Sorts data contained in a given dataframe and groups it by the column 'predefinedLocationReference'.

    :param _df: Given Dataframe
    :returns: The regrouped Dataframe
    """

    df_flow = _df.groupby(['predefinedLocationReference', 'vehicleType'])['vehicleFlowRate'].sum().reset_index()
    df_speed = _df.groupby(['predefinedLocationReference', 'vehicleType'])['speed'].mean().reset_index()

    df_flow_lorry = df_flow.loc[df_flow['vehicleType'] == 'lorry'].drop(columns={'vehicleType'})
    df_speed_lorry = df_speed.loc[df_flow['vehicleType'] == 'lorry'].drop(columns={'vehicleType'})
    df_flow_any = df_flow.loc[df_flow['vehicleType'] == 'anyVehicle'].drop(columns={'vehicleType'})
    df_speed_any = df_speed.loc[df_flow['vehicleType'] == 'anyVehicle'].drop(columns={'vehicleType'})

    df_lorry_final = df_flow_lorry.merge(df_speed_lorry, on='predefinedLocationReference').rename(
        columns={'vehicleFlowRate': 'lorryVehicle_FlowRate',
                 'speed': 'lorry_Speed'})
    df_any_final = df_flow_any.merge(df_speed_any, on='predefinedLocationReference').rename(
        columns={'vehicleFlowRate': 'anyVehicle_FlowRate',
                 'speed': 'anyVehicle_Speed'})
    _df = df_lorry_final.merge(df_any_final, on='predefinedLocationReference')

    return _df


class PreprocessorThread(threading.Thread):
    """ Preprocessor-Thread for qv-data."""

    def __init__(self, thread_id, name, counter):
        """
        Init thread.

        :param thread_id: The ID which is given to the thread

        :param name: The name which is given to the thread

        :param counter: Thread internal counter
        """
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.counter = counter

    def run(self):
        """ Run data-aggregation and storing for 15-minute time window. """
        print("Start preprocessing Thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
        aggregate_minute_data()
        print("Exit preprocessing Thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
