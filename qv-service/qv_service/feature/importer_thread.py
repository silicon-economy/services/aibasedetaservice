#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3


"""
Loads qv data from mdm service and stores it in the database.
"""

import threading
import time
from io import StringIO
import os
from pathlib import Path
from datetime import datetime
import pandas as pd
import urllib3
import lxml.etree as et

import qv_service.parameter_names as param


def import_of_minute_data(_org_url: str):
    """
    Connects to a webservice (MDM) using the given url and a subscription_id. Then writes the data gotten through a
    http request into a pickle file associated with the subscription_id and timestamp of retrieval.

    :param _org_url: The web address of the mdm-service.
    :type _org_url: String
    """
    # Prepare url parameters and webrequest
    subscription_id = os.environ.get(param.MDM_SUBSCRIPTION_ID2, '12345')
    url = _org_url + subscription_id
    http = urllib3.PoolManager(cert_file='./resources/certificates/certs.pem',
                               key_password=os.environ.get(param.QV_SERVICE_PASSWORD, 'password'))

    file = prepare_file_and_directory(subscription_id)

    # Try executing the webrequest
    with http.request('GET', url, preload_content=False) as _r:
        write_minute_data_to_pickle(_r, file)


def prepare_file_and_directory(subscription_id):
    """
    Prepares a directory (checks if exists or creates it) with the subscription_id and
    sets a full file name (path and file). The file name contains the current timestamp.

    :param subscription_id: The ID will be the directory name to create / check existence
    :returns: the directory and file name as one string.
    """
    filedir = subscription_id + '/'
    local_pickle = str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H.%M.%S')) + '.pickle'
    Path(filedir).mkdir(parents=True, exist_ok=True)
    return filedir + local_pickle


def write_minute_data_to_pickle(object_to_write, file):
    """
    Takes an object (from a http-request / file) and writes it to the given file after
    processing the object with etree parsing into a dataframe.

    :param object_to_write: The file / http request object
    :param file: The path and filename as a string
    """
    doc = et.parse(object_to_write)
    xsl = et.parse('./resources/f_out.xsl')
    _ac = et.XSLTAccessControl.DENY_ALL
    transform = et.XSLT(xsl, access_control=_ac)
    result = str(transform(doc))
    dataframe = pd.read_csv(StringIO(result))
    dataframe.to_pickle(file)


class ImportThread(threading.Thread):
    """ Importer-Thread for qv-data."""

    org_url = ""

    def __init__(self, thread_id, name, counter):
        """
        Init thread.

        :param thread_id: The ID given to the thread
        :param name: The name of the thread
        :param counter: Thread internal counter
        """
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.counter = counter

    def run(self):
        """ Run data-collection and storing for current minute. """
        print("Start import Thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))
        import_of_minute_data(self.org_url)
        print("Exit import Thread " + str(self.threadID) + " at " + str(time.ctime(time.time())))

    def set_url(self, _org_url):
        """ Set URL for import. """
        self.org_url = _org_url
