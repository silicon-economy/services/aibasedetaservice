#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
This module is designed to start a flask server. This is the entry point for external consumers to
interact with the qv-service.

Initializes flask server.
Initializes loggers.
"""
import os
import logging
import secrets
import string

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask
from flask_cors import CORS

import qv_service.parameter_names as param
from qv_service.feature.importer_thread import ImportThread
from qv_service.feature.preprocessor_thread import PreprocessorThread
from qv_service.api import api_v1_bp
from qv_service.feature import station_importer

BACKEND_DEBUG_MODE_PARAM_NAME = "BACKEND_DEBUG_MODE"


def create_app(config='production') -> Flask:
    """
    Creates flask app.
    Registers blueprint for the eta api.
    Removes CORS filter.

    :returns: the flask app
    """
    flask_app = Flask(__name__)

    if flask_app.config['ENV'] == 'development' or config == 'development':
        flask_app.config.from_object('qv_service.config.DevelopmentConfig')
    elif config == 'production':
        flask_app.config.from_object('qv_service.config.ProductionConfig')
    elif config == 'test':
        if os.environ.get('WHERE_ARE_WE_TESTING') is not None:
            flask_app.config.from_object('qv_service.config.TestAtServerConfig')
        else:
            flask_app.config.from_object('qv_service.config.TestConfig')

    # Init loggers
    init_loggers()

    # Initialize extensions
    configure_extensions(flask_app)

    # Register blueprints
    register_blueprints(flask_app)

    # Add CORS header
    CORS(flask_app)
    if config != 'test':
        import_stations()
        start_importer()
        start_preprocessing()
        scheduler = BackgroundScheduler()
        scheduler.add_job(func=start_importer, trigger="interval", minutes=1)
        scheduler.add_job(func=import_stations, trigger="interval", hours=24)
        scheduler.add_job(func=start_preprocessing, trigger="interval", minutes=15)

        scheduler.start()

    return flask_app


# -*- coding: utf-8 -*-
def import_stations():
    """ Import all stations from mdm. """
    importer = station_importer

    importer_url = os.environ.get(param.MDM_HOST, 'https://broker.mdm-portal.de')
    subscription_id = os.environ.get(param.MDM_SUBSCRIPTION_ID1, '12345')

    importer.import_stations(
        f"{importer_url}/BASt-MDM-Interface/srv/{subscription_id}/clientPullService?subscriptionID={subscription_id}")


def start_preprocessing():
    """ Start preprocessor thread for aggregating minute data. """
    preprocessing_thread = PreprocessorThread(secrets.choice(string.digits),
                                              "minute-data-aggregation", secrets.choice(string.digits))
    preprocessing_thread.start()


def start_importer():
    """ Start importer thread for importing minute data. """
    import_thread = ImportThread(secrets.choice(string.digits),
                                 "minute-importer", secrets.choice(string.digits))

    importer_url = os.environ.get(param.MDM_HOST, 'https://broker.mdm-portal.de')
    subscription_id = os.environ.get(param.MDM_SUBSCRIPTION_ID2, '12345')

    import_thread.set_url(f"{importer_url}/BASt-MDM-Interface/srv/{subscription_id}/clientPullService?subscriptionID=")

    import_thread.start()


def init_loggers():
    """ Initializes loggers. """
    logging.basicConfig(level=logging.INFO)


def configure_extensions(flask_app: Flask):
    """ Configure all extensions like Database. """


def register_blueprints(flask_app: Flask):
    """ Register all blueprints used in the python-backend. """
    flask_app.register_blueprint(api_v1_bp)


if __name__ == '__main__':
    app = create_app()
    logging.info("Starting the QV-Service!")

    app.run(host="0.0.0.0", port=5002, debug=True, use_reloader=False)
