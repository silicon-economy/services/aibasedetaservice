#   Copyright Open Logistics Foundation
#
#   Licensed under the Open Logistics Foundation License 1.3.
#   For details on the licensing terms, see the LICENSE file.
#   SPDX-License-Identifier: OLFL-1.3

"""
Some constants for parameter names.
"""

POSTGRES_USER = "POSTGRES_USER"
POSTGRES_PASSWORD = "POSTGRES_PASSWORD"
POSTGRES_DB = "POSTGRES_DB"
POSTGRES_HOST = "POSTGRES_HOST"
QV_SERVICE_PASSWORD = "QV_SERVICE_PASSWORD"
MDM_SUBSCRIPTION_ID1 = "MDM_SUBSCRIPTION_ID1"
MDM_SUBSCRIPTION_ID2 = "MDM_SUBSCRIPTION_ID2"
MDM_HOST = "MDM_HOST"
