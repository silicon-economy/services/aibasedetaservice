db.auth('admin', 'password');

db = db.getSiblingDB('admin');

db.createUser({
  user: 'user',
  pwd: 'password',
  roles: [
    {
      role: 'readWrite',
      db: 'etaDB'
    }
  ]
});

db = db.getSiblingDB('etaDB');

db.ct_terminals.insert({
  name: 'KV Test1',
  node: 111,
  lat: 123,
  lng: 321,
  ct_type: 'WATER'
});
